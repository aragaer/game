#!/usr/bin/python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""
    Selenium-related things.
"""


def find_element_by_label(browser, label):
    "What it says."
    labeled = browser.find_element_by_xpath(
        "//label[text()='%s']" % label).get_attribute("for")
    return browser.find_element_by_id(labeled)
