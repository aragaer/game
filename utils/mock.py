#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=import-error

"""
    Import mock module - will work for both CPython and PyPy
"""

try:
    from unittest.mock import call, DEFAULT, Mock, patch, sentinel
except ImportError:
    from mock import call, DEFAULT, Mock, patch, sentinel


__all__ = ['call', 'DEFAULT', 'Mock', 'patch', 'sentinel']
