# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
Feature: Crystals registration and login
  As a potential or existing player
  I want to be able to register or login to Crystals
  So that I could play it

  Background: Logged off
    Given cookies are cleared

  Scenario: Greeting
    Given I go to the crystals game page
     Then I am asked to login
      And I see those login options:
       | link               |
       | Google OpenID      |
       | LiveJournal OpenID |
       | VKontakteID        |
       | Yandex OpenID      |

  Scenario Outline: Register
    Given I go to the crystals game page
     When I click the '<provider>' link
     Then I am required to authorize
     When I authorize as '<user>'
     Then I am redirected to inventory
      And I have 3 empty bowls
      And I have the following powders:
        | color | amount  |
        | blue  | 10      |
        | red   | 10      |
        | green | 10      |

  Examples: New accounts:
    | provider      | user    |
    | Google OpenID | Vasya   |
    | VKontakteID   | alla.p  |
    | Yandex OpenID | Vasya2  |

  Scenario Outline: Returning and logout
    Given I am registered using '<provider>' as '<user>'
      And I go to the crystals game page
     Then I am redirected to inventory
     When I click 'Logout' in navigation bar
     Then I am asked to login

  Examples: Returning accounts:
    | provider      | user    |
    | Google OpenID | Vasya   |
    | Yandex OpenID | Vasya2  |
