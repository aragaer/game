# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
Feature: Crystals grouping 
  As a Crystals player
  I want to be see similar crystals grouped together
  So that I could better understand what I have

  Scenario Outline: Displaying similar crystals
    Given I have <count> <color> <size>-carat crystals
      And I go to the crystals game page
     Then I have <count> <color> <size>-carat crystals
     When I go to jeweler
     Then I can sell <count> <color> <size>-carat crystals

  Examples:
    | count | size  | color |
    | 3     | 3     | red   |
    | 2     | 5     | blue  |
    | 15    | 2     | green |

  Scenario Outline: Collecting multiple crystals
    Given bowl <bowl> contains <count> <color> <size>-carat crystals
     When I collect from bowl 1
      And I go to inventory
     Then I have <count> <color> <size>-carat crystals

  Examples:
    | bowl  | count | color | size  |
    | 2     | 7     | blue  | 2     |
    | 1     | 5     | red   | 3     |

  Scenario Outline: Combining collected crystals
    Given I have <count1> <color> <size>-carat crystals
      And bowl <bowl> contains <count2> <color> <size>-carat crystals
     When I collect from bowl 1
      And I go to inventory
     Then I have <count> <color> <size>-carat crystals

  Examples:
    | bowl  | count1  | count2  | count | color | size  |
    | 1     | 3       | 5       | 8     | red   | 3     |
    | 2     | 1       | 6       | 7     | blue  | 2     |
    | 2     | 1       | 1       | 2     | green | 2     |
    | 2     | 4       | 1       | 5     | cyan  | 2     |
