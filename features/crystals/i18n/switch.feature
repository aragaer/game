# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
Feature: Crystals game
  As a player
  I want to be able to switch the locale
  So that I could see the page in my preferred language

  Scenario: List of languages
    Given I go to the crystals game page
     Then I see the following language options:
        | language  |
        | EN        |
        | RU        |

  Scenario: Default language
    Given I go to the crystals game page
     Then selected language is EN

  Scenario: Switchable languages
    Given I go to the crystals game page
     Then I can switch to following languages:
        | language  |
        | RU        |

  @locale(ru)
  Scenario: Browser-based locale
    Given I go to the crystals game page
     Then selected language is RU
      And I can switch to following languages:
        | language  |
        | EN        |

  Scenario: Switch language
    Given I go to the crystals game page
     When I open the language dropdown
      And I click the 'RU' link
     Then selected language is RU
     When I open the language dropdown
     Then I can switch to language EN

  Scenario: Language is stored between refreshes
    Given cookies are cleared
      And I go to the crystals game page
     When I open the language dropdown
      And I click the 'RU' link
     Then selected language is RU
     When I access the path '/'
     Then selected language is RU

  @locale(ru)
  Scenario: User settings are stored between browsers
    Given I am registered using 'Google OpenID' as 'Vasya'
      And I go to the crystals game page
     When I open the language dropdown
      And I click the 'EN' link
      And I click 'Logout' in navigation bar
      And I clear all cookies
      And I access the path '/'
     Then selected language is RU
     When I click the 'Google OpenID' link
      And I authorize as 'Vasya'
     Then selected language is EN
