# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@locale(ru)
Feature: Crystals game
  As a russian-speaking player
  I want to be able to play game in russian
  So that I didn't have to translate it from english

  Background: Login page opened
    Given I go to the crystals game page

  Scenario: Welcome
     Then I see greeting in Russian

  Scenario: Localized inventory
    Given I am registered using 'VKontakteID' as 'alla.p'
     Then I see that page title is 'Инвентарь'
      And I see the following inventory categories:
        | category  |
        | Порошки   |
        | Кристаллы |
      And wallet is labeled 'Деньги'
     When I expand navbar
     Then I see the following links:
        | link    | label |
        | market  | Рынок |
        | logout  | Выход |

  Scenario: Localized market
    Given I am registered using 'VKontakteID' as 'alla.p'
     When I click 'Рынок' in navigation bar
     Then I see that page title is 'Рынок'
      And I see the following shops:
        | Чаши    |
        | Порошки |
        | Ювелир  |
     When I expand navbar
     Then I see the following links:
        | link    | label       |
        | news    | Новости     |
        |         | Статус      |
        | market  | Рынок       |
        | lab     | Лаборатория |
        | logout  | Выход       |
      And wallet is labeled 'Деньги'
