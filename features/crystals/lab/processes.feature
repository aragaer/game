# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
@ui
Feature: Laboratory page
  As a Crystals player
  I want to see only processes in my laboratory
  So that I didn't have to find an empty bowl among the processes

  Scenario: All empty
     When I access the path '/lab/'
     Then I have no running processes
      And I have 3 empty bowls
