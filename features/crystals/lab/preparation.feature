# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
@ui
Feature: Laboratory page
  As a Crystals player
  I want to start process directly from laboratory page
  So that I didn't need to go to inventory for that

  Scenario: Starting process
    Given I access the path '/lab/'
     When I click the plus button on new process
     Then I am redirected to preparation
