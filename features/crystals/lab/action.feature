# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
@ui
Feature: Laboratory page
  As a Crystals player
  I want to be able to operate on processes from laboratory page
  So that I didn't have to go to inventory for that

  Scenario: Discard a running process
    Given I access the path '/'
      And I remember how much powders I have
      And I start a process:
      | water | powder  | color |
      | 20    | 5       | green |
     When I access the path '/lab/'
     Then I have a bowl of green solution
     When I click the cancel button on process 1
     Then I see a warning message
     When I agree to empty the bowl
     Then I am redirected to laboratory
      And I have 3 empty bowls
     When I go to inventory
     Then I have spent 5 grams of green powder

  Scenario: Collect
    Given bowl 1 contains 3 purple 5-carat crystals
     When I access the path '/lab/'
     Then I have a bowl of purple crystals
     When I click the collect button on process 1
     Then I am redirected to laboratory
      And I have 3 empty bowls
     When I go to inventory
     Then I have 3 purple 5-carat crystals
