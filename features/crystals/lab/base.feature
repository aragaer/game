# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
@ui
Feature: Laboratory page
  As a Crystals player
  I want to have a nice laboratory page
  So that I could easily track the progress of my processes
  And create new ones

  Scenario: Running process
    Given I have a bowl with green solution
     When I access the path '/lab/'
     Then I have a running process
      And solution color is green

  Scenario: Sorting processes
    Given bowl 1 contains 5 green 2-carat crystals
      And I have a bowl with purple solution
     When I collect from bowl 1
     Then I have a bowl of purple solution
