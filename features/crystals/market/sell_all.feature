# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
Feature: Market preview
  As a Crystals player
  I want to be able to sell all my crystals with few clicks
  So that I could spend more time actually growing crystals

  Background: Game opened
    Given I go to the crystals game page

  Scenario Outline: Selling all crystals
    Given I have <total1> <color1> <size1>-carat crystals
      And I have <total2> <color2> <size2>-carat crystals
     When I go to jeweler
      And I click 'Mark all crystals for sale'
     Then all crystals are put for sale
      And I will earn the correct amount of money

    Examples:
      | color1  | size1 | total1  | count1  | color2  | size2 | total2  | count2  |
      | red     | 5     | 3       | 2       | red     | 4     | 8       | 5       |
      | blue    | 3     | 4       | 1       | green   | 10    | 2       | 2       |
      | green   | 7     | 7       | 7       | purple  | 4     | 1       | 0       |

  Scenario Outline: Selling all of one crystal
    Given I have <total> <color> <size>-carat crystals
     When I go to jeweler
      And I click the 'x<total>' near <color> <size>-carat crystal
     Then amount of <color> <size>-carat crystals set for sale is <total>
      And I will earn the correct amount of money

    Examples:
      | color | size  | total |
      | red   | 5     | 3     |
      | purple| 7     | 12    |
