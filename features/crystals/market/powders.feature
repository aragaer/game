# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
Feature: Powders shop
  As a Crystals player
  I want to be able to buy and sell powders
  So that I could grow new crystals and earn money

  Background: Game opened
    Given I go to the crystals game page

  Scenario: Open powder shop
    When I click 'Market' in navigation bar
     And I click the 'Powders' tab
    Then I am redirected to powder shop

  Scenario Outline: Buy powders
    Given I have 100G, 0S and 0C
      And I remember how much powders I have
     When I go to powders shop
      And I check the price of powders
      And I check my wallet
      And I buy <amount> of <color> powder
     Then I have spent the correct amount of money
     When I go to inventory
     Then I have gained <amount> of <color> powder

  Examples: Basic powders:
    | color | amount  |
    | red   | 10      |
    | blue  | 20      |
    | green | 15      |

  Examples: Other powders:
    | color   | amount  |
    | gray    | 40      |
    | cyan    | 5       |
    | purple  | 30      |
    | yellow  | 40      |

  Scenario Outline: Update price
     When I go to powders shop
      And I add <powder1> powder to cart
      And I add <powder2> powder to cart
     Then I will pay the correct amount of money

    Examples:
      | powder1       | powder2       |
      | 10g of red    | 5g of gray    |
      | 7g of purple  | 4g of cyan    |
      | 11g of green  | 0g of yellow  |

  Scenario: Too expensive
   Given I have 0G, 1S and 0C
    When I go to powders shop
     And I add 15g of green powder to cart
    Then 'Trade' button is disabled
