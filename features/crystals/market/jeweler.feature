# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
Feature: Jeweler
  As a Crystals player
  I want to be able to sell grown crystals to jeweler
  So that I could earn money

  Background: Game opened
    Given I go to the crystals game page

  Scenario: Visit jeweler
    When I click 'Market' in navigation bar
     And I click the 'Jeweler' tab
    Then I am redirected to jeweler

  Scenario Outline: Sell single crystal
    Given I have 1 <color> <size>-carat crystal
      And I go to the crystals game page
     Then I have 1 <color> <size>-carat crystal
     When I go to jeweler
      And I check my wallet
      And I sell 1 <color> <size>-carat crystal
     Then I have earned the correct amount of money
     When I go to inventory
     Then I don't have the crystal

  Examples: Single crystal:
    | color | size  |
    | red   | 5     |
    | blue  | 15    |
    | green | 7     |

  Scenario Outline: Selling single crystal from many
    Given I have <count1> <crystal1> crystals
      And I have <count2> <crystal2> crystals
      And I go to the crystals game page
     When I go to jeweler
      And I check my wallet
      And I sell 1 <crystal1> crystal
     Then I have earned the correct amount of money

  Examples: Multiple crystals:
    | count1  | crystal1    | count2  | crystal2        |
    | 5       | red 5-carat | 6       | yellow 2-carat  |
    | 5       | red 2-carat | 6       | yellow 3-carat  |
