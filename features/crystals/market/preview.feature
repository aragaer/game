# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
Feature: Market preview
  As a Crystals player
  I want to be able to see the total buying price in advance
  So that I could update my shopping cart

  Background: Game opened
    Given I go to the crystals game page

  Scenario Outline: Selling crystals
    Given I have <total1> <crystal1> crystals
      And I have <total2> <crystal2> crystals
     When I go to jeweler
      And I add <count1> <crystal1> crystals to sale
      And I add <count2> <crystal2> crystals to sale
     Then I will earn the correct amount of money

    Examples:
      | crystal1      | total1  | count1  | crystal2        | total2  | count2  |
      | red 5-carat   | 3       | 2       | red 4-carat     | 8       | 5       |
      | blue 3-carat  | 4       | 1       | green 10-carat  | 2       | 2       |
      | green 7-carat | 7       | 2       | purple 4-carat  | 1       | 0       |

  Scenario: Buying upgrades
     When I go to market
      And I add 1 bowl to cart
     Then I will pay the correct amount of money
