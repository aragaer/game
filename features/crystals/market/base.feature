# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
Feature: Market
  As a Crystals player
  I want to be able to buy and sell things on market
  So that I could upgrade my alchemist

  Background: Game opened
    Given I go to the crystals game page

  Scenario: Open market
     When I click 'Market' in navigation bar
     Then I am redirected to market
     When I click 'Status' in navigation bar
     Then I am redirected to inventory

  Scenario: Buy bowl
    Given I have 7G, 10S and 20C
      And I remember how much bowls I have
     When I go to market
      And I buy a bowl
      And I check my wallet
     Then I have 2G, 10S and 20C
     When I go to inventory
     Then I have 1 more bowl
