# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
Feature: Money 
  As a Crystals player
  I want to be able to spend and earn money
  So that I could upgrade my alchemist

  Scenario: Initial money
    Given I go to the crystals game page
     When I check my wallet
     Then I have 0G, 10S and 0C

  Scenario Outline: Other amount of money
    Given I have <gold>G, <silver>S and <copper>C
      And I go to the crystals game page
     When I check my wallet
     Then I have <gold>G, <silver>S and <copper>C
     When I go to market
     Then I see the same amount of money

  Examples: Preset money:
      | gold  | silver  | copper  |
      | 1     | 10      | 0       |
      | 0     | 0       | 5       |
      | 20    | 10      | 50      |
