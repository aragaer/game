# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
Feature: Input validation
  As a player
  I want to be able to see correct error messages when I enter incorrect data
  So that I could correct my input accordingly

  Background: Game opened
    Given I go to the crystals game page

  Scenario: No water
    When I access the path '/start/?w=0&p=1&p=0&p=0'
    Then I am redirected to laboratory
    When I access the path '/'
    Then I have the following powders:
      | color | amount  |
      | red   | 10      |
      | blue  | 10      |
      | green | 10      |

  Scenario: Buying negative powders
    Given I have 1G, 0S and 0C
     When I access the path '/trade/powders/?p=5&p=5&p=-10&p=-10&p=0&p=0&p=0&u=0'
     Then I am redirected to powder shop
      And I have 0G, 90S and 0C
     When I go to inventory
     Then I have the following powders:
      | color | amount  |
      | red   | 15      |
      | blue  | 15      |
      | green | 10      |

  Scenario: Buying negative bowls
    Given I have 10G, 0S and 0C
     When I access the path '/trade/?u=-1'
     Then I am redirected to market
      And I have 10G, 0S and 0C
     When I go to inventory
     Then I have 3 empty bowls

  Scenario: Using one negative powder in solution
    When I access the path '/start/?w=1&p=-1&p=5&p=0'
    Then I am redirected to laboratory
    When I access the path '/'
    Then I have the following powders:
      | color | amount  |
      | red   | 10      |
      | blue  | 10      |
      | green | 10      |

  Scenario: Using multiple negative powders in solution
    Given I have 5 of yellow powder
     When I access the path '/start/?w=1&p=-1&p=-5&p=0&p=-3'
     Then I am redirected to laboratory
     When I access the path '/'
     Then I have the following powders:
        | color | amount  |
        | red   | 10      |
        | blue  | 10      |
        | green | 10      |

  Scenario: Negative amount of water
     When I access the path '/start/?w=-5&p=1&p=0&p=0'
     Then I am redirected to laboratory
     When I access the path '/'
     Then I have the following powders:
        | color | amount  |
        | red   | 10      |
        | blue  | 10      |
        | green | 10      |

  Scenario: Excessive amount of water
     When I access the path '/start/?w=1000&p=1&p=0&p=0'
     Then I am redirected to laboratory
     When I access the path '/'
     Then I have the following powders:
        | color | amount  |
        | red   | 10      |
        | blue  | 10      |
        | green | 10      |

  Scenario: Excessive amount of powder
     When I access the path '/start/?w=1&p=10&p=20&p=0'
     Then I am redirected to laboratory
     When I access the path '/'
     Then I have the following powders:
        | color | amount  |
        | red   | 10      |
        | blue  | 10      |
        | green | 10      |

  Scenario: No powders
     When I access the path '/start/?w=1&p=0&p=0&p=0'
     Then I am redirected to laboratory
     When I access the path '/'
     Then I have the following powders:
        | color | amount  |
        | red   | 10      |
        | blue  | 10      |
        | green | 10      |

  Scenario: Collecting from empty bowl
     When I access the path '/bowl/0/collect/'
     Then I am redirected to 404
     When I access the path '/'
     Then I have the following powders:
      | color | amount  |
      | red   | 10      |
      | blue  | 10      |
      | green | 10      |
      And I have 3 empty bowls

  Scenario: Collecting from process
    Given I start a process:
      | water | powder  | color |
      | 10    | 1       | red   |
     When I access the path '/bowl/0/collect/'
     Then I am redirected to laboratory
      And I have a bowl of red solution
      And I have 2 empty bowls
     When I go to inventory
     Then I have the following powders:
      | color | amount  |
      | red   | 9       |
      | blue  | 10      |
      | green | 10      |

  Scenario: Emptying already empty bowl
    Given I access the path '/bowl/0/empty/'
     Then I am redirected to 404
     When I access the path '/'
     Then I have the following powders:
      | color | amount  |
      | red   | 10      |
      | blue  | 10      |
      | green | 10      |
      And I have 3 empty bowls

  Scenario: Non-number field in market
    Given I have 1G, 0S and 0C
    When I access the path '/trade/powders/?p=5&p=5&p=&p=&p=hiya&p=0&p=0&u=0'
     Then I am redirected to powder shop
      And I have 0G, 90S and 0C
     When I go to inventory
     Then I have the following powders:
      | color | amount  |
      | red   | 15      |
      | blue  | 15      |
      | green | 10      |

  Scenario: Out of range collect
    Given I start a process:
      | water | powder  | color |
      | 10    | 1       | green |
     When I access the path '/bowl/5/collect/'
     Then I am redirected to 404
     When I access the path '/'
     Then I have the following powders:
      | color | amount  |
      | red   | 10      |
      | blue  | 10      |
      | green | 9       |
      And I have 2 empty bowls

  Scenario: Negative collect
    Given I start a process:
      | water | powder  | color |
      | 10    | 1       | red   |
     When I access the path '/bowl/-2/collect/'
     Then I am redirected to 404
     When I access the path '/'
     Then I have the following powders:
      | color | amount  |
      | red   | 9       |
      | blue  | 10      |
      | green | 10      |
      And I have 2 empty bowls

  Scenario: Buying more than possible
    Given I have 0G, 2S and 0C
     When I access the path '/trade/?p=15&p=15&p=15&u=2'
     Then I am redirected to market
      And I have 0G, 2S and 0C
     When I go to inventory
     Then I have the following powders:
      | color | amount  |
      | red   | 10      |
      | blue  | 10      |
      | green | 10      |

  Scenario: Non-numeric empty
    Given I start a process:
      | water | powder  | color |
      | 10    | 1       | red   |
     When I access the path '/bowl/test/empty/'
     Then I am redirected to 404
     When I access the path '/'
     Then I have the following powders:
      | color | amount  |
      | red   | 9       |
      | blue  | 10      |
      | green | 10      |
      And I have 2 empty bowls

  Scenario: Negative empty
    Given I start a process:
      | water | powder  | color |
      | 10    | 1       | red   |
     When I access the path '/bowl/-2/empty/'
     Then I am redirected to 404
     When I access the path '/'
     Then I have the following powders:
      | color | amount  |
      | red   | 9       |
      | blue  | 10      |
      | green | 10      |
      And I have 2 empty bowls

  Scenario: Out of range empty
    Given I start a process:
      | water | powder  | color |
      | 10    | 1       | red   |
     When I access the path '/bowl/5/empty/'
     Then I am redirected to 404
     When I access the path '/'
     Then I have the following powders:
      | color | amount  |
      | red   | 9       |
      | blue  | 10      |
      | green | 10      |
      And I have 2 empty bowls

  Scenario: Non-numeric in prepare
    When I access the path '/start/?w=1&p=test&p=5&p=0'
    Then I am redirected to laboratory
    When I access the path '/start/?w=test&p=1&p=0&p=0'
    Then I am redirected to laboratory

  Scenario: Sell negative crystals
    Given I have 2 red 5-carat crystals
      And I have 1G, 0S and 0C
     When I access the path '/trade/?c=-1'
     Then I am redirected to market
      And I have 1G, 0S and 0C
     When I go to inventory
     Then I have 2 red 5-carat crystals

  Scenario: Sell inexistent crystals
    Given I have 2 red 5-carat crystals
      And I have 1G, 0S and 0C
      When I access the path '/trade/?c=0&c=5'
     Then I am redirected to market
      And I have 1G, 0S and 0C
     When I go to inventory
     Then I have 2 red 5-carat crystals

  Scenario: Sell more crystals than available
    Given I have 2 red 5-carat crystals
      And I have 1G, 0S and 0C
     When I access the path '/trade/?c=5'
     Then I am redirected to market
      And I have 1G, 0S and 0C
     When I go to inventory
     Then I have 2 red 5-carat crystals

  Scenario: Sell non-numeric crystals
    Given I have 2 red 5-carat crystals
      And I have 1G, 0S and 0C
     When I access the path '/trade/?c=woof'
     Then I am redirected to market
      And I have 1G, 0S and 0C
     When I go to inventory
     Then I have 2 red 5-carat crystals
