# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
Feature: Real-time check for valid solution
  As a player
  I want to be able to see that solution I prepared is incorrect
  So that I could correct my input accordingly

  Background: Game opened
    Given I access the path '/lab/'
      And I open an empty bowl

  Scenario: Empty solution
     Then I can't start the process

  Scenario: No powders
     When I add 1 ml of water
     Then I can't start the process

  Scenario: No water
     When I add 10 grams of red powder
     Then I can't start the process

  Scenario: Valid solution
     When I add 10 grams of red powder
      And I add 1 ml of water
     Then I can start the process
