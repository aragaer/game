# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
Feature: Solution preview
  As a Crystals player
  I want to be able to see solution parameters before starting the process
  So that I could fix inputs to tune the result

  Background: Game opened
    Given I go to the crystals game page

  Scenario Outline: Single-color preview
    Given I have <amount> of <color> powder
      And I open an empty bowl
     Then I see that there is no solution
     When I add <volume> ml of water
     Then I see that there is <volume> ml of water
     When I add <amount> grams of <color> powder
     Then I see that there is <volume> ml of <color> solution
      And concentration is <high or low>
      And optimal is at <optimal>
      And process will take <time>

    Examples: Single powder:
      | color | amount  | volume  | high or low | optimal | time  |
      | red   | 5       | 30      | high        | 50      | 07:00 |
      | green | 4       | 35      | high        | 40      | 08:10 |
      | cyan  | 7       | 80      | low         | 70      | 18:30 |

  Scenario Outline: Color mix preview
    Given I have <amount1> of <color1> powder
      And I have <amount2> of <color2> powder
      And I open an empty bowl
     Then I see that there is no solution
     When I add <volume> ml of water
     Then I see that there is <volume> ml of water
     When I add <amount1> grams of <color1> powder
     When I add <amount2> grams of <color2> powder
     Then I see that there is <volume> ml of <color3> solution

    Examples: Equal amount of two basic colors:
      | color1  | amount1 | color2  | amount2 | color3  | volume  |
      | red     | 5       | blue    | 5       | purple  | 8       |
      | green   | 3       | red     | 3       | yellow  | 9       |
      | blue    | 4       | green   | 4       | cyan    | 8       |
