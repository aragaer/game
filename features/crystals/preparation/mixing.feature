# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
Feature: Mixing powder colors
  As a Crystals player
  I want to be able to mix powders
  So that I could create crystals of new colors

  Background: Game opened
    Given I go to the crystals game page

  Scenario Outline: Two colors mix
    Given I have <amount1> of <color1> powder
      And I have <amount2> of <color2> powder
      And I open an empty bowl
     When I add <amount1> grams of <color1> powder
      And I add <amount2> grams of <color2> powder
      And I add 100 ml of water
      And I start evaporation
     Then I have a bowl of <resulting color> solution

    Examples: Two basic:
      | color1  | amount1 | color2  | amount2 | resulting color |
      | red     | 5       | green   | 5       | yellow          |
      | green   | 5       | blue    | 5       | cyan            |
      | red     | 5       | blue    | 5       | purple          |
      | red     | 10      | green   | 1       | red             |

    Examples: Opposites:
      | color1  | amount1 | color2  | amount2 | resulting color |
      | red     | 5       | cyan    | 5       | gray            |
      | green   | 5       | purple  | 5       | gray            |
      | blue    | 5       | yellow  | 5       | gray            |
