# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
Feature: Powder list in preparation
  As a player
  I want to use all powders that I have
  So that I could grow different crystals

  Scenario Outline: Powder list in preparation
    Given I have <amount> of <color> powder
      And I go to the crystals game page
     When I open an empty bowl
     Then I see that I can use <amount> of <color> powder

    Examples: Basic powders:
      | color | amount  |
      | blue  | 10      |
      | red   | 6       |
      | green | 20      |

    Examples: Additional powders:
      | color   | amount  |
      | yellow  | 30      |
      | purple  | 40      |
      | cyan    | 25      |
      | gray    | 90      |
