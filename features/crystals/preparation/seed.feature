# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
Feature: Using crystal seed
  As a Crystals player
  I want to be able to use crystal seed
  So that I could grow bigger crystals

  Scenario Outline: Using same-color seed
    Given I have 1 <color> <size>-carat crystal
      And I have <amount> of <color> powder
      And I go to the crystals game page
     Then I have 1 <color> <size>-carat crystal
     When I open an empty bowl
      And I add <amount> grams of <color> powder
      And I use <color> <size>-carat crystal as seed
      And I add 100 ml of water
      And I start evaporation
      And I go to inventory
     Then I don't have the crystal
     When I wait for evaporation to complete
      And I go to laboratory
      And I click the collect button on process 1
      And I go to inventory
     Then I have <color> crystal of at least <size> carats

    Examples: Simple seed:
      | color | amount  | size	  |
      | red   | 5       | 3       |
      | green | 5       | 5       |
      | blue  | 5       | 6       |

  Scenario Outline: Mixing colors
    Given I have 1 <color1> <size>-carat crystal
      And I have <amount> of <color2> powder
      And I go to the crystals game page
      And I remember how much crystals I have
     When I open an empty bowl
      And I add <amount> grams of <color2> powder
      And I use <color1> <size>-carat crystal as seed
      And I add 100 ml of water
      And I start evaporation
      And I wait for evaporation to complete
      And I click the collect button on process 1
      And I go to inventory
     Then I have <color> crystal of at least <size> carats

    # Crystal has to grow to about double its size
    Examples: Two basic colors:
      | color1  | color2  | color   | amount  | size	|
      | red     | green   | yellow  | 9       | 4       |
      | green   | blue    | cyan    | 8       | 4       |
      | blue    | red     | purple  | 5       | 3       |

    # The following tests were resulting in incorrect crystal counts
    Examples: Bugs
      | color1  | color2  | color   | amount  | size    |
      | blue    | green   | cyan    | 25      | 84      |
      | blue    | yellow  | gray    | 25      | 84      |
      | green   | purple  | gray    | 25      | 68      |
