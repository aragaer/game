# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
@bypass_login
Feature: Cancel preparation
  As a player
  I want to be able to cancel preparation
  So that I could buy some additional powders

  Scenario Outline: Cancel
    Given I go to the crystals game page
      And I remember how much powders I have
      And I remember how much crystals I have
     When I open an empty bowl
      And I add <powder> grams of <color> powder
      And I add <water> ml of water
      And I click the 'Cancel' link
     Then I am redirected to laboratory
     When I go to inventory
     Then I have 3 empty bowls
      And I have spent 0 grams of <color> powder

    Examples: Cancelled process:
      | color | powder  | water |
      | blue  | 10      | 20    |
      | red   | 6       | 20    |
      | green | 10      | 40    |
