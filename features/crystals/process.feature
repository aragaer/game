# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@crystals
Feature: Evaporation process
  As a player
  I want to be able to track the evaporation process
  So that I could see my crystals growing

  Background: Registered
    Given database is reset
      And I am registered
      And I am logged in

  Scenario Outline: Bowl and powder usage
    Given I go to the crystals game page
      And I remember how much powders I have
      And I start a process:
      | water   | powder    | color   |
      | <water> | <powder>  | <color> |
     Then I have 2 empty bowls
      And I have a bowl of <color> solution
     When I go to inventory
     Then I have spent <powder> grams of <color> powder

    Examples: Powders to use:
      | color | powder  | water |
      | blue  | 10      | 20    |
      | red   | 6       | 20    |
      | green | 10      | 40    |

  Scenario Outline: Persistent process
    Given I remember how much powders I have
      And I start a process:
      | water   | powder    | color   |
      | <water> | <powder>  | <color> |
     When I logout
      And I login back
     Then I have spent <powder> grams of <color> powder
     When I access the path '/lab/'
     Then I have a bowl of <color> solution

    Examples: Simple processes:
      | water | powder  | color |
      | 10    | 6       | red   |
      | 4     | 8       | blue  |
