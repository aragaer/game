# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
@ui
@bypass_login
Feature: NavBar
  As a player
  I want a navigation bar
  So that I could easily access different pages

  Scenario:
    Given I resize my browser to 640x1024
      And I access the path '/'
     Then I see navigation bar
      And navigation toggle is labeled 'Menu'
