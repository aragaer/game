#!/usr/bin/python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

import logging

from pymongo import MongoClient
from selenium import webdriver
from tornado.ioloop import IOLoop

import app
from crystals import CrystalsGameClub, CrystalsRecreation
from crystals.document import AlchemistRegistry
from establishment.reception import Reception
from establishment.reception.books import BlackBoard, MongoBook
from tests import Server
from tests.openid import MockOpenIDHandler


def _open_chrome(chrome_options=None):
    if chrome_options is None:
        chrome_options = webdriver.ChromeOptions()
    chrome_options.add_experimental_option("excludeSwitches",
                                           ["ignore-certificate-errors"])
    return webdriver.Chrome(chrome_options=chrome_options)


BROWSER = _open_chrome


def before_all(context):
    selenium_logger = logging.getLogger(
        'selenium.webdriver.remote.remote_connection')
    selenium_logger.setLevel(logging.WARNING)

    def clear_db(db):
        MongoClient().drop_database(db)

    context.reset_database = lambda: clear_db(context.db)
    context.browser = BROWSER()


def before_feature(context, feature):
    context.server = Server()

    context.db = MongoClient()['db%d' % context.server.port]

    context.get_url = lambda path: \
        "http://localhost:%d%s" % (context.server.port, path)

    context.alchemist_registry = AlchemistRegistry(context.db)
    recreation = CrystalsRecreation(context.alchemist_registry)

    club = CrystalsGameClub(recreations=recreation,
                            cookie_secret='behave_cookie')
    if 'bypass_login' in feature.tags:
        club.reception.book = BlackBoard()
    else:
        from crystals.club_handlers import LoginHandler
        club.reception.book = MongoBook(context.db)
        club.add_handlers(r'.*', [(r'/openid/(.*)/', MockOpenIDHandler,
                                   {'port': context.server.port})])
        for provider, strings in LoginHandler.providers.items():
            LoginHandler.providers[provider] = (
                strings[0], context.get_url("/openid/%s/" % provider))

    context.reception = club.reception
    context.server.set_app(club)

    context.server.start()


def after_feature(context, feature):
    context.server.stop()
    context.reset_database()
    ioloop = IOLoop.instance()
    ioloop.add_callback(lambda x: x.stop(), ioloop)


def before_scenario(context, scenario):
    if 'bypass_login' in scenario.feature.tags:
        from crystals.alchemist import AlchemistRepo
        alchemist = AlchemistRepo(context.alchemist_registry).create()
        context.alchemist_id = alchemist.alchemist_id

        context.badge = context.reception.register()
        context.reception.take_note(context.badge,
                                    alchemist=context.alchemist_id)


def after_scenario(context, scenario):
    context.browser.delete_all_cookies()


def before_tag(context, tag):
    if tag == 'locale(ru)':
        options = webdriver.ChromeOptions()
        prefs = {'intl.accept_languages': 'ru-RU'}
        options.add_experimental_option('prefs', prefs)
        # This will mask any upper-level browser instance
        # FIXME: use 'start_session' instead
        context.browser = BROWSER(chrome_options=options)
        print("RU browser created")
    if 'browser' in context:
        context.browser.set_window_size(600, 800)


def after_tag(context, tag):
    if tag == 'locale(ru)':
        context.browser.close()
        context.browser.quit()
        print("RU browser closed")


def after_all(context):
    context.browser.close()
    context.browser.quit()
