#!/usr/bin/python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4


@given('I am registered using \'{provider}\' as \'{user}\'')
def step_impl(context, provider, user):
    from crystals.alchemist import AlchemistRepo
    context.execute_steps('''
        Given cookies are cleared
          And I access the path '/'
         When I click the '%s' link
          And I authorize as '%s'
    ''' % (provider, user))
    openid = context.get_url('/openid/%s-%s' % (context.provider, user))
    context.badge = context.reception.find(openid=openid)
    context.alchemist_id = context.reception.get_note(context.badge,
                                                      'alchemist')


@given('I am registered')
def step_impl(context):
    context.execute_steps('''
        Given I am registered using 'LiveJournal OpenID' as 'aragaer'
    ''')
