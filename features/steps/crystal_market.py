#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""Steps related to market in crystals game."""

from selenium.webdriver.common.keys import Keys

from crystals.market import coins_from_cash_string
from utils.selenium import find_element_by_label


@when('I go to market')
def step_impl(context):
    context.market = {
        'crystals': {},
        'powders': {},
        'upgrades': {},
    }
    context.execute_steps('''
        Given I access the path \'/market/\'
         When I check the price of upgrades
    ''')


@when('I go to powders shop')
def step_impl(context):
    context.market = {
        'crystals': {},
        'powders': {},
        'upgrades': {},
    }
    context.execute_steps('''
        Given I access the path \'/market/powders/\'
         When I check the price of powders
    ''')


@when('I go to jeweler')
def step_impl(context):
    context.market = {
        'crystals': {},
        'powders': {},
        'upgrades': {},
    }
    context.execute_steps('''
        Given I access the path \'/market/jeweler/\'
         When I check the price of crystals
    ''')


def find_crystal_for_sale(context, color, size):
    """Searches for crystal in 'Sale crystals' list."""
    crystals_list = context.browser.find_element_by_id('id_crystals')
    crystals = crystals_list.find_elements_by_tag_name('li')
    text_to_search = '%s %d-carat crystal' % (color.capitalize(), size)
    for row in crystals:
        if row.text.startswith(text_to_search):
            return row
    assert False, "Failed to find %s %d-carat crystal in market" % (color,
                                                                    size)


def get_market_powder(browser, color):
    powders_table = browser.find_element_by_id('id_powders')
    for row in powders_table.find_elements_by_tag_name('li'):
        cols = row.find_elements_by_tag_name('span')
        if cols[0].text.lower() == color:
            return cols
    assert False, "Failed to find a %s powder on market" % color


def get_market_upgrade(browser, name):
    upgrades_table = browser.find_element_by_id('id_upgrades')
    for row in upgrades_table.find_elements_by_tag_name('li'):
        cols = row.find_elements_by_tag_name('span')
        if cols[0].text == name:
            return cols
    assert False, "Failed to find upgrade '%s' on market" % name


def get_item_price(elem, per_gram=False):
    """Fetches item price from row."""
    cols = elem.find_elements_by_tag_name('span')
    price_str = cols[-1].text
    assert price_str.startswith('@ ')
    return coins_from_cash_string(price_str[2:-4]
                                  if per_gram
                                  else price_str[2:])


@when('I check the price of {stuff}')
def step_impl(context, stuff):
    ids = {
        'crystals': 'id_crystals',
        'powders': 'id_powders',
        'upgrades': 'id_upgrades',
    }
    crystals_list = context.browser.find_element_by_id(ids[stuff])
    crystals = crystals_list.find_elements_by_tag_name('li')
    for row in crystals:
        inp = row.find_elements_by_tag_name('input')
        if not inp:
            continue
        desc = row.find_elements_by_tag_name('span')[0].text.lower()
        context.market[stuff][desc] = {'price': get_item_price(row),
                                       'amount': 0}


@when('I sell {count:d} {color} {size:d}-carat crystal')
@when('I sell {count:d} {color} {size:d}-carat crystals')
def step_impl(context, count, color, size):
    context.execute_steps('''
        When I add %d %s %d-carat crystals to sale
        And I click the 'Trade' button
    ''' % (count, color, size))


@when('I add {count:d} {color} {size:d}-carat crystal to sale')
@when('I add {count:d} {color} {size:d}-carat crystals to sale')
def step_impl(context, count, color, size):
    elem = find_crystal_for_sale(context, color, size)
    sell_field = elem.find_elements_by_tag_name('input')[0]
    sell_field.send_keys(Keys.CONTROL+"a")
    sell_field.send_keys(str(count))
    crystal_id = '%s %d-carat crystal' % (color, size)
    context.market['crystals'][crystal_id]['amount'] = count


@then('I {will_or_have} {spent_or_earned} the correct amount of money')
def step_impl(context, will_or_have, spent_or_earned):

    def total(category):
        return sum(i['price']*i['amount'] for i in category.values())

    expected = \
        + total(context.market['crystals']) \
        - total(context.market['powders']) \
        - total(context.market['upgrades'])
    if will_or_have == 'will':
        if expected > 0:
            label = 'You will get'
        else:
            label = 'You will pay'
            expected *= -1
    else:
        label = 'Cash:'
        expected += context.coins
    print("Searching for", label, expected)
    total_field = find_element_by_label(context.browser, label)
    coins = coins_from_cash_string(total_field.text)
    assert coins == expected, "Expected %d, got %d" % (expected, coins)


@when('I buy {amount:d} of {color} powder')
def step_impl(context, amount, color):
    context.execute_steps('''
        When I add %dg of %s powder to cart
        And I click the 'Trade' button
    ''' % (amount, color))


@when('I add {amount:d}g of {color} powder to cart')
def step_impl(context, amount, color):
    cols = get_market_powder(context.browser, color)
    powder_input = cols[1].find_elements_by_tag_name('input')[0]
    powder_input.send_keys(Keys.CONTROL+"a")
    powder_input.send_keys(str(amount))
    context.market['powders'][color]['amount'] = amount


@then('I can sell {count:d} {color} {size:d}-carat crystal')
@then('I can sell {count:d} {color} {size:d}-carat crystals')
def step_impl(context, count, color, size):
    elem = find_crystal_for_sale(context, color, size)
    text_to_compare = '%s %d-carat crystal' % (color.capitalize(), size)
    if count != 1:
        text_to_compare += ' x%d' % count
    text_to_compare += ' @'
    assert elem.text.startswith(text_to_compare), \
        "Expected %d crystals, got [%s]" % (count, elem.text)


@when('I buy a bowl')
def step_impl(context):
    context.execute_steps('''
        When I add 1 bowl to cart
        And I click the 'Trade' button
    ''')


@when('I add {count:d} bowl to cart')
@when('I add {count:d} bowls to cart')
def step_impl(context, count):
    cols = get_market_upgrade(context.browser, "Additional bowl")
    bowl_input = cols[1].find_elements_by_tag_name('input')[0]
    bowl_input.send_keys(Keys.CONTROL+"a")
    bowl_input.send_keys(str(count))
    context.market['upgrades']['additional bowl']['amount'] = count


@then('all crystals are put for sale')
def step_impl(context):
    crystals_list = context.browser.find_element_by_id('id_crystals')
    crystals = crystals_list.find_elements_by_tag_name('li')
    for row in crystals:
        inp = row.find_elements_by_tag_name('input')
        if inp:
            inp = inp[0]
            total = int(inp.get_attribute('max'))
            for_sale = int(inp.get_attribute("value"))
            assert total == for_sale, "Expected %d for sale, got %d" % (
                total, for_sale)
            desc = row.find_elements_by_tag_name('span')[0].text.lower()
            context.market['crystals'][desc]['amount'] = total


@when('I click the \'{link}\' near {color} {size:d}-carat crystal')
def step_impl(context, link, color, size):
    row = find_crystal_for_sale(context, color, size)
    link = row.find_element_by_link_text(link)
    link.click()


@then('amount of {color} {size:d}-carat crystals set for sale is {count:d}')
def step_impl(context, color, size, count):
    row = find_crystal_for_sale(context, color, size)
    inp = row.find_elements_by_tag_name('input')[0]
    for_sale = int(inp.get_attribute("value"))
    assert count == for_sale, "Expected %d for sale, got %d" % (count,
                                                                for_sale)
    crystal_id = '%s %d-carat crystal' % (color, size)
    context.market['crystals'][crystal_id]['amount'] = count
