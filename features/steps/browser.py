#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4


@given('I resize my browser to {width:d}x{height:d}')
def step_impl(context, width, height):
    context.browser.set_window_size(width, height)
