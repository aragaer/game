#!/usr/bin/python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

from selenium.common.exceptions import ElementNotVisibleException, \
    NoSuchElementException


def get_navigation(browser):
    return browser.find_element_by_xpath('//div[@role="navigation"]')


def expand_navbar(navigation):
    try:
        navigation.find_element_by_link_text('Settings')
    except NoSuchElementException:
        from time import sleep
        navigation.find_element_by_xpath(
            '//button[@class="navbar-toggle"]').click()
        sleep(0.25)


@then('I see the following navigation items')
def step_impl(context):
    navigation = get_navigation(context.browser)
    for row in context.table:
        try:
            navigation.find_element_by_link_text(row['title'])
        except NoSuchElementException:
            assert False, "Failed to find " + row['title']


@then('I see the \'Logout\' button')
def step_impl(context):
    navigation = get_navigation(context.browser)
    buttons = navigation.find_elements_by_tag_name('button')
    assert 'Logout' in [btn.text for btn in buttons]


@then('\'{item}\' navigation item is active')
def step_impl(context, item):
    navigation = get_navigation(context.browser)
    link = navigation.find_element_by_link_text(item)
    nav_item = link.find_element_by_xpath('..')
    assert nav_item.get_attribute('class') == 'active'


@then('navigation bar is collapsed')
def step_impl(context):
    navigation = get_navigation(context.browser)
    try:
        settings = navigation.find_element_by_link_text('Settings')
        assert not settings.is_displayed()
    except NoSuchElementException:
        # this works too
        pass


@when('I click the navigation toggle')
def step_impl(context):
    navigation = get_navigation(context.browser)
    navigation.find_element_by_xpath(
        '//button[@class="navbar-toggle"]').click()


@then('navigation bar is expanded')
def step_impl(context):
    from time import sleep
    sleep(0.25)
    navigation = get_navigation(context.browser)
    navigation.find_element_by_link_text('Settings')


@when('I expand navbar')
def step_impl(context):
    expand_navbar(get_navigation(context.browser))


@when('I click \'{what}\' in navigation bar')
def step_impl(context, what):
    navigation = get_navigation(context.browser)
    expand_navbar(navigation)
    context.browser.find_element_by_link_text(what).click()


@then('I see navigation bar')
def step_impl(context):
    get_navigation(context.browser)


@then('navigation toggle is labeled \'{label}\'')
def step_impl(context, label):
    button = context.browser.find_element_by_xpath(
        '//button[@class="navbar-toggle"]')
    assert button.text.strip() == label, "Expected %s, got %s" % (label,
                                                                  button.text)
