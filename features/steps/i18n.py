#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

from selenium.common.exceptions import NoSuchElementException


def get_language_switch(browser):
    try:
        lang_list = browser.find_element_by_id('id_languages')
    except NoSuchElementException:
        assert False, "Language list not found"
    return lang_list


@then('I see the following language options')
def step_impl(context):
    lang_list = get_language_switch(context.browser)
    items = [row['language'] for row in context.table]
    current_lang = lang_list.find_element_by_tag_name('button')
    items.remove(current_lang.text.strip())

    current_lang.click()

    for item, row in zip(items,
                         lang_list.find_elements_by_tag_name('li')):
        assert item == row.text, "Expected %s, got %s" % (
            item, row.text)


@then('selected language is {lang}')
def step_impl(context, lang):
    lang_list = get_language_switch(context.browser)
    current_lang = lang_list.find_element_by_tag_name('button')
    assert current_lang.text.strip() == lang, \
        "Expected %s, got %s" % (lang, current_lang.text)


@then('I can switch to following languages')
def step_impl(context):
    try:
        lang_list = context.browser.find_element_by_id('id_languages')
    except NoSuchElementException:
        assert False, "Language list not found"
    languages_to_search = [l[0] for l in context.table.rows]
    for item in lang_list.find_elements_by_tag_name('li'):
        if item.text in languages_to_search:
            try:
                link = item.find_element_by_tag_name('a')
            except NoSuchElementException:
                assert False, "No clickable link in %s" % item.text
            assert link.text == item.text, \
                "Lang name is not clickable for %s" % item.text


@then('I can switch to language {lang}')
def step_impl(context, lang):
    try:
        lang_list = context.browser.find_element_by_id('id_languages')
    except NoSuchElementException:
        assert False, "Language list not found"
    try:
        lang = lang_list.find_element_by_link_text(lang)
    except NoSuchElementException:
        assert False, "Language %s list is not found or not clickable" % lang


@then('I see greeting in Russian')
def step_impl(context):
    try:
        greeting = context.browser.find_elements_by_tag_name('h1')[0]
    except (NoSuchElementException, IndexError):
        assert False, "Greeting not found"

    assert greeting.text.startswith("Добро пожаловать"), greeting.text


@then('I see that page title is \'{title}\'')
def step_impl(context, title):
    assert context.browser.title.endswith(title), \
        "Expected '%s', got '%s'" % (title, context.browser.title)


@then('I see the following market categories')
def step_impl(context):
    labels = [el.text for el in
              context.browser.find_elements_by_xpath(
                  '//div[@class="panel-heading"]')]
    for category in [r[0] for r in context.table.rows]:
        assert category in labels, "Category %s not found" % category


@then('I see the following {} categories')
def step_impl(context, _):
    labels = [el.text
              for el in context.browser.find_elements_by_tag_name('label')]
    for category in [r[0]+':' for r in context.table.rows]:
        assert category in labels, "Category %s not found" % category


@then('I see the following links')
def step_impl(context):
    for link, label in context.table.rows:
        try:
            element = context.browser.find_element_by_link_text(label)
        except NoSuchElementException:
            assert False, "Link labeled %s not found" % label
        url = element.get_attribute('href')
        assert url == context.get_url('/%s/' % link if link else '/'), \
            "Expected link %s to point to %s, got %s instead" % (label, link,
                                                                 url)


@then('wallet is labeled \'{label}\'')
def step_impl(context, label):
    try:
        element = context.browser.find_element_by_xpath(
            '//label[@for="id_wallet"]')
    except NoSuchElementException:
        assert False, "Wallet label not found"
    assert element.text == label+':', "Expected %s, got %s" % (label,
                                                               element.text)


@when('I open the language dropdown')
def step_impl(context):
    lang_list = get_language_switch(context.browser)
    lang_list.find_element_by_tag_name('button').click()


@then('I see the following shops')
def search_shops(context):
    shops = context.browser.find_element_by_id('id_market_tabs')
    labels = [el.text for el in shops.find_elements_by_tag_name('li')]
    assert all(r[0] in labels for r in context.table.rows)
