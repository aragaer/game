#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4


from behave.matchers import register_type

from crystals.config import MOLECULES_PER_GRAM


register_type(carat=lambda text: float(text)/5)


@given('I have {count:d} {color} {size:carat}-carat crystal')
@given('I have {count:d} {color} {size:carat}-carat crystals')
def step_impl(context, count, color, size):
    from crystals.document import CrystalDocument
    size *= MOLECULES_PER_GRAM
    context.crystal = CrystalDocument({'color': color, 'size': size,
                                       'count': count})
    document = context.alchemist_registry.fetch(context.alchemist_id)
    document.crystals.append(context.crystal)
    context.alchemist_registry.save(document)


@given('I have {gold:d}G, {silver:d}S and {copper:d}C')
def step_impl(context, gold, silver, copper):
    doc = context.alchemist_registry.fetch(context.alchemist_id)
    doc.coins = (gold * 100 + silver) * 100 + copper
    context.alchemist_registry.save(doc)
    print("Saved", doc.data)


@given('I have {amount:d} of {color} powder')
def step_impl(context, amount, color):
    amount *= MOLECULES_PER_GRAM
    document = context.alchemist_registry.fetch(context.alchemist_id)
    for powder in document.powders:
        if powder.color == color:
            powder.mass = amount
            break
    else:
        from crystals.document import PowderDocument
        document.powders.append(PowderDocument({'color': color,
                                                'mass': amount}))
    context.alchemist_registry.save(document)


@given('bowl {_} contains {count:d} {color} {size:carat}-carat crystals')
def step_impl(context, _, count, color, size):
    from crystals.document import ProcessDocument
    from crystals.process import Process, Sediment, Solution
    from crystals.color import Color
    from crystals.utilities import molecules_from_grams
    from datetime import datetime
    document = context.alchemist_registry.fetch(context.alchemist_id)
    process = Process(sediment=Sediment({molecules_from_grams(size): count},
                                        color=Color.from_name(color)),
                      solution=Solution(Color.from_name(color), 0, 0),
                      end_time=datetime.now())
    document.processes.append(ProcessDocument.from_process(process))
    document.bowls -= 1
    context.alchemist_registry.save(document)


@given('I have a bowl with {color} solution')
def step_impl(context, color):
    context.execute_steps('''
        Given I have 5 of {0} powder
          And I start a process:
            | water | amount | color |
            | 5     | 5      | {0}   |
        '''.format(color))


@given('I have no crystals')
def step_impl(context):
    document = context.alchemist_registry.fetch(context.alchemist_id)
    document.crystals = []
    context.alchemist_registry.save(document)
