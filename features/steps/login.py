#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException, \
    NoSuchElementException


@given('I login using {provider} as {user_id}')
def step(context, provider, user_id):
    context.execute_steps('''
        Given I access the path '/'
         When I click the 'Login' link
          And I click the '%s' link
          And I authorize as '%s'
    ''' % (provider, user_id))


@then('I am logged in as \'{name}\'')
def step(context, name):
    context.execute_steps('''
         Then I see the '%s' greeting
          And I see the 'Logout' link
          But I don't see the 'Login' link
    ''' % name)


@then('I am logged out')
def step(context):
    context.execute_steps('''
         Then I see the 'unknown user' greeting
          And I see the 'Login' link
          But I don't see the 'Logout' link
    ''')
