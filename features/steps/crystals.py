#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

from datetime import timedelta
import parse

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.common.exceptions import TimeoutException, \
    NoSuchElementException
from behave.matchers import register_type

from crystals.config import MOLECULES_PER_GRAM
from crystals.market import coins_from_cash_string

from utils.selenium import find_element_by_label


@given('I go to the crystals game page')
def step_impl(context):
    context.execute_steps('Given I access the path \'/\'')


@then('I see a field to enter water amount')
def step_impl(context):
    context.execute_steps('Then I see field labeled \'Water:\'')


@when('I add {mass:d} grams of {color} powder')
def step_impl(context, mass, color):
    try:
        powder_table = find_element_by_label(context.browser, 'Powders:')
    except NoSuchElementException:
        assert False, "Failed to find powder list"

    for row in powder_table.find_elements_by_tag_name('li')[1:]:
        fields = row.find_elements_by_tag_name('span')
        if fields[1].text == color:
            amount = int(fields[0].text[:-1])
            assert amount >= mass, "Not enough %s powder: got %d need %d" % (
                color, amount, mass)
            powder_input = fields[2].find_elements_by_tag_name('input')[0]
            powder_input.send_keys(Keys.CONTROL+"a")
            powder_input.send_keys(mass)
            return
    assert False, "Failed to find %s powder" % color


@when('I add {volume:d} ml of water')
def step_impl(context, volume):
    context.execute_steps("When I type '%d' to a field labeled 'Water:'"
                          % volume)


@when('I start evaporation')
def step_impl(context):
    context.execute_steps('When I click \'Start evaporating\'')


crystal_parser1 = parse.compile("{color} {mass:carat}-carat crystal",
                                dict(carat=lambda text: float(text)/5))
crystal_parser = parse.compile(
    "{color} {mass:carat}-carat crystal x{count:d}",
    dict(carat=lambda text: float(text)/5))
powder_parser = parse.compile("{mass:g}g of {color} powder")


@then('I see {volume:d} ml of {color} solution')
def step_impl(context, volume, color):
    try:
        desc = find_element_by_label(context.browser, "Solution:")
    except NoSuchElementException:
        assert False, "Failed to find solution description"

    solution = desc.find_elements_by_tag_name('li')[0].text
    expected = "%dml of %s solution." % (volume, color)
    assert solution == expected, "Expected [%s], got [%s]" % (volume, color)


@then('all crystals are {color}')
def step_impl(context, color):
    for crystal in context.crystals:
        assert crystal[0] == color, "Found %s crystal instead of %s" % \
            (crystal[0], color)
    if context.powder is not None:
        assert context.powder['color'] == color, \
            "Found %s powder instead of %s" % (context.powder['color'], color)


@then('total mass of crystals is {mass:d} grams')
def step_impl(context, mass):
    actual = sum(m*c for (_, m), c in context.crystals.items())
    if context.powder is not None:
        actual += int(context.powder['mass'])
    assert actual == mass, \
        "Expected %f grams, got %f" % (mass, actual)


@then('I don\'t see a list of crystals')
def step_impl(context):
    try:
        crystals = find_element_by_label(context.browser, "Result:")
    except NoSuchElementException:
        return
    assert False, "Found list of crystals when no crystals are expected"


@step('I am logged in')
def step_impl(context):
    w = WebDriverWait(context.browser, 5)
    url = context.get_url('/')
    try:
        w.until(lambda d: d.current_url == url)
    except TimeoutException:
        assert False, "I am not logged in"


@step('I open an empty bowl')
def open_empty(context):
    context.execute_steps("When I access the path '/preparation/'")


@when('I wait for evaporation to complete')
def step_impl(context):
    from datetime import datetime

    # rewinding time now
    doc = context.alchemist_registry.fetch(context.alchemist_id)
    assert len(doc.processes) == 1,\
        "Expected exactly one running process, got %d" % len(doc.processes)
    doc.processes[0].end_time = datetime.now()
    context.alchemist_registry.save(doc)
    context.browser.refresh()


page_titles = {
    'inventory': 'Inventory',
    'market': 'Market',
    'preparation': 'Preparation',
    'process': 'Process',
    'result': 'Result',
    'laboratory': 'Laboratory',
    '404': '404: Not Found',
    'powder shop': 'Powders shop',
    'jeweler': 'Jeweler',
}


@then('I am redirected to {page}')
def step_impl(context, page):
    w = WebDriverWait(context.browser, 1)
    try:
        w.until(lambda d: page_titles[page] in d.title)
    except TimeoutException:
        assert False, "I am not redirected to %s, stay at %s" % (
            page, context.browser.title)


@then('I have the following powders')
def step_impl(context):
    data = context.table
    context.execute_steps('Given I remember how much powders I have')

    for row in data:
        color = row['color']
        expected = float(row['amount'])
        assert color in context.owned, "No %s powder found" % color
        assert context.owned[color] == expected, \
            "Expected %f of %s powder, got %f" % \
            (expected, color, context.owned[color])


@given('I remember how much powders I have')
def step_impl(context):
    context.owned = count_powders(context.browser)


def count_powders(browser):
    powders_list = browser.find_element_by_id('powders_list')
    result = {}
    for powder in powders_list.find_elements_by_tag_name('li'):
        try:
            desc = powder_parser.parse(powder.text).named
        except AttributeError as ex:
            assert False, "Failed to parse [%s] as a powder" % powder.text
        result[desc['color']] = desc['mass']
    return result


@given('I remember how much crystals I have')
def step_impl(context):
    context.owned_crystals = count_crystals(context.browser)


def count_crystals(browser):
    try:
        crystals_list = find_element_by_label(browser, 'Crystals:')
    except NoSuchElementException:
        assert False, "Failed to find label '%s'" % label
    crystals = crystals_list.find_elements_by_tag_name('li')
    result = {}
    powder = None
    for crystal in crystals:
        text = crystal.text.lower()
        match = crystal_parser.parse(text)
        if match is None:
            match = crystal_parser1.parse(text)
        if match is None:
            try:
                powder = powder_parser.parse(text).named
            except AttributeError as ex:
                assert False, "Failed to parse [%s]" % text
        else:
            desc = match.named
            assert (desc['color'], desc['mass']) not in result, \
                "%s %f crystals are not grouped" % (desc['color'],
                                                    desc['mass'])
            if 'count' in desc:
                assert desc['count'] > 0, \
                    "Found crystal of incorrect count: %s" % text
                result[(desc['color'], desc['mass'])] = desc['count']
            else:
                result[(desc['color'], desc['mass'])] = 1
    return result, powder


def _count_empty_bowls(browser):
    return int(_get_new_process(browser).text.strip()[len('Empty bowl x'):])


@then('{count:d} bowl is empty')
@then('{count:d} bowls are empty')
def step_impl(context, count):
    empty = _count_empty_bowls(context.browser)
    assert empty == count, \
        "Expected %d empty bowls, got %d" % (count, empty)


@when('I go to inventory')
def step_impl(context):
    context.execute_steps("When I access the path '/'")


@when('I go to laboratory')
def step_impl(context):
    context.execute_steps("When I access the path '/lab/'")


@then('I have spent {mass:d} grams of {color} powder')
def step_impl(context, mass, color):
    powders = count_powders(context.browser)
    if color in powders:
        spent = context.owned[color] - powders[color]
    else:
        spent = context.owned[color]
    assert spent == mass, \
        "Expected to spend %d, spent %d of %d of %s powder" % \
        (mass, spent, context.owned[color], color)


@given('I start a process')
def step_impl(context):
    # assuming order of cells is water-amount-color
    context.execute_steps('''
        When I access the path '/preparation/'
         And I add %s ml of water
         And I add %s grams of %s powder
         And I start evaporation
    ''' % tuple(context.table[0]))


@when('I login back')
def step_impl(context):
    context.execute_steps('''
         When I click the '%s' link
         Then I am logged in
    ''' % 'LiveJournal OpenID')


@then('I see all the crystals I have grown')
def step_impl(context):
    new_crystals, new_powder = count_crystals(context.browser)
    for crystal, count in context.crystals.items():
        assert crystal in new_crystals, \
            "Crystal %s %d was collected but is not in result" % crystal
        new = new_crystals[crystal]
        if crystal in context.owned_crystals:
            owned = context.owned_crystals[crystal]
            assert owned + count == new, \
                "Had %d, grown %d, expected %d, got %d" % \
                (owned, count, owned + count, new)
        else:
            assert new == count, "Had 0, grown %d, expected %d, got %d" % \
                (count, count, new)


@then('I see collected powder')
def step_impl(context):
    new_powder = count_powders(context.browser)
    if context.powder is None:
        color = context.active_outline['color']
        mass = 0
    else:
        color = context.powder['color']
        mass = context.powder['mass']
        assert color in new_powder, \
            "Powder %s was collected but is not in result" % color
    new_mass = new_powder[color] if color in new_powder else 0
    spent_mass = context.owned[color] - new_mass
    crystal_mass = sum(m*c for (_, m), c in context.crystals.items())
    assert spent_mass == crystal_mass, \
        "Expected to see %f grams of %s powder, got %f" % \
        (context.owned[color] - crystal_mass, color, new_mass)


@when('I check my wallet')
def step_impl(context):
    wallet = context.browser.find_element_by_id('id_wallet')
    context.coins = coins_from_cash_string(wallet.text)


@then('I see the same amount of money')
def step_impl(context):
    wallet = context.browser.find_element_by_id('id_wallet')
    coins = coins_from_cash_string(wallet.text)
    assert context.coins == coins, \
        "Expected %d coins, got %s" % (context.coins, coins)


@then('I have {gold:d}G, {silver:d}S and {copper:d}C')
def step_impl(context, gold, silver, copper):
    coins = coins_from_cash_string("%dG %dS %dC" % (gold, silver, copper))
    wallet = context.browser.find_element_by_id('id_wallet')
    actual_coins = coins_from_cash_string(wallet.text)
    assert coins == actual_coins, "Expected %d coins, got %d" % (
        coins, actual_coins)


@then('I have gained {mass:d} of {color} powder')
def step_impl(context, mass, color):
    powders = count_powders(context.browser)
    gained = powders[color]
    if color in context.owned:
        gained -= context.owned[color]
    assert gained == mass, \
        "Expected to gain %d, gained %d of %s powder" % \
        (mass, gained, color)


register_type(carat=lambda text: float(text)/5)


@then('I don\'t have the crystal')
def step_impl(context):
    from crystals.utilities import carats_from_molecules
    crystals_list = context.browser.find_element_by_id('crystals_list')
    crystals = crystals_list.find_elements_by_tag_name('li')
    color = context.crystal.color
    size = context.crystal.size
    text_to_compare = '%s %d-carat crystal' % (color.capitalize(),
                                               carats_from_molecules(size))
    assert all(not c.text.startswith(text_to_compare) for c in crystals), \
        "Found %s in inventory" % text_to_compare


@given('I remember how much bowls I have')
@when('I check my bowls')
@when('I count my bowls')
def step_impl(context):
    context.bowls = _count_empty_bowls(context.browser)


@then('I have {count:d} more bowl')
@then('I have {count:d} more bowls')
def step_impl(context, count):
    old_count = context.bowls
    new_count = _count_empty_bowls(context.browser)
    assert old_count + count == new_count, \
        "Expected %d+%d processes, got %d" % \
        (old_count, count, new_count)


@then('I have same amount of bowls')
def step_impl(context):
    context.execute_steps('Then I have 0 more bowls')


def time_to_seconds(time, fields=None):
    if fields is None:
        fields = ['minutes', 'seconds']
    delta_dict = dict((m, int(t)) for t, m in zip(time.split(':'), fields))
    return timedelta(**delta_dict).total_seconds()


@then('process will take {time}')
def step_impl(context, time):
    try:
        solution = find_element_by_label(context.browser, "Solution:")
    except NoSuchElementException:
        assert False, "Failed to find solution description"
    if context.browser.title == page_titles['process']:
        time_text = solution.find_elements_by_tag_name('li')[1].text
        assert time_text.startswith('Evaporation will end in ')
        time_string = time_text[24:-1]
    elif context.browser.title == page_titles['preparation']:
        time_text = solution.find_elements_by_tag_name('li')[2].text
        assert time_text.startswith('Evaporation will take ')
        time_string = time_text[22:]
    else:
        assert False, "Incorrect page: %s" % context.browser.title

    if time.startswith('a bit less than '):
        seconds = time_to_seconds(time[16:], ['hours', 'minutes', 'seconds'])-2
    else:
        seconds = time_to_seconds(time, ['hours', 'minutes', 'seconds'])

    t = time_string.split(' ')
    if t[0] == 'about':
        t = t[1:]
    delta_dict = dict((m, int(t)) for t, m in zip(t[::2], t[1::2]))
    seconds2 = timedelta(**delta_dict).total_seconds()
    assert abs(seconds - seconds2) < 3, \
        "Expected %s (%d), got %s (%d)" % (time, seconds, time_string,
                                           seconds2)


@when('I rewind time by {time}')
def step_impl(context, time):
    delta_dict = dict((m, int(t)) for t, m in
                      zip(time.split(':'), ['hours', 'minutes', 'seconds']))

    # add half a second grace time
    delta_dict['seconds'] = delta_dict.get('seconds', 0) - 0.5

    delta = timedelta(**delta_dict)
    doc = context.alchemist_registry.fetch(context.alchemist_id)
    for process in doc.processes:
        if process.end_time is not None:
            process.end_time -= delta
    context.alchemist_registry.save(doc)


@then('I see the following error message')
def step_impl(context):
    error = context.browser.find_element_by_id('id_error')
    assert error.text.strip() == context.text.strip()


@then('there is at least {num:d} crystal')
@then('there is at least {num:d} crystals')
def step_impl(context, num):
    assert len(context.crystals) >= num, \
        "Expected at least %d crystals, got %d" % (num, len(context.crystals))


@then('there are no crystals')
def step_impl(context):
    assert len(context.crystals) == 0, \
        "Expected no crystals, got %d" % len(context.crystals)


@then('I see that I can use {amount:d} of {color} powder')
def step_impl(context, amount, color):
    try:
        powder_table = find_element_by_label(context.browser, 'Powders:')
    except NoSuchElementException:
        assert False, "Failed to find powder list"

    for row in powder_table.find_elements_by_tag_name('li')[1:]:
        fields = row.find_elements_by_tag_name('span')
        if fields[1].text == color:
            available = int(fields[0].text[:-1])
            assert amount == available, "Expected %d of %s powder, got %d" % (
                amount, color, available)
            return
    assert False, "Failed to find %s powder" % color


@then('I see that there is no solution')
def step_impl(context):
    solution_desc = find_element_by_label(context.browser, 'Solution:')
    solution_rows = solution_desc.find_elements_by_tag_name('li')

    assert len(solution_rows) == 1
    assert solution_rows[0].text == 'Bowl is empty'


@then('I see that there is {volume:d} ml of water')
def step_impl(context, volume):
    solution_desc = find_element_by_label(context.browser, 'Solution:')
    solution_rows = solution_desc.find_elements_by_tag_name('li')

    assert len(solution_rows) == 1
    assert solution_rows[0].text == '%d ml of water' % volume, \
        "Expected %d ml of water, got [%s]" % (volume,
                                               solution_rows[0].text)


@then('I see that there is {volume:d} ml of {color} solution')
def step_impl(context, volume, color):
    solution_desc = find_element_by_label(context.browser, 'Solution:')
    solution_rows = solution_desc.find_elements_by_tag_name('li')

    expected_text = "%d ml of %s solution" % (volume, color)
    actual_text = solution_rows[0].text
    assert actual_text == expected_text, \
        "Expected [%s], got [%s]" % (expected_text, actual_text)


@then('concentration is {high_or_low}')
def step_impl(context, high_or_low):
    solution_desc = find_element_by_label(context.browser, 'Solution:')
    solution_rows = solution_desc.find_elements_by_tag_name('li')

    expected_text = "Concentration: %s" % high_or_low
    actual_text = solution_rows[1].text
    assert actual_text.startswith(expected_text), \
        "Expected [%s], got [%s]" % (expected_text, actual_text)


@then('optimal is at {volume:d}')
def step_impl(context, volume):
    solution_desc = find_element_by_label(context.browser, 'Solution:')
    solution_rows = solution_desc.find_elements_by_tag_name('li')

    expected_text = "(optimal at %d ml)" % volume
    actual_text = solution_rows[1].text
    assert actual_text.endswith(expected_text), \
        "Expected [%s], got [%s]" % (expected_text, actual_text)


@when('I collect from bowl {bowl:d}')
def step_impl(context, bowl):
    context.execute_steps('''
        When I access the path '/lab/'
        When I click the collect button on process %d
    ''' % bowl)


@when('I use {crystal} as seed')
def step_impl(context, crystal):
    try:
        seed_select = find_element_by_label(context.browser, 'Seed:')
    except NoSuchElementException:
        assert False, "Failed to find seed list"

    Select(seed_select).select_by_visible_text(crystal.capitalize())


@then('I have {color} crystal of at least {size:carat} carats')
def step_impl(context, color, size):
    context.crystals, context.powder = count_crystals(context.browser)
    assert any(c == color and s >= size for c, s in context.crystals), \
        "Failed to find %s crystal of at least %g size" % (color, size)


@then('I {can_or_cant} start the process')
def step_impl(context, can_or_cant):
    expect_disabled = 'true' if can_or_cant == 'can\'t' else None
    try:
        button = context.browser.find_element_by_xpath(
            '//input[@type="submit" and @value="Start evaporating"]')
    except NoSuchElementException:
        assert False, "Failed to find 'Start evaporating' button" % button

    assert button.get_attribute('disabled') == expect_disabled


def _xpath_contains(attr, val):
    return 'contains(concat(" ", normalize-space(@%s), " "), " %s ")' \
        % (attr, val)


def _get_lab_processes(browser):
    return browser.find_elements_by_xpath(
        '//div[%s]' % _xpath_contains('class', 'process'))


def _get_new_process(browser):
    return browser.find_element_by_xpath(
        '//div[%s]' % _xpath_contains('class', 'new-process'))


@then('I have a running process')
def find_process(context):
    context.process = _get_lab_processes(context.browser)[0]


@then('solution color is {color}')
def check_solution_color(context, color):
    assert context.process.text.startswith(color.capitalize()), \
        "Expected %s, got [%s]" % (color, context.process.text)


@then('I have no running processes')
def step_impl(context):
    processes = _get_lab_processes(context.browser)
    assert not processes, "Expected 0 processes, got %d" % len(processes)


@then('I have {count} empty bowls')
def empty_bowls_count(context, count):
    new_process = _get_new_process(context.browser)
    text = new_process.text.strip()
    expected = 'Empty bowl x%d' % int(count)
    assert text == expected, "Expected %s bowls, got [%s]" % (count, text)


@when('I click the plus button on new process')
def click_new_process(context):
    new_process = _get_new_process(context.browser)
    new_process.find_element_by_tag_name('button').click()


@then('I have a bowl of {color} solution')
def bowl_of_solution(context, color):
    def _is_color_solution(process):
        try:
            process.find_element_by_xpath(
                '//div[%s]' % _xpath_contains('class', 'process_time'))
        except NoSuchElementException:
            return False
        desc = process.find_element_by_xpath(
            'div[%s]' % _xpath_contains('class', 'process_desc'))
        return desc.text.strip().lower() == color
    assert(any(_is_color_solution(p)
           for p in _get_lab_processes(context.browser)))


@then('I have a bowl of {color} crystals')
def bowl_of_solution(context, color):
    def _is_color_solution(process):
        try:
            process.find_element_by_xpath(
                '//div[%s]' % _xpath_contains('class', 'process_time'))
            return False
        except NoSuchElementException:
            pass    # Good - we don't need it here
        desc = process.find_element_by_xpath(
            'div[%s]' % _xpath_contains('class', 'process_desc'))
        return desc.text.strip().lower() == color
    assert(any(_is_color_solution(p)
           for p in _get_lab_processes(context.browser)))


@when('I click the {button} button on process {number:d}')
def cancel_process(context, button, number):
    process = _get_lab_processes(context.browser)[number-1]
    process.find_element_by_xpath(
        '//button[%s]' % _xpath_contains('class', button)).click()


@then('I have {count:d} {crystal} crystal')
@then('I have {count:d} {crystal} crystals')
def step_impl(context, count, crystal):
    crystals_list = context.browser.find_element_by_id('crystals_list')
    crystals = crystals_list.find_elements_by_tag_name('li')
    text_to_compare = crystal.capitalize() + ' crystal'
    if count != 1:
        text_to_compare += ' x%d' % count
    assert any(row.text == text_to_compare for row in crystals), \
        "Failed to find %s in inventory" % crystal


_TABS = {'Powders': 'id_powder_shop',
         'Jeweler': 'id_jeweler_shop'}


@when('I click the \'{tab}\' tab')
def click_powders_tab(context, tab):
    tabs = context.browser.find_element_by_id('id_market_tabs')
    tabs.find_element_by_id(_TABS[tab]).click()
