#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

from datetime import timedelta
from time import sleep

from selenium.common.exceptions import TimeoutException, \
    NoSuchElementException, StaleElementReferenceException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait

from utils.selenium import find_element_by_label


@step('I access the path \'{path}\'')
@when('I go to \'{path}\'')
def navigate_to(context, path):
    context.browser.get(context.get_url(path))


@then('I am asked to login')
def step_impl(context):
    assert any('Please login' in h1.text
               for h1 in context.browser.find_elements_by_tag_name('h1')), \
        'I am not asked to login'


@when('I logout')
def logout(context):
    try:
        context.browser.find_element_by_link_text('Logout').click()
    except NoSuchElementException:
        context.execute_steps('When I click \'Logout\' in navigation bar')


@then('I see the \'{link}\' link')
def step_impl(context, link):
    try:
        context.browser.find_element_by_link_text(link)
    except NoSuchElementException:
        assert False, "Failed to find link '%s'" % link


@when('I click the \'{link}\' link')
def step_impl(context, link):
    try:
        context.browser.find_element_by_link_text(link).click()
    except NoSuchElementException:
        assert False, "Failed to find link '%s' on page %s" % (
            link, context.browser.title)


@then('I see those login options')
def step_impl(context):
    for row in context.table:
        context.execute_steps('then I see the \'%s\' link' % row['link'])


@then('I am required to authorize')
def step_impl(context):
    w = WebDriverWait(context.browser, 5)
    try:
        w.until(lambda d: d.find_element_by_id('login'))
    except TimeoutException:
        print(context.browser.title)
        print(context.browser.current_url)
        assert False, "I am not asked to authorize"


@when('I authorize as \'{user_name}\'')
def step_impl(context, user_name):
    context.provider = context.browser.find_element_by_id('provider')\
        .get_attribute('value')
    context.execute_steps('''
        when I type '%s' to 'login' field
         and I click the 'Log in' button
    ''' % user_name)


@when('I type \'{what}\' to \'{where}\' field')
def step_impl(context, what, where):
    try:
        where_input = context.browser.find_element_by_id(where)
        where_input.send_keys(Keys.CONTROL+"a")
        where_input.send_keys(what)
    except NoSuchElementException:
        assert False, "Can't find input field with id '%s'" % where


@when('I click the \'{button}\' button')
def step(context, button):
    try:
        context.browser.find_element_by_xpath(
            '//input[@type="submit" and @value="%s"]' % button
        ).submit()
    except NoSuchElementException:
        assert False, "Failed to find '%s' button" % button


@then('I don\'t see the \'{link}\' link')
def step_impl(context, link):
    try:
        context.execute_steps('then I see the \'%s\' link' % link)
    except:
        return
    assert False, "Found link \'%s\' when it shouldn't be visible" % link


@given('database is reset')
def step_impl(context):
    context.reset_database()


@given('cookies are cleared')
@when('I clear all cookies')
def clear_cookies(context):
    context.browser.delete_all_cookies()


@then('I see {item:w} labeled \'{label}\'')
def step(context, item, label):
    try:
        find_element_by_label(context.browser, label)
    except NoSuchElementException:
        assert False, "Failed to find %s labeled '%s'" % (item, label)


@then('I see value \'{value}\' in field labeled \'{label}\'')
def step(context, value, label):
    field = find_element_by_label(context.browser, label)
    assert field.text == value, "Got %s, expected %s" % (field.text, value)


@when('I type \'{what}\' to a field labeled \'{label}\'')
def step(context, what, label):
    try:
        where_input = find_element_by_label(context.browser, label)
        where_input.send_keys(Keys.CONTROL+"a")
        where_input.send_keys(what)
    except NoSuchElementException:
        assert False, "Failed to find field labeled '%s'" % label


@when('I click \'{label}\'')
def step(context, label):
    # it could be link, button or submit..
    try:
        context.browser.find_element_by_link_text(label).click()
    except NoSuchElementException:
        # not link, let's look for submit
        context.execute_steps("When I click the '%s' button" % label)


@then('I see a warning message')
def step(context):
    context.alert = context.browser.switch_to_alert()


@when('I agree to empty the bowl')
def step(context):
    context.alert.accept()


def time_to_seconds(time):
    return timedelta(**dict(
        zip(['hours', 'minutes', 'seconds'], map(int, time.split(':')))
    )).total_seconds()


@when('I wait for {time}')
def step_impl(context, time):
    sleep(time_to_seconds(time) - 0.5)


@then('\'{button}\' button is disabled')
def step_impl(context, button):
    button = context.browser.find_element_by_xpath(
        '//input[@type="submit" and @value="%s"]' % button)
    assert not button.is_enabled()
