#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""
    Recreation.

    That's the very point of visiting the establishment at all.

    The establishment guarantees you a proper service,
    however the exact details of your entertainment are left to
    the recreation runners.
"""


class Area(object):
    """
        There are various places where you can go.
    """
    def __init__(self, name, spec, handler):
        self.name = name
        self.spec = spec
        self.handler = handler


class Direct(Area):
    """
        Direct number you can use to get some information from recreation.
    """


class Recreation(object):
    """
        An entertainment of some sort is hidden beyond the door.

        Welcome!
    """
    def __init__(self, name, areas, directs=None):
        self.name = name
        self.areas = areas
        self.directs = directs or []

    @property
    def handlers(self):
        "Convert areas to handlers."
        return [(area.spec, area.handler) for area in self.areas]

    @property
    def direct_handlers(self):
        "Convert direct numbers to handlers."
        return [(direct.spec, direct.handler) for direct in self.directs]
