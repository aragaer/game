#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=too-many-public-methods

"""
    People come here not just for tea at the reception, right?

    There has to be something they are interested in.
    Some recreational activity maybe?
"""

import unittest

from utils.mock import sentinel

from establishment.recreation import Area, Direct, Recreation


class RecreationTest(unittest.TestCase):
    """
        We require some organization from our recreational areas.

        How could our establishment manage them all otherwise?
    """

    def test_recreation(self):
        """
            Suppose we are building a sample recreation area.

            A golf field for example.
            Or a hot bath.
        """
        golf = Recreation(name="Golf field",
                          areas=[Area(name='field', spec='/field/',
                                      handler=sentinel.golf)])
        bath = Recreation(name="Hot bath",
                          areas=[Area(name='locker room', spec='/login/',
                                      handler=sentinel.locker),
                                 Area(name='bath', spec='/',
                                      handler=sentinel.bath)])

        self.assertEquals(golf.name, 'Golf field')
        self.assertEquals(bath.name, 'Hot bath')
        self.assertEquals(golf.handlers, [('/field/', sentinel.golf)])
        self.assertEquals(bath.handlers, [('/login/', sentinel.locker),
                                          ('/', sentinel.bath)])

    def test_direct_number(self):
        """
            Certain recreations could have direct numbers.
        """
        golf = Recreation(name="Golf field",
                          areas=[Area(name='field', spec='/field/',
                                      handler=sentinel.golf)],
                          directs=[Direct(name='score', spec='/score/',
                                          handler=sentinel.score)])
        self.assertEquals(golf.direct_handlers, [('/score/', sentinel.score)])


if __name__ == '__main__':
    unittest.main()
