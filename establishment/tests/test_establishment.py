#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=too-many-public-methods

"""
    Verify that every establishment is adhering to certain standards.
"""

import unittest

from utils.mock import Mock, patch, sentinel

from establishment import Establishment


class EstablishmentTest(unittest.TestCase):
    """
        There are some required facilities.
    """

    def setUp(self):
        patcher = patch('tornado.web.Application.__init__')
        self.application = patcher.start()
        self.addCleanup(patcher.stop)

    @property
    def handlers(self):
        "Active handlers"
        return self.application.call_args[0][0]

    def test_reception(self):
        """
            Reception is required for example.
        """
        establishment = Establishment([],
                                      reception=sentinel.reception,
                                      cookie_secret='stuff')

        self.assertIsNotNone(establishment)
        self.assertEqual(establishment.reception, sentinel.reception)

    def test_recreation(self):
        """
            There has to be a list of all recreational areas.
        """
        recreation = Mock()
        recreation.name = 'Golf field'
        recreation.handlers = [('/field/', sentinel.golf)]
        recreation.direct_handlers = []
        recreation2 = Mock()
        recreation2.name = 'Hot bath'
        recreation2.handlers = [('/login/', sentinel.locker),
                                ('/', sentinel.bath)]
        recreation2.direct_handlers = []
        establishment = Establishment([],
                                      recreations=[recreation, recreation2],
                                      reception=sentinel.reception,
                                      cookie_secret='stuff')
        required_handlers = [
            ('/golf_field/field/', sentinel.golf, {'recreation': recreation}),
            ('/hot_bath/login/', sentinel.locker, {'recreation': recreation2}),
            ('/hot_bath/', sentinel.bath, {'recreation': recreation2})]
        for required in required_handlers:
            self.assertIn(required, self.handlers)

    def test_single_recreation(self):
        """
            If only one recreation is present, all areas are bound to it.
        """
        recreation = Mock()
        recreation.name = 'Golf field'
        recreation.handlers = [('/field/', sentinel.golf)]
        recreation.direct_handlers = []
        establishment = Establishment([],
                                      recreations=recreation,
                                      reception=sentinel.reception,
                                      cookie_secret='stuff')
        self.assertIn(('/field/', sentinel.golf, {'recreation': recreation}),
                      self.handlers)

    @patch('sockjs.tornado.SockJSRouter')
    def test_recreation_direct(self, router):
        """
            Some recreations have a direct line to provide a better service.
        """
        recreation = Mock()
        recreation.name = 'Golf field'
        recreation.handlers = []
        recreation.direct_handlers = [('/score/', sentinel.score)]
        router.return_value.urls = [sentinel.direct]
        establishment = Establishment([],
                                      recreations=recreation,
                                      reception=sentinel.reception,
                                      cookie_secret='stuff')
        router.assert_called_once_with(sentinel.score, '/score',
                                       user_settings={'recreation':
                                                      recreation})
        self.assertIn(sentinel.direct, self.handlers)

    def test_copy_handers(self):
        """
            Establishment should not modify the list of handlers.
        """
        recreation = Mock()
        recreation.name = 'Golf field'
        recreation.handlers = [('/field/', sentinel.golf)]
        recreation.direct_handlers = []
        handlers = [('/', sentinel.handler)]
        establishment = Establishment(handlers,
                                      recreations=recreation,
                                      reception=sentinel.reception,
                                      cookie_secret='stuff')
        self.assertEqual(handlers, [('/', sentinel.handler)])


if __name__ == '__main__':
    unittest.main()
