#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=missing-docstring, too-many-public-methods

from bson import ObjectId
import unittest

from establishment import AuthenticatedHandler
from utils.mock import Mock, patch, sentinel


class AuthTest(unittest.TestCase):

    object_id = ObjectId('000000000000000000000000')

    def setUp(self):
        self.handler = Mock()
        self.reception = Mock()
        self.handler.get_secure_cookie.return_value = bytes(
            str(self.object_id), 'utf-8')
        self.handler.application.reception = self.reception

    def test_return_user_document(self):
        self.reception.check.return_value = sentinel.badge

        badge = AuthenticatedHandler.get_current_user(self.handler)

        self.reception.check.assert_called_once_with(str(self.object_id))
        self.assertEqual(badge, sentinel.badge)

    def test_handle_exception(self):
        self.reception.check.side_effect = ValueError()

        user = AuthenticatedHandler.get_current_user(self.handler)

        self.reception.check.assert_called_once_with(str(self.object_id))
        self.assertIsNone(user)


if __name__ == '__main__':
    unittest.main()
