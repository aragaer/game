#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""
    Establishment - the entry point for the whole service.
"""

import sockjs.tornado
import tornado.escape
import tornado.web


def _path_from_name(name):
    return '/'+name.lower().replace(' ', '_')


def _urls_from_direct(direct, prefix, settings):
    return sockjs.tornado.SockJSRouter(direct[1], prefix+direct[0][1:-1],
                                       user_settings=settings).urls


def _one_recreation_urls(recreation, path=''):
    settings = {'recreation': recreation}
    result = [(path+spec, handler, settings)
              for spec, handler in recreation.handlers]
    for direct in recreation.direct_handlers:
        result.extend(_urls_from_direct(direct, '/'+path, settings))
    return result


class Establishment(tornado.web.Application):
    """
        Provides recreational activities to all guests.
    """

    def __init__(self, handlers, **kwargs):
        self.reception = kwargs.pop('reception', None)
        self.recreations = kwargs.pop('recreations', [])
        handlers = handlers[:]
        handlers.extend(self._build_recreation_urls())
        super().__init__(handlers, **kwargs)

    def _build_recreation_urls(self):
        result = []
        if isinstance(self.recreations, list):
            for recreation in self.recreations:
                path = _path_from_name(recreation.name)
                result.extend(_one_recreation_urls(recreation, path))
        else:
            result.extend(_one_recreation_urls(self.recreations))
        return result


# pylint: disable=too-many-public-methods
class AuthenticatedHandler(tornado.web.RequestHandler):
    """
        How we authenticate when using tornado.
    """

    def get_current_user(self):
        "Returns current user document"
        user_id = tornado.escape.to_unicode(self.get_secure_cookie('id'))
        try:
            return self.application.reception.check(user_id)
        except ValueError:
            return None
