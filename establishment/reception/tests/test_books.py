#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=too-many-public-methods

"""
    We like our books tested - we don't want them falling apart.
"""

import unittest

from mongomock import MongoClient

from utils.mock import Mock, patch, sentinel

from establishment.reception.books import BlackBoard, MongoBook, Record


def examine_book(book_class, *args, **kwargs):
    """
        Books are different, but they do share similar qualities.
    """

    class Test(unittest.TestCase):
        """
            Basic tests for binding, paper and ink.
        """

        def setUp(self):
            self.book = book_class(*args, **kwargs)

        def test_create_record(self):
            """
                It is important to be able to create new records.
            """
            record = self.book.register()

            self.assertIsInstance(record, Record)
            self.assertIn('_id', record)

        def test_locate_record(self):
            """
                Why would someone use a book he can't read?
            """
            record = self.book.register()

            other_record = self.book.find(record['_id'])

            self.assertEqual(record, other_record)

        def test_locate_note(self):
            """
                We don't always need the whole record.
            """
            record = self.book.register()
            self.book.update(record['_id'],
                             {'field1': 'value1',
                              'field2': 'value2'})

            other_record = self.book.find(record['_id'], ('field1',))

            self.assertEqual(other_record, {'field1': 'value1'})

        def test_keep_notes(self):
            """
                Our special guests have some specific demands.
                Make sure we can note that down.
            """
            record = self.book.register()

            self.book.update(record['_id'],
                             {'key': str(sentinel.value)})

            record = self.book.find(record['_id'])
            self.assertEqual(record['key'], str(sentinel.value))

        def test_erase(self):
            """
                Sometimes we have to erase the notes.
            """
            record = self.book.register()
            self.book.update(record['_id'],
                             {str(sentinel.key): sentinel.value})

            self.book.erase(record['_id'])
            # Result is None, just record or KeyError

            try:
                new_record = self.book.find(record['_id'])
                if new_record is not None:
                    self.assertEqual(new_record, {'_id': record['_id']})
            except KeyError:
                # That would be OK too
                pass

        def test_find_by_spec(self):
            """
                We have to search by ID to issue a new badge.
            """
            record = self.book.register()
            self.book.update(record['_id'], {'key': 'value'})

            new_record = self.book.find({'key': 'value'})

            self.assertEqual(record['_id'], new_record['_id'])

    return Test


BlackBoardTest = examine_book(BlackBoard)


class MongoBookTest(examine_book(MongoBook, MongoClient().test)):
    """
        Some specifict tests for huMONGOus book.
    """

    def test_find_none(self):
        """
            If there is no record, there is no record.
        """
        badge = self.book.find('000000000000000000000000', ('_id',))

        self.assertIsNone(badge)


@patch('establishment.reception.books.ObjectId',
       return_value=sentinel.object_id)
class MongoBookMockTest(unittest.TestCase):
    """
        A bit more tests for huMONGOus.

        Now we need to certify it is working correcly with real database.
    """

    def setUp(self):
        self.connection = Mock()
        self.book = MongoBook(self.connection)

    def test_update_spec(self, mock_object_id):
        """
            The book doesn't simply accept a badge.
        """
        self.book.update(sentinel.badge, sentinel.data)

        mock_object_id.assert_called_once_with(sentinel.badge)
        self.connection.book.update.assert_called_once_with(
            {'_id': sentinel.object_id}, {'$set': sentinel.data})

    def test_find_spec(self, mock_object_id):
        """
            It has to be ObjectId, not badge.
        """

        self.book.find(sentinel.badge, ('projection',))

        mock_object_id.assert_called_once_with(sentinel.badge)
        self.connection.book.find_one.assert_called_once_with(
            sentinel.object_id, {'projection': 1, '_id': 0})


if __name__ == '__main__':
    unittest.main()
