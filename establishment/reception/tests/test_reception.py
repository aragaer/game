#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=too-many-public-methods

"""
    Make sure that all our guests are welcomed and offered some tea.
"""

import unittest

from utils.mock import Mock, sentinel

from establishment.reception import Reception


class RegisterTest(unittest.TestCase):
    """
        What if some fine person decided to join our club?
    """

    def setUp(self):
        """
            Certain facilities are a bare minimum for a working reception.
        """
        self.book = Mock()
        self.reception = Reception(book=self.book)

    def test_register(self):
        """
            Pretending I'm a first time visitor here.

            And I'm interested in joining the club.
        """
        self.book.register.return_value = {'_id': sentinel.badge}

        badge = self.reception.register()

        self.assertEqual(badge, sentinel.badge)

    def test_revoke(self):
        """
            Pretending I am about to revoke my membership.
        """
        self.reception.revoke(sentinel.badge)

        self.book.erase.assert_called_onec_with(sentinel.badge)


class ReceptionTest(unittest.TestCase):
    """
        The desk, the book and the porter - everything must be in order.
    """

    def setUp(self):
        """
            I am already known here.
        """
        self.book = Mock()
        self.book.register.return_value = {'_id': sentinel.badge}
        self.reception = Reception(book=self.book)
        self.badge = sentinel.badge

    def test_take_note(self):
        """
            Let's see if we can take notes about guest preferences.
        """
        # I have told on the reception that I prefer darjeeling
        self.reception.take_note(self.badge, tea='darjeeling')

        # What was written to the book?
        self.book.update.assert_called_once_with(self.badge,
                                                 {'tea': 'darjeeling'})

    def test_get_note(self):
        """
            We can write it, but can we read it when needed?
        """
        self.book.find.return_value = {'tea': sentinel.tea}

        tea = self.reception.get_note(self.badge, 'tea')

        self.book.find.assert_called_once_with(self.badge, ('tea',))
        self.assertEqual(tea, sentinel.tea)

    def test_get_note_not_found(self):
        """
            We don't yet know the preference.
        """
        self.book.find.return_value = None

        tea = self.reception.get_note(self.badge, 'tea')

        self.assertEqual(tea, None)

    def test_check(self):
        """
            Is this badge valid?
        """
        self.book.find.return_value = {'_id': sentinel.badge}

        badge = self.reception.check(sentinel.badge)

        self.book.find.assert_called_once_with(sentinel.badge, ('_id',))
        self.assertEqual(badge, sentinel.badge)

    def test_invalid_check(self):
        """
            It could be invalid actually.
        """
        self.book.find.return_value = None

        try:
            badge = self.reception.check(sentinel.badge)

            self.assertIsNone(badge)
        except KeyError:
            pass

        self.book.find.assert_called_once_with(sentinel.badge, ('_id',))

    def test_find(self):
        """
            No badge but we got an ID.
        """
        self.book.find.return_value = {'_id': sentinel.badge}

        badge = self.reception.find(field=str(sentinel.criteria))

        self.book.find.assert_called_once_with({'field':
                                                str(sentinel.criteria)},
                                               ('_id',))
        self.assertEqual(badge, sentinel.badge)


if __name__ == '__main__':
    unittest.main()
