#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""
    Guest books.

    Do not worry, our club always uses only one book.
    It is just good to have something to choose from.
"""

from abc import ABCMeta, abstractmethod
from bson import ObjectId


class Book(object):
    """
        Not actually a book, just a pure idea of a book.
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def erase(self, criteria):
        """
            Sometimes we have to forget things.
        """

    @abstractmethod
    def find(self, criteria, projection=None):
        """
            The book can be read too.
        """

    @abstractmethod
    def register(self):
        """
            The act of creating a new record.
        """

    @abstractmethod
    def update(self, identifier, data):
        """
            Whatever data is here it is not always set in stone.
        """


Record = dict


class BlackBoard(Book):
    """
        Not really a book.
        It is a blackboard on which we can write anything we like.

        Too bad it can keep only one record.
    """

    def __init__(self):
        self._data = {'_id': 'blackboard user'}

    def erase(self, _):
        self._data = {'_id': 'blackboard user'}

    def find(self, criteria, projection=None):
        if projection is None:
            return self._data
        result = {}
        for key in self._data.keys() & set(projection):
            result[key] = self._data[key]
        return result

    def register(self):
        return self._data

    def update(self, _, record):
        self._data.update(record)


class MongoBook(Book):
    """
        A leather-bound huMONGOus book.
    """
    def __init__(self, connection):
        self.connection = connection
        self.collection = self.connection.book

    def find(self, criteria, projection=None):
        if projection is not None:
            projection = dict((key, 1) for key in projection)
        if projection is not None and '_id' not in projection:
            projection['_id'] = 0
        if not isinstance(criteria, dict):
            criteria = ObjectId(criteria)

        return self.collection.find_one(criteria, projection)

    def erase(self, badge):
        self.collection.remove(badge)

    def register(self):
        record_id = self.collection.save({})
        return {'_id': record_id}

    def update(self, badge, record):
        self.collection.update({'_id': ObjectId(badge)}, {'$set': record})
