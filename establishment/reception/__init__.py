#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""
    Reception.

    This is where all visitors are recognised,
    welcomed and offered a cup of tea.

    You can also specify your preferences to help us serve you better.
    Do you prefer indian or russian cuisine for example?
"""


class Reception(object):
    """
        A porter to welcome our guests.
        A book to write the names of the guests.
    """

    def __init__(self, book):
        self.book = book

    def register(self):
        """
            Someone decided to join our club.

            How delightful.
        """
        # Let me find an empty space in the book first
        record = self.book.register()

        # Use this badge so we can identify you while you are staying here
        return record['_id']

    def take_note(self, badge, **kwargs):
        """
            Take note of a guest's preference.

            It might be useful later.
        """
        self.book.update(badge, kwargs)

    def get_note(self, badge, *args):
        """
            Search our book for the guest preferences.
        """
        result = self.book.find(badge, args)
        if result and len(args) == 1:
            return result[args[0]]
        else:
            return result

    def revoke(self, badge):
        """
            Someone is leaving.

            While we don't know the reason for this,
            make sure he can't be traced. Just in case.
        """
        self.book.erase(badge)

    def check(self, badge):
        """
            Someone already got a badge. Is it still valid?

            We'll have to check your books to know for sure.
        """
        result = self.book.find(badge, ('_id',))
        if result is not None:
            # Your badge is valid indeed, thank you
            return result['_id']

    def find(self, **criteria):
        """
            Someone without a badge claims he is already registered.

            Would you like a cup of tea while we check your credentials?
        """
        result = self.book.find(criteria, ('_id',))
        if result is not None:
            # Here's your new badge, enjoy your stay
            return result['_id']
