#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""Some utility functions used in the crystals application."""

from collections import OrderedDict
from .config import MOLECULES_PER_GRAM, MOLECULES_PER_CARAT


def grams_from_molecules(molecules):
    "Convert molecules to grams."
    return round(molecules / MOLECULES_PER_GRAM, 1)


def molecules_from_grams(grams):
    "Convert grams to molecules."
    return round(grams * MOLECULES_PER_GRAM)


def carats_from_molecules(molecules):
    "Convert molecules to carats."
    return int(round(molecules / MOLECULES_PER_CARAT))


def plural(num=0, text=''):
    "Naive implementation of plurals."
    return "%d %s%s" % (num, text, "s"[num == 1:])


def _round_to(seconds):
    """Returns the number of seconds to which the time will be rounded."""
    if seconds >= 36000:    # 10 hours
        round_to = 1800
    elif seconds >= 7200:   # 2 hours
        round_to = 600
    elif seconds >= 900:     # 15 minutes
        round_to = 60
    else:
        round_to = 1
    return round_to


def time_str(time_left, short=False):
    """Human-readable time string."""
    seconds = time_left.total_seconds()
    round_to = _round_to(seconds)
    seconds += round_to / 2
    seconds -= seconds % round_to
    hours = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60

    result = []
    out = OrderedDict()
    if hours or short:
        out['hour'] = hours
    if minutes or short:
        out['minute'] = minutes
    if round_to == 1:
        out['second'] = seconds

    if 'second' not in out and not short:
        result.append('about')

    if short:
        result += ["%02d" % v for v in out.values()]
    else:
        result += [plural(v, k) for k, v in out.items()]
    return (':' if short else ' ').join(result)
