#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""
    Document classes - persistence layer.
"""


from crystals.color import Color


class Crystal(object):
    """
        A group of similar crystals.
    """
    def __init__(self, document):
        self._color = Color.from_hsl(document.hsl)
        self._size = document.size
        self.label = document

    @property
    def size(self):
        return self._size

    @property
    def color(self):
        return self._color.name

    @property
    def hsl(self):
        return self._color.hsl

    @property
    def count(self):
        "Number of crystals in this group."
        return self.label.count

    @count.setter
    def count(self, value):
        "Setter for number of crystals in this group."
        self.label.count = value

    def __repr__(self):
        return "%d molecules %s crystal x%d" % \
            (self.size, self.color, self.count)

    def __eq__(self, other):
        return self.size == other.size and self.color == other.color \
            and self.count == other.count
