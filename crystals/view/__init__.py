#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""
    Groups all the presentation layer classes together.
"""

from datetime import datetime

from crystals.market import appraise, cash_string_from_coins
from crystals.utilities import carats_from_molecules


class AlchemistView(object):
    """
        Presentation of a single alchemist.
    """

    def __init__(self, inner):
        self.inner = inner
        self.processes = [ProcessView(p) for p in inner.processes]
        self.bowls = inner.bowls
        self.powders = inner.powders
        self.coins = inner.coins

        self._crystal_groups = {}
        for crystal in inner.crystals:
            print(crystal)
            color = crystal.color
            size = carats_from_molecules(100*round(crystal.size/100))
            key = (color, size)
            if key not in self._crystal_groups:
                self._crystal_groups[key] = CrystalGroupView(color, size)
            for _ in range(crystal.count):
                self._crystal_groups[key].add(CrystalView(crystal))
        print(self._crystal_groups)

    @property
    def crystals(self):
        "List of crystal groups."
        return sorted(self._crystal_groups.values(),
                      key=lambda c: c.carat,
                      reverse=True)


class CrystalView(object):
    """
        Has all the properties needed for displaying a single crystal.
    """

    def __init__(self, inner):
        self.inner = inner

    @property
    def carat(self):
        "Crystal size in carats"
        return carats_from_molecules(self.inner.size)


class CrystalGroupView(object):
    """
        A group of similar crystals displayed together.
    """

    def __init__(self, color, size):
        self.color = color
        self.carat = size
        self.members = []

    def add(self, crystal):
        "Add a crystal to this group."
        self.members.append(crystal)

    @property
    def count(self):
        "Number of crystals in the group."
        return len(self.members)

    @property
    def cost(self):
        "Cost of a crystal in this group."
        return appraise(self.members[0].inner)

    @property
    def cost_string(self):
        "Human-readable cost of a crystal in this group."
        return cash_string_from_coins(self.cost)


class ProcessView(object):
    """
        A running process as seen by alchemist.
    """
    def __init__(self, inner):
        self.inner = inner

    @property
    def color(self):
        """Human-readable string describing color of bowl contents."""
        return self.inner.solution.color.name

    @property
    def finished(self):
        """Is the process finished?"""
        return self.inner.end_time is None \
            or self.inner.end_time <= datetime.now()

    @property
    def time_left(self):
        """How much time left till result."""
        return self.inner.end_time-datetime.now()
