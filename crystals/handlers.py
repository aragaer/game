#!/usr/bin/python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""Handlers for crystals."""

from abc import abstractmethod
from datetime import timedelta
import json

from sockjs.tornado import SockJSConnection
import tornado.auth
import tornado.gen
from tornado.escape import url_escape, to_unicode
import tornado.locale
import tornado.web

from crystals.alchemist import AlchemistRepo
import crystals.color
from crystals.config import EVAPORATION_TICK_TIME
import crystals.market as market
from crystals.utilities import molecules_from_grams, time_str
from crystals.view import AlchemistView
from establishment import AuthenticatedHandler
from establishment.recreation import Area


# pylint: disable=R0904
class CrystalHandler(AuthenticatedHandler):
    "Base class for all handlers."

    recreation = None
    repo = None

    def initialize(self, *_, **kwargs):
        self.recreation = kwargs['recreation']
        self.repo = AlchemistRepo(self.recreation.registry)

    def get_template_path(self):
        return 'crystals/templates'

    def get_login_url(self):
        return '/login/?next=%s' % url_escape(self.request.uri)

    def write_error(self, status_code, **kwargs):
        if 'exc_info' in kwargs:
            print(kwargs['exc_info'])
        self.render('error.xhtml',
                    error="The page you requested is not available.")

    def get_user_locale(self):
        user = self.get_current_user()
        if user is None:
            return None
        locale = self.application.reception.get_note(user, 'locale')
        return tornado.locale.get(locale)

    def get_current_alchemist(self):
        "Returns current alchemist model object."
        badge = self.get_current_user()
        alchemist_id = self.application.reception.get_note(badge, 'alchemist')
        return self.repo.find(alchemist_id)


class CrystalAsyncHandler(SockJSConnection):
    "Base class for all async handlers."

    alchemist = None

    def decode(self, name, data):
        "Decode secure cookie."
        return to_unicode(self.session.handler.get_secure_cookie(name,
                                                                 value=data))

    def get_current_alchemist(self):
        "Return the alchemist model object."
        secure_cookie = self.session.handler.get_cookie('id')
        badge = self.decode('id', secure_cookie)
        reception = self.session.handler.application.reception
        registry = self.session.server.settings['recreation'].registry
        alch_id = reception.get_note(badge, 'alchemist')
        return AlchemistRepo(registry).find(alch_id)

    @abstractmethod
    def on_message(self, message):
        "Not implemented here"


class AlchemistViewNamespaceMixin(object):
    "Adds alchemist view to namespace template."

    def get_template_namespace(self):
        """Overrides get_template_namespace from RequestHandler."""
        super_namespace = super(AlchemistViewNamespaceMixin,
                                self).get_template_namespace()
        if self.get_status() == 200:
            alchemist = AlchemistView(self.get_current_alchemist())
            super_namespace.update({'alchemist': alchemist})
        return super_namespace


class StartProcessHandler(CrystalHandler):
    """Start the process in a first empty bowl."""

    def _try_start_process(self, alchemist):
        """
            Parse all the parameters and check them.

            Returns True if process is actually started, False otherwise.
        """
        try:
            amounts = [molecules_from_grams(float(p))
                       for p in self.get_arguments('p')]
        except ValueError:
            # Any error means we don't start the process
            amounts = []

        correct = [0 <= a <= p.mass for p, a in
                   zip(alchemist.powders, amounts)]
        try:
            seed = int(self.get_argument('seed', -1))
            if seed < 0:
                seed = None
        except ValueError:
            seed = None
        try:
            water = int(self.get_argument('w', -1))
        except ValueError:
            water = -1

        if 0 < water <= 100 and all(correct) and any(amounts):
            alchemist.start_process(water=water, powders=amounts, seed=seed)

    @tornado.web.authenticated
    def get(self):
        alchemist = self.get_current_alchemist()
        self._try_start_process(alchemist)
        self.repo.save(alchemist)
        self.redirect('/lab/')


class BowlActionHandler(CrystalHandler):
    """Handler for bowl-related actions."""

    @tornado.web.authenticated
    def get(self, *args, **kwargs):
        alchemist = self.get_current_alchemist()
        view = AlchemistView(alchemist)
        bowl_num, action = args[0:2]

        try:
            process = view.processes[int(bowl_num)]
        except (IndexError, ValueError):
            self.send_error(status_code=404)
            return

        if action == 'collect':
            try:
                alchemist.collect_from(process.inner)
            except ValueError:
                pass
        elif action == 'empty':
            alchemist.discard_from(process.inner)
        self.repo.save(alchemist)
        self.redirect('/lab/')


class MarketPreviewConnection(CrystalAsyncHandler):
    """SockJS handler for solution preview"""

    def on_open(self, _):
        print("Market preview client connected")
        self.alchemist = AlchemistView(self.get_current_alchemist())
        print("Market preview client authenticated")

    def on_message(self, message):
        """Handle connection message."""
        message = json.loads(message)
        data = message['data']

        total = 0
        # crystals are sorted in sale list
        sell_crystals = sorted(self.alchemist.crystals,
                               key=lambda crystal: crystal.carat,
                               reverse=True)
        total += sum(c.cost*int(cnt)
                     for c, cnt in zip(sell_crystals, data.get('c', [])))
        total -= sum(p.price*amount
                     for p, amount in zip(market.POWDERS, data.get('p', [])))
        total -= 50000 * sum(data.get('u', []))
        cant_afford = total < -self.alchemist.coins
        if total <= 0:
            total *= -1
            label = 'You will pay'
        else:
            label = 'You will get'
        result = {'label': label,
                  'coins': market.cash_string_from_coins(total)}
        if cant_afford:
            result['cant_afford'] = True
        response = json.dumps({'type': 'preview', 'data': result})
        self.send(response)


class CrystalsConnection(CrystalAsyncHandler):
    "Swiss-knife async handler."

    def on_open(self, _):
        print("Async client connected")

    def on_message(self, message):
        """Handle request for solution preview."""
        message = json.loads(message)
        if message['type'] == 'preview':
            self.handle_solution(message['data'])

    def handle_solution(self, data):
        """Get request for solution, return solution preview data."""
        volume = data['w']
        color, mass = (0, 0, 0), 0
        for p_color, amount in data['p'].items():
            if not amount:
                continue
            p_color = crystals.color.hsl_from_color_name(p_color)
            color = crystals.color.mix_hsl((color, mass),
                                           (p_color, amount))
            mass += amount
        if volume and mass:
            result = [
                "%d ml of %s solution" %
                (volume, crystals.color.color_name_from_hsl(color)),
                "Concentration: %s (optimal at %d ml)" %
                ("low" if volume > mass * 10 else "high", mass * 10),
                "Evaporation will take %s" %
                time_str(timedelta(seconds=EVAPORATION_TICK_TIME*volume)),
            ]
        elif volume:
            result = ["%d ml of water" % volume]
        else:
            result = ["Bowl is empty"]

        response = json.dumps({'type': 'preview', 'data': result})
        self.send(response)


class _TemplateHandler(AlchemistViewNamespaceMixin, CrystalHandler):

    template = None

    def _validate(self):
        return True

    @tornado.web.authenticated
    def get(self):
        if self._validate():
            self.render(self.template)
        else:
            self.send_error(status_code=404)


class _TemplateArea(Area):

    def __init__(self, name, template, spec, validate=None):
        self._template = template
        self._validate = validate
        super().__init__(name, spec, self._make_handler(name))

    def _make_handler(self, name):
        namespace = {'template': self._template}
        if self._validate is not None:
            namespace['validate'] = self._validate
        return type(name, (_TemplateHandler,), namespace)


def _check_bowls(request):
    return AlchemistView(request.get_current_alchemist()).bowls


TEMPLATE_AREAS = [_TemplateArea('InventoryHandler', 'inventory.xhtml', '/'),
                  _TemplateArea('LabHandler', 'laboratory.xhtml', '/lab/'),
                  _TemplateArea('NewsHandler', 'news.xhtml', '/news/'),
                  _TemplateArea('PreparationHandler', 'preparation.xhtml',
                                '/preparation/', _check_bowls)]
