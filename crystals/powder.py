#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""
    Powder - used to grow crystals.
"""

from crystals.color import Color, mix_hsl


class Powder(object):
    """
        Powder model.
    """
    def __init__(self, amount, color):
        self._color = color
        self._mass = amount

    @property
    def color(self):
        """Powder color."""
        return self._color.name

    @property
    def hsl(self):
        """HSL tuple for current color."""
        return self._color.hsl

    def split(self, amount):
        """
            Reduces amount in this powder, creates new powder.
        """
        self._mass -= amount
        return Powder(amount, self._color)

    @property
    def mass(self):
        """
            How much powder we have. Molecules.
        """
        return self._mass

    @mass.setter
    def mass(self, value):
        """
            Change the amount we have.
        """
        self._mass = value

    def mix(self, other):
        """
            Mix two powders together.
        """
        self._color = Color.from_hsl(mix_hsl(
            (self.hsl, self.mass),
            (other.hsl, other.mass)))
        self._mass += other.mass
