#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""Module for colorless crystal growth"""

from collections import Counter
import random

from crystals.config import NUCLEATION_RATE, NUCLEUS_SIZE, GROWTH_SIZE, \
    MISS_RATE, SATURATED_CONCENTRATION, MOLECULES_PER_CARAT


class Solution(object):
    "Some matter dissolved in some water."
    def __init__(self, matter, water):
        self.matter = matter
        self.volume = water

    def absorbable(self):
        """Amount of matter that can be absorbed by crystals"""
        return max(0, self.matter - self.volume * SATURATED_CONCENTRATION)

    def coagulate(self):
        """Primary nucleation."""
        if self.volume:
            nuclei = random.randint(*self.nucleation_limits())
            return [(NUCLEUS_SIZE, nuclei)] if nuclei else []
        else:
            nuclei = self.matter // NUCLEUS_SIZE
            mass_left = self.matter % NUCLEUS_SIZE
            if not nuclei:
                return [(mass_left, 1)]
            extra_mass = mass_left // nuclei
            mass_left %= nuclei
            suspension = [(NUCLEUS_SIZE + extra_mass, nuclei - mass_left)]
            if mass_left:
                suspension.append((NUCLEUS_SIZE + extra_mass + 1, mass_left))
            return suspension

    def nucleation_limits(self):
        """Minimum and maximum possible numbers of nuclei"""
        supersaturation = self.matter / self.volume - SATURATED_CONCENTRATION
        if supersaturation <= 0:
            return 0, 0
        rate = self.volume * NUCLEATION_RATE * supersaturation ** 3
        nuclei_1 = int(rate)
        nuclei_2 = int(rate * supersaturation)

        # can't go to undersaturated solution
        nuclei_max = self.absorbable() // NUCLEUS_SIZE

        return min(nuclei_1, nuclei_max), min(nuclei_2, nuclei_max)


class Powder(Counter):
    """Colorless powder - storage for crystals"""

    def add_suspension(self, suspension):
        """Settling suspension on existing powder - secondary nucleation"""
        changes = Counter()
        mass = sum(s * c for s, c in self.items())

        # prepare structures for distributing nuclei over crystal groups
        states = [0 for _ in suspension]
        increments = [(mass + MISS_RATE) / c for _, c in suspension]

        # goes to create new crystals
        missed = dict(suspension)

        for size, count in self.items():
            guaranteed = 0

            # count growth in chunks of equally grown crystals
            # key - chunk start, tuple - growth value and chunk length
            chunks = {0: (count, 0)}
            position = 0
            for i, (n_size, _) in enumerate(suspension):
                states[i] += size * count
                hits = int(states[i] / increments[i])
                states[i] %= increments[i]
                missed[n_size] -= hits
                guaranteed += n_size * (hits // count)

                # resplit existing chunks by adding extra nuclei
                tail_len = hits % count
                while tail_len:
                    chunk = chunks[position]
                    piece = min(tail_len, chunk[0])
                    chunks[position] = (piece, chunk[1] + n_size)
                    position = (position + piece) % count
                    if tail_len < chunk[0]:
                        chunks[position] = (chunk[0] - tail_len, chunk[1])
                    tail_len -= piece
            changes[size] -= count
            changes.update(dict((guaranteed + size + change, c)
                                for c, change in chunks.values()))

        self.update(missed)
        self.update(changes)

    def grow(self, matter):
        """Crystal growth"""
        if not self:
            return 0
        used = 0
        mapping = {}
        growth = min(GROWTH_SIZE, int(matter / sum(self.values())))
        for size, count in self.items():
            # since all crystals grow by same size, there's never collision
            mapping[size + growth] = count
            used += growth * count
        self.subtract(self)
        self.update(mapping)
        to_delete = [k for k, v in self.items() if not v]
        for k in to_delete:
            del self[k]
        return used

    def extract_crystals(self):
        """Counter of grown crystals sizes."""
        crystals = Counter()
        for size, count in self.items():
            size -= size % MOLECULES_PER_CARAT
            crystals[size] += count
        return crystals

    def extract_lost_mass(self):
        """Matter that did not attach to any crystal."""
        return sum(c*(s % MOLECULES_PER_CARAT) for s, c in self.items())
