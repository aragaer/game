#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""
    Document classes - persistence layer.
"""

from abc import ABCMeta, abstractmethod
from bson import ObjectId
from datetime import datetime

from crystals.color import Color, color_name_from_hsl, hsl_from_color_name
from crystals.powder import Powder


class Document(object):
    """Base class for all documents."""
    __metaclass__ = ABCMeta

    @property
    @abstractmethod
    def data(self):
        """Document contents serialized as a python dictionary."""


class AlchemistDocument(Document):
    """
        An alchemist papers and book-keeping data.
    """
    document_id = None

    def __init__(self, data=None):
        if data is None:
            data = {}
        self._data = data
        self.document_id = data.get('_id')
        self.coins = data.get('coins', 0)
        self.processes = [ProcessDocument(doc)
                          for doc in data.get('processes', [])]
        self.powders = [PowderDocument(doc)
                        for doc in data.get('powders', [])]
        self.crystals = [CrystalDocument(doc)
                         for doc in data.get('crystals', [])]
        self.bowls = data.get('bowls', 0)

    @property
    def data(self):
        self._data.update(
            {'_id': self.document_id,
             'coins': self.coins,
             'crystals': [c.data for c in self.crystals],
             'processes': [p.data for p in self.processes],
             'powders': [p.data for p in self.powders],
             'bowls': self.bowls})
        return self._data

    @staticmethod
    def from_alchemist(alchemist):
        result = alchemist.document
        result.powders = [PowderDocument.from_powder(powder)
                          for powder in alchemist.powders]
        result.crystals = [CrystalDocument.from_crystal(crystal)
                           for crystal in alchemist.crystals]
        result.coins = alchemist.coins
        result.bowls = alchemist.bowls
        result.document_id = alchemist.alchemist_id
        return result

    def make_alchemist(self):
        from crystals.alchemist import Alchemist
        powders = [pdoc.make_powder() for pdoc in self.powders]
        crystals = [cdoc.make_crystal() for cdoc in self.crystals]
        return Alchemist(self, powders=powders, coins=self.coins,
                         bowls=self.bowls, _id=self.document_id,
                         crystals=crystals)


class AlchemistRegistry(object):
    """
        Is used to retrieve data about alchemists
    """

    def __init__(self, connection):
        self._db = connection
        self._collection = self._db.alchemist

    def fetch(self, alchemist_id=None):
        """
            Gets an alchemist data from the bookshelf.
        """
        if alchemist_id is None:
            raise ValueError("Must supply an ID")

        data = self._collection.find_one(alchemist_id)
        if data is None:
            raise ValueError("ID not found in database")

        for process in data.get('processes', []):
            process['details'] = self._db.bowl.find_one(process['details'])

        return AlchemistDocument(data)

    def register(self):
        """
            Registers a new alchemist.

            Returns a new id.
        """
        return self._collection.save({})

    def save(self, document):
        "Update the alchemist data."
        data = document.data
        old_process_data = self._collection.find_one(data['_id'],
                                                     {'processes': 1})
        try:
            old_processes = [p['details']
                             for p in old_process_data['processes']]
        except KeyError:
            old_processes = []
        for process in data['processes']:
            process['details'] = self._db.bowl.save(process['details'])
            try:
                old_processes.remove(process['details'])
            except ValueError:
                pass
        self._collection.save(data)
        for process in old_processes:
            self._db.bowl.remove(process)


class _ColoredDocument(Document):

    def __init__(self, data):
        self.color = data.get('color')
        self.hsl = data.get('hsl')
        if self.hsl is None:
            self.hsl = hsl_from_color_name(self.color)
        elif self.color is None:
            self.color = color_name_from_hsl(self.hsl)

    @abstractmethod
    def data(self):
        """Document contents serialized as a python dictionary."""


class CrystalDocument(_ColoredDocument):
    """
        Label on a pouch of quite similar crystals.
    """

    def __init__(self, data):
        super().__init__(data)
        self.size = data['size']
        self.count = data.get('count', 1)

    @property
    def data(self):
        return {'size': self.size, 'color': self.color, 'hsl': self.hsl,
                'count': self.count}

    def __repr__(self):
        return "%s crystal of %d size x %d" % (self.color, self.size,
                                               self.count)

    @staticmethod
    def from_crystal(crystal):
        return CrystalDocument({'hsl': crystal.hsl, 'size': crystal.size,
                                'count': crystal.count})

    def make_crystal(self):
        from crystals.crystal import Crystal
        return Crystal(self)


class PowderDocument(_ColoredDocument):
    """
        Label on a pouch of fine powder.
    """

    color = None
    mass = None

    def __init__(self, data):
        super().__init__(data)
        self.mass = data['mass']

    @property
    def data(self):
        return {'mass': self.mass, 'color': self.color}

    def __repr__(self):
        return "%d molecules of %s powder" % (self.mass, self.color)

    @staticmethod
    def from_powder(powder):
        return PowderDocument({'color': powder.color, 'mass': powder.mass})

    def make_powder(self):
        return Powder(self.mass, Color.from_hsl(self.hsl))


class ProcessDocument(Document):
    """
        Detailed description of an ongoing process.
    """
    def __init__(self, data):
        self._data = data
        self.end_time = data.get('end_time', datetime.min)
        self.details = data['details']

    @property
    def data(self):
        self._data.update({
            'end_time': self.end_time,
            'details': self.details})
        return self._data

    @staticmethod
    def from_process(process):
        details = {'matter': process.solution.matter,
                   'water': process.solution.volume,
                   'hsl': process.solution.color.hsl,
                   '_id': process.process_id,
                   'powder': list(process.sediment.items())}
        if process.sediment.seed:
            details['seed'] = {'size': process.sediment.seed.size,
                               'hsl': process.sediment.seed.hsl}
        return ProcessDocument({'details': details,
                                'end_time': process.end_time})

    def make_process(self):
        from crystals.process import Process, Solution, Sediment
        process_id = self.details['_id']
        solution = Solution(Color.from_hsl(self.details['hsl']),
                            self.details['matter'],
                            self.details['water'])
        seed_data = self.details.get('seed')
        seed = CrystalDocument(seed_data) if seed_data else None
        end_time = self.end_time
        sediment = Sediment(dict(self.details.get('powder', [])),
                            color=solution.color, seed=seed)
        return Process(process_id=process_id, solution=solution,
                       sediment=sediment, end_time=end_time)
