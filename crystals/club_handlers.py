#!/usr/bin/python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""
    These handlers are not directly related to crystals game.
"""

from collections import OrderedDict

from tornado.auth import OpenIdMixin
from tornado.gen import coroutine
from tornado.locale import get as get_locale
from tornado.web import authenticated, RequestHandler

from crystals.alchemist import AlchemistRepo
from establishment import AuthenticatedHandler


class LoginHandler(RequestHandler, OpenIdMixin):
    "Handler for logging in."

    providers = OrderedDict([
        ('google', ('Google OpenID',
                    "https://www.google.com/accounts/o8/ud")),
        ('lj', ('LiveJournal OpenID',
                "http://www.livejournal.com/openid/server.bml")),
        ('vkid', ('VKontakteID', "http://www.vkontakteid.ru/OP")),
        ('ya', ('Yandex OpenID', "http://openid.yandex.ru/server/")),
    ])

    _OPENID_ENDPOINT = ''

    def get_template_path(self):
        return 'crystals/templates'

    def get_user_locale(self):
        cookie_locale = self.get_cookie('lang')
        if cookie_locale is not None:
            return get_locale(cookie_locale)

    @coroutine
    def get(self):
        if 'logout' in self.request.path:
            self.clear_cookie('id')
            self.redirect('/')
            return

        provider = self.get_argument('provider', None)
        return_to = self.get_argument('next', '/')
        if provider is None:
            self.render('login.xhtml', return_to=return_to,
                        providers=LoginHandler.providers)
        else:
            self._OPENID_ENDPOINT = LoginHandler.providers[provider][1]
            result = self.get_argument("openid.mode", None)
            if result == 'id_res':
                openid_user = yield self.get_authenticated_user()
                self._login(openid_user['claimed_id'])
                self.redirect(return_to)
            else:
                yield self.authenticate_redirect(self.request.uri)

    def _login(self, openid):
        try:
            badge = self.application.reception.find(openid=openid)
        except ValueError:
            badge = None

        if badge is None:
            badge = self._register(openid)
        self.set_secure_cookie('id', str(badge))

    def _register(self, openid):
        badge = self.application.reception.register()
        self.application.reception.take_note(
            badge, openid=openid, alchemist=self._register_alchemist(),
            locale=self.get_browser_locale().code[:2])
        return badge

    def _register_alchemist(self):
        repo = AlchemistRepo(self.application.recreations.registry)
        return repo.create().alchemist_id


class LocaleHandler(AuthenticatedHandler):
    """Handler for locale switching."""

    @authenticated
    def get(self, *args, **kwargs):
        badge = self.get_current_user()
        locale = self.get_argument('lang').lower()
        self.application.reception.take_note(badge, locale=locale)

        self.redirect(self.request.headers.get('Referer') or '/')
