#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=missing-docstring, too-many-public-methods

"""
    Testing bowl handler.
"""

import unittest
import tornado.testing

from crystals.alchemist import Alchemist
from crystals.document import ProcessDocument
from crystals.handlers import BowlActionHandler
from crystals.tests import CrystalsTestCase
from crystals.utilities import grams_from_molecules
from crystals.view import AlchemistView
from utils.mock import call, patch


class TestModelIsCalled(CrystalsTestCase):

    def _skip_if_process_not_found(self):
        alchemist = self.repo.find(self.alchemist.alchemist_id)
        view = AlchemistView(alchemist)
        if not view.processes:
            raise unittest.SkipTest("Process not found")

    @patch.object(Alchemist, 'collect_from')
    def test_inventory_collect(self, collect):
        self.alchemist.start_process([10], 1)
        process = self.alchemist.processes[0]
        process.evaporate()
        self.alchemist.document.processes[0] = \
            ProcessDocument.from_process(process)
        self.repo.save(self.alchemist)
        self._skip_if_process_not_found()

        response = self.fetch('/bowl/0/collect/')

        collect.assert_called_once_with(self.alchemist.processes[0])
        self.assertEqual(response.code, 200)

    @patch.object(Alchemist, 'discard_from')
    def test_empty_bowl(self, empty):
        self.alchemist.start_process([1000], 10)
        self.repo.save(self.alchemist)
        self._skip_if_process_not_found()

        response = self.fetch('/bowl/0/empty/')

        empty.assert_called_once_with(self.alchemist.processes[0])
        self.assertEqual(response.code, 200)

    #pylint: disable=star-args
    @patch.object(Alchemist, 'start_process')
    def test_start_from_preparation(self, start):
        tcs = [{'water': 10, 'powders': [0, 10000, 0], 'seed': None},
               {'water': 10, 'powders': [0, 10000, 0], 'seed': 0}]
        for case in tcs:
            url = '/start/?w=%d&%s&seed=%d' % \
                (case['water'],
                 '&'.join('p=%d' % grams_from_molecules(p)
                          for p in case['powders']),
                 -1 if case['seed'] is None else case['seed'])

            response = self.fetch(url)

            self.assertEqual(response.code, 200)
        start.assert_has_calls([call(**tc) for tc in tcs])


class TestInputData(CrystalsTestCase):

    def test_invalid_bowl_number(self):
        for arg in ['-2', '5', 'test']:
            response = self.fetch('/bowl/%s/' % arg)

            self.assertEqual(response.code, 404)

    @patch.object(BowlActionHandler, 'send_error')
    def test_collect_wrong_bowl_number(self, mock_send_error):
        self.fetch('/bowl/5/collect/')

        mock_send_error.assert_called_once_with(status_code=404)
        mock_send_error.reset_mock()

    @patch.object(BowlActionHandler, 'send_error')
    def test_empty_wrong_bowl_number(self, mock_send_error):
        self.fetch('/bowl/5/empty/')

        mock_send_error.assert_called_once_with(status_code=404)
        mock_send_error.reset_mock()

    @patch.object(Alchemist, 'start_process')
    def test_start_exception_handled(self, mock_start):
        response = self.fetch('/start/?w=0&p=1')

        self.assertFalse(mock_start.called)
        self.assertEqual(response.code, 200)


if __name__ == '__main__':
    tornado.testing.main()
