#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=missing-docstring, too-many-public-methods

"""
    Testing the market operations here.
"""

import unittest

from crystals.alchemist import AlchemistRepo
from crystals.crystal import Crystal
from crystals.document import AlchemistDocument, CrystalDocument
from crystals.market import cash_string_from_coins, coins_from_cash_string, \
    appraise, TradeSession, POWDERS
from crystals.tests import create_mock_registry
from crystals.utilities import molecules_from_grams


class UnitTestMarket(unittest.TestCase):

    def test_powders(self):
        powders = dict((p.color, p.price) for p in POWDERS)
        for color in ['red', 'blue', 'green']:
            self.assertIn(color, powders)
            self.assertEqual(powders[color], 100)

        for color in ['yellow', 'purple', 'cyan']:
            self.assertIn(color, powders)
            self.assertEqual(powders[color], 70)

        for color in ['gray']:
            self.assertIn(color, powders)
            self.assertEqual(powders[color], 10)

    def test_format_price(self):
        self.assertEqual(cash_string_from_coins(10), '10C')
        self.assertEqual(cash_string_from_coins(100), '1S')
        self.assertEqual(cash_string_from_coins(10000), '1G')
        self.assertEqual(cash_string_from_coins(1180000), '118G')
        self.assertEqual(cash_string_from_coins(1000000), '100G')

    def test_parse_price(self):
        self.assertEqual(coins_from_cash_string('10S'), 1000)
        self.assertEqual(coins_from_cash_string('2G 5C'), 20005)

    def test_appraise(self):
        def appraise_crystal(color, size):
            document = CrystalDocument({'color': color,
                                        'size': molecules_from_grams(size)})
            return appraise(Crystal(document))
        self.assertEqual(appraise_crystal('blue', 1), 1000)
        self.assertEqual(appraise_crystal('red', 2), 4000)
        self.assertEqual(appraise_crystal('green', 1.6), 2560)
        self.assertEqual(appraise_crystal('yellow', 1), 900)
        self.assertEqual(appraise_crystal('purple', 1.4), 1764)
        self.assertEqual(appraise_crystal('cyan', 2.2), 4356)
        self.assertEqual(appraise_crystal('gray', 5), 2500)


class _SetupMarketTest(unittest.TestCase):

    def setUp(self):
        self.registry = create_mock_registry()
        repo = AlchemistRepo(self.registry)
        self.context = repo.create()
        self.context.bowls = 0
        document = AlchemistDocument.from_alchemist(self.context)
        document.powders = []
        self.registry.save(document)
        self.context = repo.find(self.context.alchemist_id)
        self._session = TradeSession(self.context, self.registry)

    def trade(self, crystals, powders, upgrades):
        self._session.trade(crystals, powders, upgrades)


class MarketTradeTest(_SetupMarketTest):

    def test_should_be_able_to_buy_bowl(self):
        self.context.coins = 1000000

        self.trade(crystals=[], powders=[], upgrades=['1'])

        self.assertEqual(self.context.coins, 950000)
        self.assertEqual(self.context.bowls, 1)
        document = self.registry.fetch(self.context.document.document_id)
        self.assertEqual(document.coins, 950000)

    def test_buy_two_processes(self):
        self.context.coins = 1000000

        self.trade(crystals=[], powders=[], upgrades=['2'])

        self.assertEqual(self.context.coins, 900000)
        self.assertEqual(self.context.bowls, 2)

    def test_buy_powder(self):
        self.context.coins = 1000000

        self.trade(crystals=[], upgrades=[],
                   powders=['10', '20', '0', '0', '0', '0', '50'])

        self.assertEqual(self.context.coins, 996500)
        document = self.registry.fetch(self.context.document.document_id)
        self.assertEqual(dict((p.color, p.mass) for p in document.powders),
                         {'red': 10000, 'blue': 20000, 'gray': 50000})

    def test_sell_crystal(self):
        from crystals.view import AlchemistView
        data = [
            ('red', 1, 2),
            ('blue', 3, 5),
            ('green', 1.4, 1),
        ]
        sales = [2, 1, 0]
        crystal_list = [CrystalDocument(
            {'color': c, 'size': molecules_from_grams(s), 'count': cnt})
            for c, s, cnt in data]
        self.context.coins = coins = 0
        self.context._crystals = [cdoc.make_crystal() for cdoc in crystal_list]

        view = AlchemistView(self.context)
        sale_list = view.crystals   # ensure the same order as in UI

        self.trade(crystals=[str(n) for n in sales], upgrades=[], powders=[])

        for group, count in zip(sale_list, sales):
            coins += appraise(group.members[0].inner) * count

        self.assertEqual(self.context.coins, coins)

        expected = set()
        for group, sold in zip(sale_list, sales):
            if group.count > sold:
                expected.add((group.color,
                              molecules_from_grams(group.carat) / 5,
                              group.count - sold))

        actual = set()
        for crystal in self.context.crystals:
            actual.add((crystal.color, crystal.size, crystal.count))

        self.assertEqual(actual, expected)

    def test_buy_empty(self):
        self.context.coins = 1000000

        self.trade(crystals=[], upgrades=[''], powders=['10'])

        self.assertEqual(self.context.coins, 999000)
        document = self.registry.fetch(self.context.document.document_id)
        self.assertEqual(dict((p.color, p.mass) for p in document.powders),
                         {'red': 10000})

    def test_spend_all_money(self):
        self.trade(crystals=[], upgrades=[''], powders=['10'])

        self.assertEqual(self.context.coins, 0)
        document = self.registry.fetch(self.context.document.document_id)
        self.assertEqual(dict((p.color, p.mass) for p in document.powders),
                         {'red': 10000})

    def test_buy_none(self):
        self.context.coins = 42

        self.trade(None, [], None)

        self.assertEqual(self.context.coins, 42)


class TestMarketValidation(_SetupMarketTest):

    def test_buy_more_than_can_afford(self):
        self.trade(crystals=[], upgrades=['1'], powders=['15', '15', '15'])

        self.assertEqual(self.context.coins, 1000)
        self.assertEqual(self.context.powders, [])
        self.assertEqual(self.context.processes, [])

    def test_buy_negative(self):
        self.context.coins = 1000000

        self.trade(crystals=[], upgrades=['-1'],
                   powders=['10', '-20', '0', '0', '0', '0', '50'])

        self.assertEqual(self.context.coins, 998500)
        document = self.registry.fetch(self.context.document.document_id)
        self.assertEqual(dict((p.color, p.mass) for p in document.powders),
                         {'red': 10000, 'gray': 50000})

    def test_sell_inexistent_crystal(self):
        self.context.document.crystals = [(CrystalDocument({'color': 'red',
                                                            'count': 2,
                                                            'size': 600}))]
        self.context = self.context.document.make_alchemist()

        self.trade(crystals=['-1'], upgrades=[], powders=[])

        self.assertEqual(self.context.coins, 1000)
        self.assertEqual(len(self.context.crystals), 1)
        crystal = self.context.crystals[0]
        self.assertEqual(crystal.size, 600)
        self.assertEqual(crystal.count, 2)
        self.assertEqual(crystal.color, 'red')

    def test_sell_negative_crystal(self):
        self.context.document.crystals = [(CrystalDocument({'color': 'red',
                                                            'count': 2,
                                                            'size': 600}))]
        self.context = self.context.document.make_alchemist()

        self.trade(crystals=['-1'], upgrades=[], powders=[])

        self.assertEqual(self.context.coins, 1000)
        self.assertEqual(len(self.context.crystals), 1)
        crystal = self.context.crystals[0]
        self.assertEqual(crystal.size, 600)
        self.assertEqual(crystal.count, 2)
        self.assertEqual(crystal.color, 'red')


if __name__ == '__main__':
    unittest.main()
