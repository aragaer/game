#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=missing-docstring, too-many-public-methods

"""
    Testing templates only.
"""

from types import SimpleNamespace
import unittest

from bs4 import BeautifulSoup
import tornado.template

from utils.mock import Mock, call, sentinel


_MOCK_MODULES = Mock()


class TemplateTest(unittest.TestCase):

    namespace = {
        '_': lambda x: x,
        '_tt_modules': _MOCK_MODULES,
    }
    template_file = None

    @classmethod
    def setUpClass(cls):
        loader = tornado.template.Loader('crystals/templates')
        cls.template = loader.load(cls.template_file)

    def render(self, **kwargs):
        kwargs.update(self.namespace)
        return self.template.generate(**kwargs)

    def get_soup(self, **kwargs):
        return BeautifulSoup(self.render(**kwargs))


class MockAlchemistMixin(object):

    def setUp(self):
        self.alchemist = SimpleNamespace(processes=[], powders=[],
                                         crystals=[], coins=0, bowls=2)
        self.namespace.update({'alchemist': self.alchemist})


class UsesWalletModuleMixin(object):

    def test_should_use_wallet_module(self):
        self.alchemist.coins = sentinel.coins
        _MOCK_MODULES.WalletModule.reset_mock()

        self.render(action='')

        _MOCK_MODULES.WalletModule.assert_called_once_with()


class LoginTemplateTest(TemplateTest):

    template_file = 'login.xhtml'

    def test_renders_providers(self):
        providers = {'provider1': ('name1', 'url1'),
                     'provider2': ('name2', 'url2')}

        soup = self.get_soup(providers=providers, return_to=str(sentinel.ret))

        for provider, (name, _) in providers.items():
            link = soup.find('a', text=name)
            self.assertEqual(link.get('href'),
                             '?next=%s&provider=%s' % (sentinel.ret,
                                                       provider))

    def test_language_switch(self):
        _MOCK_MODULES.LocationSwitch.reset_mock()

        self.render(providers={}, return_to='/')

        _MOCK_MODULES.LocationSwitch.assert_called_once()


class InventoryTemplateTest(TemplateTest, MockAlchemistMixin,
                            UsesWalletModuleMixin):
    """
        Used by InventoryHandler.
    """

    template_file = 'inventory.xhtml'

    def setUp(self):
        MockAlchemistMixin.setUp(self)

    def test_use_inventory_modules(self):
        self.alchemist.crystals = [sentinel.c1, sentinel.c2, sentinel.c3]
        self.alchemist.powders = [sentinel.p1, sentinel.p2, sentinel.p3]
        _MOCK_MODULES.InventoryCrystal.reset_mock()
        _MOCK_MODULES.InventoryPowder.reset_mock()
        _MOCK_MODULES.NavBarModule.reset_mock()

        self.render()

        _MOCK_MODULES.InventoryCrystal.assert_has_calls(
            list(call(crystal) for crystal in self.alchemist.crystals))
        _MOCK_MODULES.InventoryPowder.assert_has_calls(
            list(call(powder) for powder in self.alchemist.powders))
        self.assertTrue(_MOCK_MODULES.NavBarModule.called)

    def test_new_process_module(self):
        self.alchemist.bowls = sentinel.bowl_count
        self.alchemist.processes = [sentinel.p1, sentinel.p2, sentinel.p3]
        _MOCK_MODULES.NewProcess.reset_mock()

        self.render()

        _MOCK_MODULES.NewProcess.assert_called_once_with(
            sentinel.bowl_count)


class MarketTemplateTest(TemplateTest, MockAlchemistMixin,
                         UsesWalletModuleMixin):

    template_file = 'market.xhtml'

    def setUp(self):
        MockAlchemistMixin.setUp(self)

    def test_market_categories(self):
        soup = self.get_soup(action='')
        upgrade_list = soup.find('ul', id='id_upgrades').find_all('li')
        upgrades = {}
        for item in upgrade_list:
            cols = item.find_all('span')
            upgrades[cols[0].text] = cols[2].text
            field = item.find('input')
            self.assertEqual(field.get('name'), 'u')
        self.assertIn('Additional bowl', upgrades)
        self.assertEqual(upgrades['Additional bowl'], '@ 5G')


class JewelerTemplateTest(TemplateTest, MockAlchemistMixin,
                          UsesWalletModuleMixin):

    template_file = 'jeweler.xhtml'

    def setUp(self):
        MockAlchemistMixin.setUp(self)

    def test_no_crystals_placeholder(self):
        soup = self.get_soup(action='')
        self.assertIn('No\xa0crystals\xa0for\xa0sale',
                      soup.find('ul', id='id_crystals').text)

    def test_use_market_modules(self):
        self.alchemist.crystals = [sentinel.c1, sentinel.c2, sentinel.c3]
        _MOCK_MODULES.MarketCrystal.reset_mock()
        _MOCK_MODULES.NavBarModule.reset_mock()

        self.render(action='')

        _MOCK_MODULES.MarketCrystal.assert_has_calls(
            list(call(crystal) for crystal in self.alchemist.crystals))
        self.assertTrue(_MOCK_MODULES.NavBarModule.called)


class PowderShopTemplateTest(TemplateTest, MockAlchemistMixin,
                             UsesWalletModuleMixin):

    template_file = 'powders_shop.xhtml'

    def setUp(self):
        MockAlchemistMixin.setUp(self)

    def test_submit_to_trade(self):
        soup = self.get_soup()

        form = soup.find('form')
        self.assertEqual(form.get('action'), '/trade/powders/')

    def test_use_market_modules(self):
        import crystals.market
        _MOCK_MODULES.MarketPowder.reset_mock()

        self.render()

        _MOCK_MODULES.MarketPowder.assert_has_calls(
            list(call(p) for p in crystals.market.POWDERS))


class LabTemplateTest(TemplateTest, MockAlchemistMixin):

    template_file = 'laboratory.xhtml'

    def setUp(self):
        MockAlchemistMixin.setUp(self)

    def test_has_correct_title(self):
        soup = self.get_soup()

        title = soup.find('title')
        self.assertIn('Laboratory', title.text)

    def test_uses_navigation_module(self):
        _MOCK_MODULES.NavBarModule.reset_mock()

        self.render()

        _MOCK_MODULES.NavBarModule.assert_called_once_with()

    def test_uses_process_ui_module(self):
        self.alchemist.processes = [sentinel.p1, sentinel.p2, sentinel.p3]
        _MOCK_MODULES.LabProcess.reset_mock()

        self.render()

        _MOCK_MODULES.LabProcess.assert_has_calls(
            list(call(i, process)
                 for i, process in enumerate(self.alchemist.processes)))

    def test_uses_new_process_ui_module(self):
        self.alchemist.bowls = sentinel.bowl_count
        _MOCK_MODULES.LabNewProcess.reset_mock()

        self.render()

        _MOCK_MODULES.LabNewProcess.assert_called_once_with(
            sentinel.bowl_count)


if __name__ == '__main__':
    unittest.main()
