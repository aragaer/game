#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=missing-docstring, too-many-public-methods

import unittest
from urllib.parse import urlparse

from tornado.testing import AsyncHTTPTestCase, main as tornado_test_main
from tornado.web import UIModule

from crystals import CrystalsGameClub, CrystalsRecreation
from crystals.alchemist import AlchemistRepo
from crystals.club_handlers import LoginHandler, LocaleHandler
from crystals.tests import create_mock_registry
from establishment import Establishment
from tests.openid import MockOpenIDHandler
from utils.mock import call, Mock, patch, sentinel


class UiModuleStub(UIModule):
    def render(self):
        return ""


class _LoginHandlerTestSetup(AsyncHTTPTestCase):

    reception = None
    registry = None
    openid = None

    def setUp(self):
        self.registry = create_mock_registry()
        super().setUp()
        self.reception.register.reset_mock()
        self.reception.take_note.reset_mock()
        self.reception.register.return_value = sentinel.badge
        self.reception.find.return_value = None

    def get_app(self):
        self.reception = Mock()
        recreation = Mock()
        recreation.registry = self.registry
        recreation.handlers = recreation.direct_handlers = []
        handlers = [
            (r'/login/', LoginHandler),
            (r'/logout/', LoginHandler),
            (r'/openid/(.*)/', MockOpenIDHandler,
             {'port': self.get_http_port(), 'fixed_user': 'user'})]
        LoginHandler.providers['mock'] = ('Mock',
                                          self.get_url('/openid/mock/'))
        self.openid = 'http://localhost:%d/openid/mock-user' \
            % self.get_http_port()
        return Establishment(handlers, reception=self.reception,
                             recreations=recreation,
                             ui_modules={'LanguageSwitch': UiModuleStub})


@patch.object(LoginHandler, 'set_secure_cookie')
class OpenIDTest(_LoginHandlerTestSetup):

    def test_login(self, mock_set_cookie):
        self.reception.find.return_value = sentinel.badge

        response = self.fetch('/login/?provider=mock&return_to=%2F')

        self.reception.find.assert_called_once_with(openid=self.openid)
        self.assertEqual(urlparse(response.effective_url).path, '/')
        self.assertFalse(self.reception.register.called)
        mock_set_cookie.assert_any_call('id', str(sentinel.badge))

    def test_register(self, mock_set_cookie):
        response = self.fetch('/login/?provider=mock&return_to=%2F')

        self.assertEqual(urlparse(response.effective_url).path, '/')
        notes = {}
        for args, kwargs in self.reception.take_note.call_args_list:
            if args[0] == sentinel.badge:
                notes.update(kwargs)
        self.assertEqual(notes['openid'], self.openid)
        mock_set_cookie.assert_any_call('id', str(sentinel.badge))
        alchemist = AlchemistRepo(self.registry).find(notes['alchemist'])
        self.assertIsNotNone(alchemist)


class LoginHandlerLocaleTest(_LoginHandlerTestSetup):

    @patch.object(LoginHandler, 'get_browser_locale')
    def test_default_locale(self, mock_locale):
        mock_locale.return_value.code = 'ru-RU'
        self.fetch('/login/?provider=mock&return_to=%2F')

        notes = {}
        for args, kwargs in self.reception.take_note.call_args_list:
            if args[0] == sentinel.badge:
                notes.update(kwargs)
        self.assertEqual(notes['locale'], 'ru')

    @patch.object(LoginHandler, 'get_cookie', return_value='ru')
    def test_locale_from_cookie(self, get_cookie):
        response = self.fetch('/login/')

        get_cookie.assert_called_once_with('lang')
        self.assertEqual(response.code, 200)


class UnitTestLogin(AsyncHTTPTestCase):

    registry = None

    def setUp(self):
        self.reception = Mock()
        self.reception.check.side_effect = ValueError()
        super().setUp()

    def get_app(self):
        return CrystalsGameClub(reception=self.reception,
                                recreations=CrystalsRecreation(self.registry))

    # My lovely mixin won't work here because of coroutine
    @patch.object(LoginHandler, 'render')
    def test_uses_template(self, mock_render):
        self.fetch('/')

        mock_render.assert_called_once_with(
            'login.xhtml', return_to='/', providers=LoginHandler.providers)

    def test_openid_redirect(self):
        for provider, (_, uri) in LoginHandler.providers.items():
            response = self.fetch('/login/?provider=%s' % provider,
                                  follow_redirects=False)

            self.assertEqual(response.code, 302)
            self.assertIn('Location', response.headers)
            self.assertIn(uri, response.headers['Location'])

    @patch.object(LoginHandler, 'clear_cookie')
    def test_logout(self, mock_clear):
        response = self.fetch('/logout/', follow_redirects=False)

        mock_clear.assert_called_once_with('id')
        self.assertEqual(response.code, 302)
        self.assertIn('Location', response.headers)
        self.assertEqual(response.headers['Location'], '/')


class LocaleSwitchTest(unittest.TestCase):

    user = None

    def setUp(self):
        self.badge = sentinel.badge
        self.reception = Mock()

    # FIXME: Rewrite without mocks
    def test_set_locale_and_redirect(self):
        handler = Mock()
        handler.get_current_user.return_value = sentinel.badge
        handler.get_argument.return_value = 'RU'
        handler.request = Mock()
        handler.request.headers.get.return_value = sentinel.return_to
        handler.application.reception = self.reception

        LocaleHandler.get(handler)

        handler.get_argument.assert_has_calls([call('lang')])
        handler.redirect.assert_has_calls([call(sentinel.return_to)])
        self.reception.take_note.assert_called_once_with(
            sentinel.badge, locale='ru')


if __name__ == '__main__':
    tornado_test_main()
