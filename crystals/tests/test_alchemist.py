#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=missing-docstring, too-many-public-methods

"""
    Testing alchemist model.
"""

from datetime import datetime
import unittest

from crystals.alchemist import Alchemist, AlchemistRepo, INITIAL_POWDERS
from crystals.document import AlchemistDocument, CrystalDocument, \
    ProcessDocument, PowderDocument
from crystals.color import Color
from crystals.process import Process, Solution, Sediment
from crystals.tests import create_mock_registry
from crystals.utilities import molecules_from_grams


class _AlchemistRepoSetup(unittest.TestCase):

    document = None
    registry = None
    repo = None

    def setUp(self):
        self.registry = create_mock_registry()
        self.repo = AlchemistRepo(self.registry)

        self.document = self.registry.fetch(self.registry.register())


class AlchemistRepoTest(_AlchemistRepoSetup):

    def test_create(self):
        repo = AlchemistRepo(self.registry)

        alchemist = repo.create()

        alchemist2 = repo.find(alchemist.alchemist_id)
        self.assertEqual(alchemist.alchemist_id, alchemist2.alchemist_id)

    def test_save(self):
        alchemist = self.repo.create()
        alchemist.coins = 42

        self.repo.save(alchemist)

        alchemist = self.repo.find(alchemist.alchemist_id)
        self.assertEqual(alchemist.coins, 42)


class AlchemistTest(_AlchemistRepoSetup):

    alchemist = None

    def setUp(self):
        super().setUp()
        self.alchemist = self.repo.create()

    def test_initial_powders(self):
        self.assertEqual([p.mass for p in self.alchemist.powders],
                         [10000] * 3)
        self.assertEqual({p.color for p in self.alchemist.powders},
                         {'red', 'green', 'blue'})

    def test_use_bowl(self):
        self.alchemist.start_process([10000], 10)

        self.assertEqual(len(self.alchemist.processes), 1)
        self.assertEqual(self.alchemist.bowls, 2)

    def test_collect_from(self):
        self.alchemist.start_process([10000], 10)
        self.alchemist.document.processes[0].end_time = datetime.now()
        process = self.alchemist.processes[0]
        color = process.solution.color.name

        process.sync_time()
        self.alchemist.collect_from(process)

        self.assertEqual(len(self.alchemist.powders), 3)
        self.assertTrue(any(p.color == color for p in self.alchemist.powders))
        self.assertEqual(self.alchemist.processes, [])

    def test_collect_join_powders(self):
        alchemist = self.repo.create()
        data = [5000, 8000]
        for num, powder in enumerate(data):
            color = alchemist.powders[num].color
            old_mass = alchemist.powders[num].mass
            # Use only one powder - the one with same number as test case
            powders_to_use = [0] * (num+1)
            powders_to_use[-1] = powder
            alchemist.start_process(powders_to_use, 10)
            alchemist.document.processes[0].end_time = datetime.now()
            process = alchemist.processes[0]

            process.sync_time()
            alchemist.collect_from(process)

            self.assertEqual(len(alchemist.powders), 3)
            self.assertEqual(alchemist.processes, [])
            new_mass = alchemist.powders[num].mass
            for crystal in alchemist.crystals:
                if color == crystal.color:
                    new_mass += crystal.size
            self.assertEqual(old_mass, new_mass)

    def test_collect_group_crystals(self):
        size = molecules_from_grams(0.6)
        document = AlchemistDocument({'crystals':
                                      [{'size': size, 'color': 'red',
                                        'count': 3}]})
        alchemist = document.make_alchemist()
        color = Color.from_name('red')
        process = Process(sediment=Sediment({size: 5}, color=color),
                          solution=Solution(color, 0, 0))

        alchemist.collect_from(process)

        self.assertEqual(len(alchemist.crystals), 1)

        crystal = alchemist.crystals[0]
        self.assertEqual(crystal.color, 'red')
        self.assertEqual(crystal.size, size)
        self.assertEqual(crystal.count, 8)

    def test_add_powder(self):
        self.alchemist.add_powder(color='blue', mass=10)
        self.alchemist.add_powder(color='yellow', mass=50)

        powders = {powder.color: powder.mass
                   for powder in self.alchemist.powders}
        self.assertEqual(powders, {'red': 10000, 'green': 10000, 'blue': 10010,
                                   'yellow': 50})

    def test_collect_no_powder(self):
        alchemist = self.repo.create()
        color = Color.from_name('yellow')
        process = Process(sediment=Sediment({molecules_from_grams(1): 5}),
                          solution=Solution(color, 0, 0))

        alchemist.collect_from(process)

        self.assertEqual(len(alchemist.powders), 3)

    def test_start_process_no_seed(self):
        initial_powders = INITIAL_POWDERS
        alchemist = self.repo.create()
        amount = molecules_from_grams(5)
        size = molecules_from_grams(0.6)
        alchemist.document.crystals.append(
            CrystalDocument({'color': 'red', 'size': size, 'count': 5}))
        alchemist = alchemist.document.make_alchemist()
        alchemist.start_process(powders=[amount], water=50)

        process = alchemist.processes[0]
        self.assertEqual(process.solution.volume, 50)
        self.assertEqual(process.solution.matter, amount)
        self.assertEqual(process.solution.color.name, 'red')

        self.assertEqual(len(alchemist.powders), len(initial_powders))
        for num in range(1, len(alchemist.powders)):
            self.assertEqual(alchemist.powders[num].mass,
                             initial_powders[num][1])

        powder = alchemist.powders[0]
        self.assertEqual(powder.mass, initial_powders[0][1] - amount)

        self.assertEqual(len(alchemist.crystals), 1)
        self.assertEqual(alchemist.crystals[0].count, 5)

    def test_start_process_seed(self):
        initial_powders = INITIAL_POWDERS
        alchemist = self.repo.create()
        amount = molecules_from_grams(5)
        size = molecules_from_grams(0.6)
        alchemist.document.crystals.append(
            CrystalDocument({'color': 'red', 'size': size, 'count': 5}))
        alchemist = alchemist.document.make_alchemist()
        alchemist.start_process(powders=[amount], water=50, seed=0)

        process = alchemist.processes[0]
        self.assertEqual(process.solution.volume, 50)
        self.assertEqual(process.solution.matter, amount)
        self.assertEqual(process.solution.color.name, 'red')
        self.assertIsNotNone(process.end_time)

        self.assertEqual(len(alchemist.powders), len(initial_powders))
        for num in range(1, len(alchemist.powders)):
            self.assertEqual(alchemist.powders[num].mass,
                             initial_powders[num][1])

        powder = alchemist.powders[0]
        self.assertEqual(powder.mass, initial_powders[0][1] - amount)

        self.assertEqual(len(alchemist.crystals), 1)
        self.assertEqual(alchemist.crystals[0].count, 4)

    def test_seed_only_crystal(self):
        initial_powders = INITIAL_POWDERS
        alchemist = self.repo.create()
        amount = molecules_from_grams(5)
        size = molecules_from_grams(0.6)
        alchemist.document.crystals.append(CrystalDocument({'color': 'red',
                                                            'size': size}))
        alchemist = alchemist.document.make_alchemist()
        alchemist.start_process(powders=[amount], water=50, seed=0)

        process = alchemist.processes[0]
        self.assertEqual(process.solution.volume, 50)
        self.assertEqual(process.solution.matter, amount)
        self.assertEqual(process.solution.color.name, 'red')
        self.assertIsNotNone(process.end_time)

        self.assertEqual(len(alchemist.powders), len(initial_powders))
        for num in range(1, len(alchemist.powders)):
            self.assertEqual(alchemist.powders[num].mass,
                             initial_powders[num][1])

        powder = alchemist.powders[0]
        self.assertEqual(powder.mass, initial_powders[0][1] - amount)

        self.assertEqual(len(alchemist.crystals), 0)

    def test_get_read_fields(self):
        self.document.coins = 42
        self.registry.save(self.document)

        document = self.registry.fetch(self.document.document_id)
        alchemist = document.make_alchemist()

        self.assertEqual(alchemist.coins, 42)

    def test_write_process_to_document(self):
        alchemist = self.repo.create()

        alchemist.start_process([1], 1)

        process = alchemist.processes[0]
        self.assertEqual(alchemist.document.data['processes'],
                         [ProcessDocument.from_process(process).data])

    def test_remove_finished_process(self):
        alchemist = self.repo.create()
        bowls = alchemist.bowls
        alchemist.start_process([1], 1)
        alchemist.document.processes[0].end_time = datetime.now()
        process = alchemist.processes[0]
        process.sync_time()
        alchemist.document.processes[0] = ProcessDocument.from_process(process)

        alchemist.collect_from(alchemist.processes[0])
        self.repo.save(alchemist)

        document = self.registry.fetch(alchemist.document.document_id)
        self.assertEqual(document.processes, [])
        self.assertEqual(alchemist.bowls, bowls)

    def test_correct_number_of_bowls(self):
        alchemist = self.repo.create()
        bowls = alchemist.bowls

        alchemist = self.repo.find(alchemist.alchemist_id)

        self.assertEqual(alchemist.bowls, bowls)

    def test_process_added(self):
        alchemist = self.repo.create()
        bowls = alchemist.bowls
        alchemist.start_process([10], 1)
        self.repo.save(alchemist)

        alchemist = self.repo.find(alchemist.alchemist_id)

        self.assertFalse(alchemist.processes[0].is_dry)
        self.assertEqual(len(alchemist.processes), 1)
        self.assertEqual(alchemist.bowls, bowls-1)

    def test_process_synced(self):
        alchemist = self.repo.create()
        alchemist.start_process([10], 1)
        self.repo.save(alchemist)
        doc = self.registry.fetch(alchemist.document.document_id)
        doc.processes[0].end_time = datetime.now()
        self.registry.save(doc)

        alchemist = self.repo.find(alchemist.alchemist_id)

        self.assertEqual(len(alchemist.processes), 1)
        self.assertTrue(alchemist.processes[0].is_dry)

    def test_remove_crystal(self):
        alchemist = self.repo.create()
        document = AlchemistDocument.from_alchemist(alchemist)
        cdoc = CrystalDocument({'color': 'red', 'size': 200})
        document.crystals.append(cdoc)
        self.registry.save(document)
        alchemist = self.repo.find(alchemist.alchemist_id)

        alchemist.remove_crystal(cdoc.make_crystal())

        self.assertEqual(alchemist.crystals, [])

    def test_remove_just_one_crystal(self):
        alchemist = self.repo.create()
        document = AlchemistDocument.from_alchemist(alchemist)
        cdoc = CrystalDocument({'color': 'red', 'size': 200})
        document.crystals.append(cdoc)
        document.crystals.append(cdoc)
        self.registry.save(document)
        alchemist = self.repo.find(alchemist.alchemist_id)

        alchemist.remove_crystal(cdoc.make_crystal())

        self.assertEqual(alchemist.crystals, [cdoc.make_crystal()])

    def test_remove_inexistent_crystal(self):
        alchemist = self.repo.create()
        document = AlchemistDocument.from_alchemist(alchemist)
        cdoc = CrystalDocument({'color': 'red', 'size': 200})
        cdoc2 = CrystalDocument({'color': 'blue', 'size': 200})
        document.crystals.append(cdoc)
        self.registry.save(document)
        alchemist = self.repo.find(alchemist.alchemist_id)

        with self.assertRaises(ValueError):
            alchemist.remove_crystal(cdoc2.make_crystal())


if __name__ == '__main__':
    unittest.main()
