#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=missing-docstring, too-many-public-methods

"""
    The "View" layer - handlers operate with objects of this level.
"""

import unittest

from crystals.alchemist import AlchemistRepo, Alchemist
from crystals.crystal import Crystal as CrystalModel
from crystals.document import CrystalDocument
from crystals.tests import create_mock_registry
from crystals.utilities import carats_from_molecules
from crystals.view import AlchemistView, CrystalView, CrystalGroupView
from utils.mock import patch, sentinel


class AlchemistViewTest(unittest.TestCase):
    """
        Presentation view of a single Alchemist.
    """

    def test_have_crystals_list(self):
        repo = AlchemistRepo(create_mock_registry())
        inner = repo.create()

        view = AlchemistView(inner)

        self.assertEqual(len(view.crystals), 0)

    def test_have_powders_list(self):
        inner = Alchemist()

        view = AlchemistView(inner)

        self.assertEqual(len(view.powders), len(inner.powders))

    def test_have_coins(self):
        inner = Alchemist()

        view = AlchemistView(inner)

        self.assertEqual(view.coins, inner.coins)

    def test_group_crystals(self):
        inner = Alchemist()
        data = [    # ordered by size
            ('red', 10000, [9998, 10001, 10001]),
            ('green', 800, [798, 802]),
            ('green', 600, [599, 600, 600, 601]),
        ]
        document = inner.document
        for color, _, sizes in data:
            for size in sizes:
                document.crystals.append(CrystalDocument({'color': color,
                                                          'size': size}))
        inner = document.make_alchemist()

        view = AlchemistView(inner)

        self.assertEqual(len(view.crystals), len(data))
        print(view.crystals)
        for crystal, (color, size, _) in zip(view.crystals, data):
            self.assertIsInstance(crystal, CrystalGroupView)
            self.assertEqual(crystal.carat, carats_from_molecules(size))

    def test_order_crystals_by_size(self):
        inner = Alchemist()
        data = [
            ('green', 800, [798, 802]),
            ('red', 10000, [9998, 10001, 10001]),
            ('green', 600, [599, 600, 600, 601]),
        ]
        document = inner.document
        for color, _, sizes in data:
            for size in sizes:
                document.crystals.append(CrystalDocument({'color': color,
                                                          'size': size}))
        inner = Alchemist(document)

        view = AlchemistView(inner)

        sizes = sorted([carats_from_molecules(size) for _, size, _ in data],
                       reverse=True)
        for size, group in zip(sizes, view.crystals):
            self.assertEqual(group.carat, size)


class CrystalViewTest(unittest.TestCase):
    """
        Presentation view of a single crystal.
    """

    def test_display_size_in_carats(self):
        inner = CrystalModel(CrystalDocument({'color': 'red', 'size': 10000}))

        view = CrystalView(inner)

        self.assertEqual(view.carat, carats_from_molecules(10000))


class CrystalGroupViewTest(unittest.TestCase):
    """
        Grouped similar crystals.
    """

    def test_display_size_in_carats(self):
        view = CrystalGroupView(color='red',
                                size=carats_from_molecules(10000))

        self.assertEqual(view.carat, carats_from_molecules(10000))

    def test_display_count(self):
        view = CrystalGroupView(color='red',
                                size=carats_from_molecules(10000))
        sizes = [9997, 9998, 10000, 10002, 10002]
        models = [CrystalModel(CrystalDocument({'color': 'red', 'size': size}))
                  for size in sizes]
        views = [CrystalView(model) for model in models]

        for crystal in views:
            view.add(crystal)

        self.assertEqual(view.count, len(sizes))

    @patch('crystals.view.appraise', return_value=sentinel.coins)
    def test_call_appraise(self, mock_appraise):
        view = CrystalGroupView(color='red',
                                size=carats_from_molecules(10000))
        crystal = CrystalModel(CrystalDocument({'color': 'red', 'size': 10}))
        view.add(CrystalView(crystal))

        cost = view.cost

        mock_appraise.assert_called_once_with(crystal)
        self.assertEqual(cost, sentinel.coins)

    @patch('crystals.view.cash_string_from_coins', return_value=sentinel.cost)
    @patch('crystals.view.appraise')
    def test_coins_string(self, mock_appraise, mock_cash_string):
        view = CrystalGroupView(color='red',
                                size=carats_from_molecules(10000))
        crystal = CrystalModel(CrystalDocument({'color': 'red', 'size': 10}))
        view.add(CrystalView(crystal))

        cost = view.cost_string

        mock_appraise.assert_called_once_with(crystal)
        mock_cash_string.assert_called_once_with(mock_appraise.return_value)
        self.assertEqual(cost, sentinel.cost)


if __name__ == '__main__':
    unittest.main()
