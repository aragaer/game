#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=too-many-public-methods

"""
    Verify that once the club is open, it is operating as it should.
"""

import unittest

import tornado.web

from establishment import Establishment
from establishment.reception import Reception
from establishment.reception.books import Book

from utils.mock import patch, sentinel

from crystals import CrystalsGameClub, CrystalsRecreation
import crystals.handlers


class ClubTest(unittest.TestCase):
    """
        Some checks to verify that our club is a proper establishment.
    """

    def test_reception(self):
        """
            Make sure we don't have cleaners in place of porters.
        """
        club = CrystalsGameClub()

        self.assertIsInstance(club, Establishment)
        self.assertIsNotNone(club.reception)
        self.assertIsInstance(club.reception, Reception)
        self.assertIsNotNone(club.reception.book)
        self.assertIsInstance(club.reception.book, Book)

    @patch('uuid.uuid4')
    @patch('base64.b64encode', return_value=sentinel.encoded)
    def test_secret_cookie(self, encode, uuid):
        """
            For security reasons we change the key every time.
        """
        uuid.return_value.bytes = b'bytes'

        club = CrystalsGameClub()

        self.assertEqual(club.settings['cookie_secret'],
                         sentinel.encoded)
        encode.assert_called_once_with(b'bytesbytes')

    def test_necessary_modules(self):
        """
            Some parts of our establishment have to be set in stone.
        """
        club = CrystalsGameClub()

        self.assertIn('ui_modules', club.settings)
        for module in ['LanguageSwitch', 'NavBarModule', 'InventoryCrystal',
                       'InventoryPowder', 'WalletModule', 'MarketCrystal',
                       'MarketPowder']:
            self.assertIn(module, club.settings['ui_modules'])

    def test_use_recreation(self):
        """
            The whole point of our club really.
        """
        club = CrystalsGameClub()

        self.assertIsInstance(club.recreations, CrystalsRecreation)


class ClubHandlersTest(unittest.TestCase):
    """
        Certain data has to be available.
    """

    def setUp(self):
        with patch('crystals.Establishment.__init__') as establishment:
            CrystalsGameClub()
            self.handlers = establishment.call_args[0][0]

    def test_serve_static(self):
        """
            Some simple furniture.
        """
        self.assertIn((r'/static/([^\/]*)', tornado.web.StaticFileHandler,
                       {'path': 'crystals/static'}),
                      self.handlers)

    def test_serve_bootstrap(self):
        """
            We need some fine decorations.
        """
        self.assertIn((r'/static/bootstrap/(.+)',
                       tornado.web.StaticFileHandler,
                       {'path': 'crystals/static/bootstrap/dist'}),
                      self.handlers)

    def test_serve_jquery_cookie(self):
        """
            Cookie dispenser.
        """
        self.assertIn((r'/static/(jquery.cookie.js)',
                       tornado.web.StaticFileHandler,
                       {'path': 'crystals/static/jquery-cookie'}),
                      self.handlers)

    def test_serve_language_switch(self):
        """
            Check your preferred language here.
        """
        self.assertIn((r'/locale/', crystals.club_handlers.LocaleHandler),
                      self.handlers)


if __name__ == '__main__':
    unittest.main()
