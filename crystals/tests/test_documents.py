#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=missing-docstring, too-many-public-methods

"""
    Testing the documents - persistence layer.
"""

import unittest

from mongomock import MongoClient

from crystals.document import AlchemistDocument, AlchemistRegistry, \
    CrystalDocument, PowderDocument, ProcessDocument
from utils.mock import Mock, patch, sentinel


class AlchemistRegistryTest(unittest.TestCase):

    def setUp(self):
        self.db = MongoClient().db
        self.registry = AlchemistRegistry(self.db)

    def test_need_id_to_retrieve(self):
        self.assertRaises(ValueError, self.registry.fetch)

    def test_cant_fetch_inexistent_id(self):
        self.assertRaises(ValueError, self.registry.fetch, "random data")

    def test_register_returns_new_id(self):
        alchemist_id = self.registry.register()

        self.assertIsNotNone(self.registry.fetch(alchemist_id))


class AlchemistDocumentTest(unittest.TestCase):

    def test_get_id(self):
        data = {'_id': sentinel.id}
        doc = AlchemistDocument(data)

        self.assertEqual(doc.document_id, sentinel.id)

    def test_coins(self):
        data = {'coins': 1000}
        doc = AlchemistDocument(data)

        self.assertEqual(doc.coins, data['coins'])

    @patch('crystals.document.ProcessDocument',
           return_value=sentinel.process)
    def test_load_processes(self, process):
        data = {'processes': [sentinel.process_data]}
        doc = AlchemistDocument(data)

        process.assert_called_once_with(sentinel.process_data)
        self.assertEqual(doc.processes, [sentinel.process])

    def test_save_processes(self):
        process = Mock(spec=ProcessDocument)
        process.data = sentinel.data
        doc = AlchemistDocument()
        doc.processes.append(process)

        self.assertIn('processes', doc.data)
        self.assertEqual(doc.data['processes'], [sentinel.data])

    def test_save_powders(self):
        powder = Mock(spec=PowderDocument)
        powder.data = sentinel.data
        doc = AlchemistDocument()
        doc.powders.append(powder)

        self.assertIn('powders', doc.data)
        self.assertEqual(doc.data['powders'], [sentinel.data])

    @patch('crystals.document.PowderDocument', return_value=sentinel.powder)
    def test_load_powders(self, powder):
        data = {'powders': [sentinel.powder_data]}
        doc = AlchemistDocument(data)

        powder.assert_called_once_with(sentinel.powder_data)
        self.assertEqual(doc.powders, [sentinel.powder])

    def test_save_crystals(self):
        crystal = Mock(spec=CrystalDocument)
        crystal.data = sentinel.data
        doc = AlchemistDocument()
        doc.crystals.append(crystal)

        self.assertIn('crystals', doc.data)
        self.assertEqual(doc.data['crystals'], [sentinel.data])

    @patch('crystals.document.CrystalDocument', return_value=sentinel.crystal)
    def test_load_crystals(self, crystal):
        data = {'crystals': [sentinel.crystal_data]}
        doc = AlchemistDocument(data)

        crystal.assert_called_once_with(sentinel.crystal_data)
        self.assertEqual(doc.crystals, [sentinel.crystal])


class CrystalDocumentTest(unittest.TestCase):

    @patch('crystals.document.hsl_from_color_name',
           return_value=sentinel.hsl)
    def test_color_from_hsl(self, hsl_func):
        data = {'color': sentinel.color, 'size': sentinel.mass}

        crystal = CrystalDocument(data)

        self.assertEqual(crystal.color, sentinel.color)
        self.assertEqual(crystal.hsl, sentinel.hsl)
        hsl_func.assert_called_once_with(sentinel.color)

    @patch('crystals.document.color_name_from_hsl',
           return_value=sentinel.color)
    def test_hsl_from_color(self, color_func):
        data = {'hsl': sentinel.hsl, 'size': sentinel.mass}

        crystal = CrystalDocument(data)

        self.assertEqual(crystal.color, sentinel.color)
        self.assertEqual(crystal.hsl, sentinel.hsl)
        color_func.assert_called_once_with(sentinel.hsl)

    @patch('crystals.document.hsl_from_color_name',
           return_value=sentinel.hsl)
    def test_writable_count(self, _):
        crystal = CrystalDocument({'color': sentinel.color,
                                   'size': sentinel.size,
                                   'count': sentinel.count})

        crystal.count = sentinel.count2

        self.assertEqual(crystal.data['count'], sentinel.count2)


class PowderDocumentTest(unittest.TestCase):

    def test_data(self):
        data = {'color': 'purple', 'mass': sentinel.mass}
        powder = PowderDocument(data)

        powder.mass = sentinel.other_mass

        self.assertEquals(powder.data['mass'], sentinel.other_mass)

    def test_from_document(self):
        document = PowderDocument({'color': 'purple', 'mass': 42})

        powder = document.make_powder()

        self.assertEqual(powder.color, 'purple')
        self.assertEqual(powder.mass, 42)

    def test_to_document(self):
        document = PowderDocument({'color': 'yellow', 'mass': 42})
        powder = document.make_powder()

        powder.mass = 35

        document = PowderDocument.from_powder(powder)
        self.assertEqual(document.mass, 35)


class ProcessDocumentTest(unittest.TestCase):

    def test_data(self):
        data = {'color': sentinel.color, 'water': sentinel.water,
                'end_time': sentinel.end_time, 'details': sentinel.details}
        process = ProcessDocument(data)
        self.assertEquals(process.data, data)

    def test_fields(self):
        data = {'color': sentinel.color, 'water': sentinel.water,
                'end_time': sentinel.end_time, 'details': sentinel.details}
        process = ProcessDocument(data)
        self.assertEquals(process.end_time, sentinel.end_time)
        self.assertEquals(process.details, sentinel.details)


if __name__ == '__main__':
    unittest.main()
