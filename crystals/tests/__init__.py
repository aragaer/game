#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=too-many-public-methods

"""
    Unit tests.
"""

# Fake gettext stuff
import builtins

from tornado.testing import AsyncHTTPTestCase


setattr(builtins, '_', lambda x: x)


def create_mock_registry():
    """
        Prepare an alchemist registry based on mongomock.
    """
    from mongomock import MongoClient

    from crystals.document import AlchemistRegistry

    return AlchemistRegistry(MongoClient().db)


class CrystalsTestCase(AsyncHTTPTestCase):
    """
        Base class for tornado tests.
    """

    alchemist = None
    reception = None
    registry = None
    club = None
    repo = None

    def renew_alchemist(self):
        "Return the test model."
        return self.repo.find(self.alchemist.document.document_id)

    def get_app(self):
        return self.club

    def setUp(self):
        from utils.mock import patch, sentinel

        from crystals import CrystalsRecreation, CrystalsGameClub
        from crystals.alchemist import AlchemistRepo
        from crystals.handlers import CrystalHandler

        self.registry = create_mock_registry()
        self.repo = AlchemistRepo(self.registry)
        self.alchemist = self.repo.create()

        patcher = patch.object(CrystalHandler, 'get_current_user',
                               return_value=sentinel.user)
        patcher.start()
        self.addCleanup(patcher.stop)
        patcher = patch.object(CrystalHandler, 'get_user_locale',
                               return_value=None)
        patcher.start()
        self.addCleanup(patcher.stop)
        patcher = patch.object(CrystalHandler, 'get_current_alchemist',
                               side_effect=self.renew_alchemist)
        patcher.start()
        self.addCleanup(patcher.stop)

        recreation = CrystalsRecreation(self.registry)
        self.club = CrystalsGameClub(reception=sentinel.reception,
                                     recreations=recreation)

        super().setUp()
