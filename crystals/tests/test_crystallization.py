#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=missing-docstring, too-many-public-methods

import unittest

from crystals.color import Color
from crystals.colorless import Solution as ColorlessSolution, \
    Powder as ColorlessPowder
from crystals.config import NUCLEUS_SIZE as N, GROWTH_SIZE
from crystals.process import Process, Solution, Sediment
from utils.mock import patch


class TestSolution(unittest.TestCase):

    def test_evaporate_undersaturated(self):
        result = ColorlessSolution(1000, 10).coagulate()

        self.assertEqual(len(result), 0)

    def test_nuclei_limits(self):
        from crystals.config import NUCLEATION_RATE, \
            SATURATED_CONCENTRATION
        tcs = [
            (10, 10000),
            (100, 100),
        ]
        for water, matter in tcs:
            supersaturation = (matter / water - SATURATED_CONCENTRATION)

            rate1 = water * NUCLEATION_RATE * supersaturation ** 3
            rate2 = water * NUCLEATION_RATE * supersaturation ** 4
            if rate1 > rate2:
                rate1, rate2 = rate2, rate1
            n_max = (matter - SATURATED_CONCENTRATION * water) // N

            nuclei_min, nuclei_max = ColorlessSolution(
                matter, water).nucleation_limits()
            if supersaturation <= 0:
                self.assertEqual(nuclei_max, 0)
                self.assertEqual(nuclei_min, 0)
            elif n_max < rate1:
                self.assertEqual(n_max, nuclei_max)
                self.assertEqual(n_max, nuclei_min)
            elif n_max < rate2:
                self.assertEqual(nuclei_max, n_max)
                self.assertEqual(nuclei_min, int(rate1))
            else:
                self.assertEqual(nuclei_max, int(rate2))
                self.assertEqual(nuclei_min, int(rate1))

    def test_coagulate(self):
        tcs = [
            (10, 10000, 910),
            (100, 20000, 1010),
        ]
        for water, matter, rnd in tcs:
            with patch('random.randint', return_value=rnd):
                suspension = ColorlessSolution(matter, water).coagulate()

            self.assertEqual(suspension, [(N, rnd)])

    def test_fallout(self):
        tcs = [
            (N * 1000, [(N, 1000)]),
            (N * 12 + 3, [(N, 9), (N + 1, 3)]),
            (N * 5 + 7, [(N + 1, 3), (N + 2, 2)]),
        ]
        for matter, result in tcs:
            suspension = ColorlessSolution(matter, 0).coagulate()

            self.assertEqual(suspension, result)

    def test_absorbable(self):
        tcs = [
            (10, 10000, 9000),
            (99, 10000, 100),
            (100, 100, 0),
            (0, 100, 100),
        ]
        for water, matter, expected in tcs:
            self.assertEqual(ColorlessSolution(matter, water).absorbable(),
                             expected)


class TestPowder(unittest.TestCase):

    def test_grow(self):
        tcs = [
            {
                'crystals': {N: 100},
                'matter': N * 100,
                'expected': {N * 2: 100},
            },
            {
                'crystals': {N: 100},
                'matter': 100,
                'expected': {N + 1: 100},
            },
            {
                'crystals': {N: 100},
                'matter': 110,
                'expected': {N + 1: 100},
            },
            {
                'crystals': {N: 100, N * 2: 50},
                'matter': 200,
                'expected': {N + 1: 100, N * 2 + 1: 50},
            },
            {
                'crystals': {N: 100, N * 2: 50},
                'matter': 10000,
                'expected': {N + GROWTH_SIZE: 100, N * 2 + GROWTH_SIZE: 50},
            },
            {
                'crystals': {},
                'matter': N * 100,
                'expected': {},
            },
        ]
        for i, case in enumerate(tcs):
            print("Starting TC", i)
            powder = ColorlessPowder()
            mass = 0
            crystals = {}
            for size, count in case['crystals'].items():
                mass += size * count
                crystals[size] = count
            powder.update(crystals)

            result = powder.grow(case['matter'])

            self.assertEqual(len(powder.keys()),
                             len(case['expected'].keys()))
            new_mass = 0
            for size, count in case['expected'].items():
                self.assertIn(size, powder)
                self.assertEqual(powder[size], count)
                new_mass += size * count
            self.assertEqual(result, new_mass - mass)
            print("Passed TC", i)

    def test_distribute_suspension(self):
        tcs = [
            {
                'crystals': {},
                'suspension': [(N, 100)],
                'expected': {N: 100},
            },
            {
                'crystals': {N: 100},   # N * 100 is equal to MISS_RATE
                'suspension': [(N, 100)],
                'expected': {N: 100, N * 2: 50},
            },
            {
                'crystals': {N: 100, N * 4: 25},
                'suspension': [(N, 30)],
                'expected': {N * 5: 10, N * 4: 15, N * 2: 10, N: 100},
            },
            {
                'crystals': {N: 100},
                'suspension': [(N, 270), (N + 1, 250)],
                'expected': {N: 135, N + 1: 125,
                             N * 3 + 1: 40, N * 4 + 1: 35, N * 4 + 2: 25},
            },
        ]
        for i, case in enumerate(tcs):
            print("Starting TC", i)
            powder = ColorlessPowder()
            crystals = {}
            for size, count in case['crystals'].items():
                crystals[size] = count
            powder.update(crystals)

            powder.add_suspension(case['suspension'])

            self.assertEqual(len(powder.keys()),
                             len(case['expected'].keys()))
            for size, count in case['expected'].items():
                self.assertIn(size, powder)
                self.assertEqual(powder[size], count)
            print("TC", i, "passed")


class TestEvaporation(unittest.TestCase):

    def test_undersaturated_evaporation(self):
        solution = Solution(Color.from_name('green'), 1, 99)

        suspension = solution.coagulate()

        self.assertEqual(suspension.mass, 0)

    def test_first_nucleation(self):
        tcs = [
            (10, 10000, 910, 910, 910),
            (100, 20000, 1010, 1010, 1010),
            (100, 1000, 0, 0, 0),
            (100, 10000, 0, 0, 0),
            (95, 10000, 0, 1, 0),
            (95, 10000, 0, 1, 1),
            (94, 10000, 0, 2, 2),
            (94, 9990, 0, 2, 2),
            (100, 11000, 1, 15, 10),
            (100, 10500, 0, 1, 0),
        ]
        for water, matter, rnd_min, rnd_max, rnd in tcs:
            process = Process(solution=Solution(Color.from_name('red'),
                                                matter, water))

            with patch('random.randint', return_value=rnd) as mock_randint:
                process.evaporate()
                mock_randint.assert_called_once_with(rnd_min, rnd_max)

            self.assertEqual(process.solution.matter + N * rnd, matter)

    def test_growth(self):
        tcs = [
            {
                'water': 50,
                'matter': 5000,
                'crystals': {N: 100},
                'expected': {N + 1: 100},
            },
            {
                'water': 50,
                'matter': 4900 + GROWTH_SIZE * 100,
                'crystals': {N: 100},
                'expected': {N + GROWTH_SIZE: 100},
            },
        ]
        for i, case in enumerate(tcs):
            print("Starting TC", i)
            process = Process(solution=Solution(Color.BLACK,
                                                case['matter'], case['water']),
                              sediment=Sediment(case['crystals']))

            process.evaporate()

            old_crystal_mass = sum(s * c for s, c in case['crystals'].items())
            new_crystal_mass = sum(s * c for s, c in case['expected'].items())
            self.assertEqual(process.solution.matter + new_crystal_mass,
                             case['matter'] + old_crystal_mass)
            print("TC", i, "passed")

    def test_grow_and_settle(self):
        tcs = [
            {
                'water': 50,
                'matter': 6000,
                'rnd': (0, 0, 0),
                'crystals': {N: 100},
                'expected': {N + GROWTH_SIZE: 100},
            },
            {
                'water': 40,
                'matter': 6000,
                'rnd': (8, 110, 30),
                'crystals': {N: 100},
                'expected': {
                    N: 10,
                    N + GROWTH_SIZE: 80,
                    N * 2 + GROWTH_SIZE: 20
                },
            },
            {
                'water': 40,
                'matter': 6000,
                'rnd': (8, 110, 91),
                'crystals': {N: 100},
                'expected': {
                    N: 31,
                    N + GROWTH_SIZE: 40,
                    N * 2 + GROWTH_SIZE: 60
                },
            },
            {
                'water': 50,
                'matter': 6000,
                'rnd': (4, 84, 36),
                'crystals': {N * 9: 10},
                'expected': {
                    N: 18,
                    N * 10 + GROWTH_SIZE: 2,
                    N * 11 + GROWTH_SIZE: 8
                },
            },
            {
                'water': 50,
                'matter': 6000,
                'rnd': (3, 69, 32),
                'crystals': {N * 9: 5, N * 4: 10},
                'expected': {
                    N: 16,
                    N * 10 + GROWTH_SIZE: 2,
                    N * 11 + GROWTH_SIZE: 3,
                    N * 4 + GROWTH_SIZE: 2,
                    N * 5 + GROWTH_SIZE: 8,
                },
            },
        ]
        for i, case in enumerate(tcs):
            print("Starting TC", i)
            process = Process(solution=Solution(Color.BLACK, case['matter'],
                                                case['water']),
                              sediment=Sediment(case['crystals']))

            rnd_min, rnd_max, rnd = case['rnd']
            with patch('random.randint', return_value=rnd) as mock_randint:
                process.evaporate()
                mock_randint.assert_called_once_with(rnd_min, rnd_max)

            old_crystal_mass = sum(s * c for s, c in case['crystals'].items())
            new_crystal_mass = sum(s * c for s, c in case['expected'].items())
            self.assertEqual(process.solution.matter + new_crystal_mass,
                             case['matter'] + old_crystal_mass)
            print("TC", i, "passed")

    def test_drying_up(self):
        tcs = [
            {
                'matter': 180,
                'crystals': {N * 2 - 3: 50},
                'expected': {N * 2: 49, N * 3: 1, N: 2},
            },
            {
                'matter': 1000,
                'crystals': {N: 50},
                'expected': {
                    N + GROWTH_SIZE: 25,
                    N * 2 + GROWTH_SIZE: 25,
                    N: 25,
                },
            },
            {
                'matter': 10000,
                'crystals': {},
                'expected': {N: 1000},
            },
        ]
        for i, case in enumerate(tcs):
            print("Starting TC", i)
            process = Process(solution=Solution(Color.BLACK,
                                                case['matter'], 1),
                              sediment=Sediment(case['crystals']))

            process.evaporate()

            self.assertEqual(process.solution.matter, 0)
            old_crystal_mass = sum(s * c for s, c in case['crystals'].items())
            new_crystal_mass = sum(s * c for s, c in case['expected'].items())
            self.assertEqual(new_crystal_mass,
                             case['matter'] + old_crystal_mass)
            print("TC", i, "passed")


if __name__ == '__main__':
    unittest.main()
