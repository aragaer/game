#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=missing-docstring, too-many-public-methods

import unittest

from bs4 import BeautifulSoup
import tornado.testing

from crystals.tests import CrystalsTestCase
from crystals.utilities import carats_from_molecules, grams_from_molecules, \
    molecules_from_grams


class TestBowlRender(CrystalsTestCase):

    def _try_fetch_soup(self):
        response = self.fetch('/preparation/')
        if response.code != 200:
            raise unittest.SkipTest("Failed to load the page")
        return BeautifulSoup(response.body.decode('utf-8'))

    def test_preparation_powders_list(self):
        self.alchemist.document.powders[0].mass = 6000
        self.registry.save(self.alchemist.document)
        self.alchemist = self.alchemist.document.make_alchemist()

        soup = self._try_fetch_soup()

        powders_label = soup.find('label', text='Powders:')
        powders_list = soup.find('div',
                                 id=powders_label.get('for')).find_all('li')

        self.assertEqual(len(powders_list), len(self.alchemist.powders) + 1)
        for powder, row in zip(self.alchemist.powders, powders_list[1:]):
            grams = grams_from_molecules(powder.mass)
            cells = row.find_all('span')
            self.assertEqual(cells[0].text, '%gg' % grams)
            self.assertEqual(cells[1].text, powder.color)
            inp = cells[2].find('input')
            self.assertEqual(int(inp.get('max')), grams)
            self.assertEqual(inp.get('name'), 'p')
            self.assertEqual(inp.get('data-color'), powder.color)

    def test_logout_present_preparation(self):
        soup = self._try_fetch_soup()

        link = soup.find('a', text='Logout')
        self.assertEqual(link.get('href'), '/logout/')

    def test_cancel_present_preparation(self):
        soup = self._try_fetch_soup()

        link = soup.find('a', text='Cancel')
        self.assertEqual(link.get('href'), '/lab/')

    def test_seed_list(self):
        from crystals.document import CrystalDocument
        data = [
            ('red', 1, 1),
            ('blue', 3, 5),
            ('green', 1.4, 7),
        ]
        seeds = [CrystalDocument({'color': c, 'size': molecules_from_grams(s),
                                  'count': cnt}) for c, s, cnt in data]
        document = self.alchemist.document
        document.crystals = seeds[:]
        self.registry.save(document)

        soup = self._try_fetch_soup()

        seed_label = soup.find('label', text='Seed:')
        seed_list = soup.find('select',
                              id=seed_label.get('for')).find_all('option')

        self.assertEqual(len(seeds), len(seed_list) - 1)
        self.assertEqual(seed_list[0].text, "No seed")
        self.assertEqual(seed_list[0].get('value'), '-1')
        sorted_seeds = sorted(seeds, key=lambda c: c.size, reverse=True)
        for crystal, row, num in zip(sorted_seeds, seed_list[1:],
                                     range(len(seeds))):
            text = "%s %d-carat crystal" % (
                crystal.color.capitalize(),
                carats_from_molecules(crystal.size))
            self.assertEqual(row.text, text)
            self.assertEqual(row.get('value'), str(num))


if __name__ == '__main__':
    tornado.testing.main()
