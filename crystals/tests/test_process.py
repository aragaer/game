#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=missing-docstring, too-many-public-methods

import unittest

from crystals.process import Process, Solution, Sediment
from crystals.color import Color
from crystals.document import CrystalDocument, PowderDocument
from crystals.powder import Powder
from crystals.tests import create_mock_registry
from crystals.utilities import molecules_from_grams
from utils.mock import Mock, patch, sentinel


class ProcessTest(unittest.TestCase):

    def setUp(self):
        self.registry = create_mock_registry()

    def test_collect(self):
        process = Process(solution=Solution(Color.from_name('red'),
                                            10000, 100))
        while not process.is_dry:
            process.evaporate()

        powder, crystals = process.collect()

        mass = 0
        if powder is not None:
            self.assertEqual(powder.color, 'red')
            mass += powder.mass
        for crystal in crystals:
            self.assertEqual(crystal.color, 'red')
            mass += crystal.size * crystal.count
        self.assertEqual(mass, 10000)

    def test_collect_no_powder(self):
        process = Process(sediment=Sediment({10000: 1}),
                          solution=Solution(Color.from_name('yellow'), 0, 0))

        powder, _ = process.collect()

        self.assertIsNone(powder)

    def test_evaporation_end_time(self):
        from datetime import datetime, timedelta
        from crystals.config import EVAPORATION_TICK_TIME
        process = Process(solution=Solution((0, 0, 0), 0, 10))
        expected = timedelta(seconds=EVAPORATION_TICK_TIME*10)

        process.start_evaporation()
        delta = process.end_time - datetime.now()

        self.assertLessEqual(delta, expected)
        self.assertLessEqual(expected - delta, timedelta(seconds=1))

    def test_crystals(self):
        tcs = [{'powder': {10: 120, 40: 140, 210: 15, 420: 5},
                'crystals': {200: 15, 400: 5}},
               {'powder': {10: 120, 40: 140, 210: 15, 230: 5},
                'crystals': {200: 20}}]
        for case in tcs:
            _, crystals = Process(sediment=Sediment(case['powder'])).collect()
            self.assertEqual(dict((c.size, c.count) for c in crystals),
                             case['crystals'])

    def test_sediment(self):
        tcs = [
            {
                'powder': {10: 120, 40: 140, 210: 15, 420: 5},
                'sediment': 1200 + 5600 + 150 + 100,
            },
            {
                'powder': {10: 120, 40: 140, 210: 15, 230: 5},
                'sediment': 1200 + 5600 + 150 + 150,
            },
        ]
        for case in tcs:
            process = Process(sediment=Sediment(case['powder']))
            powder, _ = process.collect()
            self.assertEqual(powder.mass, case['sediment'])

    def test_collect_not_dry(self):
        process = Process.create(
            water=5,
            powders=[Powder(5, Color.from_name('green'))])

        with self.assertRaises(ValueError):
            process.collect()

    def test_collect_with_seed(self):
        from crystals.color import mix_hsl, color_name_from_hsl, hsl_from_rgb
        seed_size = molecules_from_grams(0.4)
        powder_size = molecules_from_grams(0.2)
        grown_size = molecules_from_grams(0.8)
        seed_hsl = hsl_from_rgb((255, 0, 0))
        solution_hsl = hsl_from_rgb((0, 255, 0))
        expected_hsl = mix_hsl((seed_hsl, 1), (solution_hsl, 1))

        seed = CrystalDocument({'size': seed_size, 'hsl': seed_hsl})
        process = Process(sediment=Sediment({powder_size: 5, grown_size: 1},
                                            Color.from_hsl(solution_hsl),
                                            seed=seed))

        powder, crystals = process.collect()

        self.assertIsNone(powder)
        self.assertEqual(len(crystals), 2)
        crystals = sorted(crystals, key=lambda x: x.size)
        self.assertEqual(crystals[0].size, powder_size)
        self.assertEqual(crystals[1].size, grown_size)
        self.assertEqual(crystals[0].count, 5)
        self.assertEqual(crystals[1].count, 1)
        self.assertEqual(crystals[0].hsl, solution_hsl)
        self.assertAlmostEqual(crystals[1].hsl[0], expected_hsl[0])
        self.assertAlmostEqual(crystals[1].hsl[1], expected_hsl[1])
        self.assertEqual(crystals[1].color, color_name_from_hsl(expected_hsl))

    @patch('crystals.process.mix_hsl', return_value=(1, 0, 0))
    def test_create_solution(self, mock_mix):
        powder1 = Mock()
        powder1.hsl, powder1.mass = sentinel.hsl1, 10
        powder2 = Mock()
        powder2.hsl, powder2.mass = sentinel.hsl2, 20
        powder3 = Mock()
        powder3.hsl, powder3.mass = sentinel.hsl3, 30
        powders = [powder1, powder2, powder3]

        process = Process.create(water=50, powders=powders)

        mock_mix.assert_called_once_with((sentinel.hsl1, 10),
                                         (sentinel.hsl2, 20),
                                         (sentinel.hsl3, 30))
        self.assertEqual(process.solution.color.hsl, (1, 0, 0))
        self.assertEqual(process.solution.volume, 50)
        self.assertEqual(process.solution.matter, 60)

    def test_create_solution_no_water(self):
        with self.assertRaises(ValueError):
            Process.create(water=0,
                           powders=[Powder(5, Color.from_name('blue'))])

    def test_create_with_seed(self):
        from crystals.document import ProcessDocument
        process = Process.create(water=1,
                                 powders=[Powder(1, Color.from_name('cyan'))],
                                 seed=CrystalDocument({'size': 200,
                                                       'color': 'cyan'}))
        pdoc = ProcessDocument.from_process(process)
        self.assertEqual(pdoc.details['powder'], [(200, 1)])


if __name__ == '__main__':
    unittest.main()
