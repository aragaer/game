#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=missing-docstring, too-many-public-methods
# pylint: disable=too-few-public-methods

import json
import unittest

import sockjs.tornado
import tornado.testing

from utils.mock import Mock, patch, sentinel

from crystals.alchemist import AlchemistRepo
from crystals.document import CrystalDocument
from crystals.handlers import CrystalAsyncHandler, MarketPreviewConnection, \
    CrystalsConnection
from crystals.market import cash_string_from_coins, appraise, POWDERS
from crystals.tests import create_mock_registry
from crystals.utilities import time_str, molecules_from_grams
from crystals.view import AlchemistView


class ConnectionAuthTest(unittest.TestCase):

    @patch('crystals.handlers.to_unicode', return_value=sentinel.string)
    def test_decode(self, mock_unicode):
        connection = CrystalAsyncHandler(None)
        connection.session = Mock()
        connection.session.handler.get_secure_cookie.return_value = \
            sentinel.secure

        result = connection.decode(sentinel.cookie, sentinel.data)

        connection.session.handler.get_secure_cookie.assert_called_onec_with(
            sentinel.cookie, value=sentinel.data)
        mock_unicode.assert_called_once_with(sentinel.secure)
        self.assertEqual(result, sentinel.string)

    # pylint: disable=maybe-no-member
    @patch.object(sockjs.tornado.SockJSConnection, 'send')
    @patch('crystals.handlers.CrystalAsyncHandler.decode',
           return_value=sentinel.badge)
    def test_get_current_alchemist(self, *_):
        registry = create_mock_registry()
        repo = AlchemistRepo(registry)
        alchemist = repo.create()

        reception = Mock()
        reception.get_note.return_value = alchemist.alchemist_id
        recreation = Mock()
        recreation.registry = registry

        session = Mock()
        session.handler.application.reception = reception
        session.server.settings = {'recreation': recreation}

        connection = CrystalAsyncHandler(session)

        alch = connection.get_current_alchemist()

        reception.get_note.assert_called_once_with(sentinel.badge,
                                                   'alchemist')
        self.assertEqual(alch.alchemist_id, alchemist.alchemist_id)


class AuthenticatedConnectionTest(unittest.TestCase):

    alchemist = None
    connection_class = None
    _send = None

    def setUp(self):
        self.alchemist = AlchemistRepo(create_mock_registry()).create()

        self._send = Mock()
        patcher = patch.object(sockjs.tornado.SockJSConnection, 'send',
                               self._send)
        patcher.start()
        self.addCleanup(patcher.stop)
        patcher = patch.object(CrystalAsyncHandler, 'get_current_alchemist',
                               return_value=self.alchemist)
        patcher.start()
        self.addCleanup(patcher.stop)

        self.connection = self.connection_class(None)

    def sent_response(self):
        self.assertEqual(self._send.call_count, 1)
        result = self._send.call_args[0][0]
        self._send.reset_mock()
        return result


class AuthTestMixin(object):

    path = None

    def test_auth(self):
        connection_info = Mock()
        connection_info.path = self.path

        self.connection.on_open(connection_info)

        self.assertEqual(self.connection.alchemist.inner, self.alchemist)


def _water_to_time_str(water):
    from datetime import timedelta
    from crystals.config import EVAPORATION_TICK_TIME

    return time_str(timedelta(seconds=EVAPORATION_TICK_TIME*water))


class TestAsyncHandler(AuthenticatedConnectionTest):

    connection_class = CrystalsConnection
    path = '/bowl_preview'

    def test_demultiplex(self):
        self.connection.handle_solution = Mock()
        request = json.dumps({'type': 'preview', 'data': 'some data'})

        self.connection.on_message(request)

        self.connection.handle_solution.assert_called_once_with('some data')

    def test_empty_preview(self):
        request = json.dumps({'type': 'preview', 'data': {'w': 0, 'p': {}}})

        self.connection.on_message(request)

        result = json.loads(self.sent_response())
        self.assertEqual(result,
                         {'type': 'preview', 'data': ["Bowl is empty"]})

    def test_water_preview(self):
        request = json.dumps({'type': 'preview', 'data': {'w': 10, 'p': {}}})

        self.connection.on_message(request)

        result = json.loads(self.sent_response())
        self.assertEqual(result,
                         {'type': 'preview', 'data': ["10 ml of water"]})

    def test_single_color_preview(self):
        for color in ['red', 'green', 'blue']:
            request = json.dumps({'type': 'preview',
                                  'data': {'w': 20, 'p': {color: 1}}})

            self.connection.on_message(request)

            result = json.loads(self.sent_response())
            expected_result = {
                'type': 'preview',
                'data': [
                    "20 ml of %s solution" % color,
                    "Concentration: low (optimal at 10 ml)",
                    "Evaporation will take %s" % _water_to_time_str(20),
                ],
            }
            self.assertEqual(result, expected_result)

    def test_two_basic_color_preview(self):
        tcs = [
            ('red', 'green', 'yellow'),
            ('red', 'blue', 'purple'),
            ('blue', 'green', 'cyan'),
        ]

        for color1, color2, color in tcs:
            request = json.dumps({'type': 'preview',
                                  'data': {'w': 30,
                                           'p': {color1: 1, color2: 1}}})

            self.connection.on_message(request)

            result = json.loads(self.sent_response())
            expected_result = {
                'type': 'preview',
                'data': [
                    "30 ml of %s solution" % color,
                    "Concentration: low (optimal at 20 ml)",
                    "Evaporation will take %s" % _water_to_time_str(30),
                ],
            }
            self.assertEqual(result, expected_result)

    def test_concentration_preview(self):
        tcs = [
            ({'red': 1}, 20),
            ({'red': 5}, 20),
        ]
        for powders, water in tcs:
            request = json.dumps({'type': 'preview',
                                  'data': {'w': water, 'p': powders}})

            self.connection.on_message(request)

            result = json.loads(self.sent_response())
            optimal = 10 * sum(powders.values())
            if water > optimal:
                expected = "Concentration: low (optimal at %d ml)" % optimal
            else:
                expected = "Concentration: high (optimal at %d ml)" % optimal
            self.assertEqual(result['data'][1], expected)


class TestMarketPreviewConnection(AuthenticatedConnectionTest, AuthTestMixin):

    connection_class = MarketPreviewConnection
    path = '/market_preview'

    def test_single_crystal_preview(self):
        crystals = [
            ('red', 1),
            ('blue', 0.6),
            ('green', 1.4)
        ]
        for color, grams in crystals:
            crystal = CrystalDocument(
                {'color': color, 'size': molecules_from_grams(grams)})
            self.alchemist.document.crystals.append(crystal)
        self.connection.alchemist = AlchemistView(self.alchemist)

        # In UI crystals are sorted by size
        sell_list = sorted(self.alchemist.crystals,
                           key=lambda crystal: crystal.size,
                           reverse=True)
        for num, crystal in enumerate(sell_list):
            data = [0, 0, 0]
            data[num] = 1
            request = json.dumps({'type': 'preview', 'data': {'c': data}})
            cash_string = cash_string_from_coins(appraise(crystal))

            self.connection.on_message(request)

            result = json.loads(self.sent_response())
            self.assertEqual(result, {'type': 'preview',
                                      'data': {'coins': cash_string,
                                               'label': 'You will get'}})

    def test_buy_powder_preview(self):
        self.alchemist.coins = 100000
        self.connection.alchemist = self.alchemist
        tcs = [
            [10, 5, 2],
            [0, 0, 2],
            [2, 2, 8, 0, 0, 7],
        ]
        for case in tcs:
            request = json.dumps({'type': 'preview', 'data': {'p': case}})
            coins = sum(a*p.price for a, p in zip(case, POWDERS))
            cash_string = cash_string_from_coins(coins)

            self.connection.on_message(request)

            result = json.loads(self.sent_response())
            self.assertEqual(result, {'type': 'preview',
                                      'data': {'coins': cash_string,
                                               'label': 'You will pay'}})

    def test_buy_bowl_preview(self):
        self.alchemist.coins = 300000
        self.connection.alchemist = self.alchemist
        for count in range(5):
            request = json.dumps({'type': 'preview', 'data': {'u': [count]}})
            cash_string = cash_string_from_coins(count * 50000)

            self.connection.on_message(request)

            result = json.loads(self.sent_response())
            self.assertEqual(result, {'type': 'preview',
                                      'data': {'coins': cash_string,
                                               'label': 'You will pay'}})

    def test_buy_cant_afford(self):
        self.connection.alchemist = self.alchemist
        for count in range(1, 5):
            request = json.dumps({'type': 'preview', 'data': {'u': [count]}})
            cash_string = cash_string_from_coins(count * 50000)

            self.connection.on_message(request)

            result = json.loads(self.sent_response())
            self.assertEqual(result, {'type': 'preview',
                                      'data': {'coins': cash_string,
                                               'label': 'You will pay',
                                               'cant_afford': True}})


if __name__ == '__main__':
    tornado.testing.main()
