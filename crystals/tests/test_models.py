#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=missing-docstring, too-many-public-methods

"""
    Testing powder and crystal here.
"""

import unittest

from crystals.powder import Powder
from crystals.color import Color, rgb_from_color_name, color_name_from_rgb, \
    mix_rgb, color_name_from_hsl, mix_hsl, hsl_from_color_name


class UnitTestColors(unittest.TestCase):

    def test_parse(self):
        self.assertEqual(rgb_from_color_name('red'), (255, 0, 0))
        self.assertEqual(rgb_from_color_name('green'), (0, 255, 0))
        self.assertEqual(rgb_from_color_name('blue'), (0, 0, 255))

    def test_format(self):
        self.assertEqual(color_name_from_rgb((255, 0, 0)), 'red')
        self.assertEqual(color_name_from_rgb((0, 255, 0)), 'green')
        self.assertEqual(color_name_from_rgb((0, 0, 255)), 'blue')
        self.assertEqual(color_name_from_rgb((255, 255, 0)), 'yellow')
        self.assertEqual(color_name_from_rgb((0, 255, 255)), 'cyan')
        self.assertEqual(color_name_from_rgb((255, 0, 255)), 'purple')
        self.assertEqual(color_name_from_rgb((128, 128, 128)), 'gray')

        # some additional tests that fail with LAB color system
        self.assertEqual(color_name_from_rgb((63, 174, 192)), 'cyan')
        self.assertEqual(color_name_from_rgb((191, 191, 64)), 'yellow')

    def test_mix(self):
        tcs = [
            (((255, 0, 0), 5), ((0, 255, 0), 5), (191, 191, 64)),
            (((0, 255, 0), 5), ((0, 255, 0), 5), (0, 255, 0)),
            (((255, 0, 0), 5), ((0, 255, 255), 5), (128, 128, 128)),
            (((0, 0, 0), 0), ((0, 0, 255), 20), (0, 0, 255)),
            (((255, 32, 0), 5), ((255, 0, 32), 5), (255, 5, 5)),
        ]
        for component1, component2, expected in tcs:
            result = mix_rgb(component1, component2)
            print("expected:", expected, "result:", result)
            for res, exp in zip(result, expected):
                self.assertTrue(abs(res - exp) < 5,
                                "%g is too different from %g" % (res, exp))


# pylint: disable=star-args
class UnitTestUtilities(unittest.TestCase):
    def test_time_str(self):
        from datetime import timedelta
        from crystals.utilities import time_str

        tcs = [
            ({'minutes': 10, 'seconds': 45}, '10 minutes 45 seconds',
             '00:10:45'),
            ({'minutes': 14, 'seconds': 1}, '14 minutes 1 second',
             '00:14:01'),
            ({'minutes': 16, 'seconds': 45}, 'about 17 minutes', '00:17'),
            ({'minutes': 50, 'seconds': 29}, 'about 50 minutes', '00:50'),
            ({'hours': 1, 'minutes': 55, 'seconds': 29},
             'about 1 hour 55 minutes', '01:55'),
            ({'hours': 2, 'minutes': 55, 'seconds': 30}, 'about 3 hours',
             '03:00'),
            ({'hours': 2, 'minutes': 37, 'seconds': 30},
             'about 2 hours 40 minutes', '02:40'),
        ]

        for delta_dict, time_string, time_string_short in tcs:
            delta = timedelta(**delta_dict)
            self.assertEqual(time_str(delta), time_string)
            self.assertEqual(time_str(delta, True), time_string_short)


class PowderTest(unittest.TestCase):

    def test_powder_split(self):
        powder1 = Powder(15, Color.from_name('cyan'))

        powder2 = powder1.split(5)

        self.assertEqual(powder1.color, 'cyan')
        self.assertEqual(powder2.color, 'cyan')
        self.assertEqual(powder1.mass, 10)
        self.assertEqual(powder2.mass, 5)

    def test_powder_mix(self):
        powder1 = Powder(10, Color.from_name('green'))
        powder2 = Powder(5, Color.from_name('blue'))

        powder1.mix(powder2)

        self.assertEqual(powder1.color, color_name_from_hsl(
            mix_hsl((hsl_from_color_name('green'), 10),
                    (hsl_from_color_name('blue'), 5))))
        self.assertEqual(powder1.mass, 15)


if __name__ == '__main__':
    unittest.main()
