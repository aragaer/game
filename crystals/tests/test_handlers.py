#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=missing-docstring, too-many-public-methods
# pylint: disable=maybe-no-member

"""
    Handlers. These get user requests and handle them to parsers,
    then delegate calls to View layer.
"""

import unittest

import tornado.web

import crystals
from crystals.alchemist import AlchemistRepo
from crystals.tests import create_mock_registry, CrystalsTestCase
from utils.mock import Mock, patch, sentinel


class AlchemistViewNamespaceMixinTest(unittest.TestCase):
    def test_inserts_context_to_namespace(self):
        import builtins
        class_to_test = crystals.handlers.AlchemistViewNamespaceMixin
        handler = Mock()
        handler.ui = {}
        handler.get_current_alchemist = Mock(return_value=sentinel.alchemist)
        handler.get_status.return_value = 200

        with patch('crystals.handlers.AlchemistView',
                   return_value=sentinel.view) as mock_view:
            mock_super = Mock(tornado.web.RequestHandler)
            mock_super.get_template_namespace.return_value = {}
            original_super = super

            def mysuper(*args, **kwargs):
                if len(args) == 2 and args[1] == handler:
                    return mock_super
                return original_super(*args, **kwargs)
            builtins.super = mysuper

            try:
                namespace = class_to_test.get_template_namespace(handler)
            finally:
                builtins.super = original_super

            mock_view.assert_called_once_with(sentinel.alchemist)
            mock_super.get_template_namespace.assert_called_once_with()

        self.assertIn('alchemist', namespace)
        self.assertEqual(namespace['alchemist'], sentinel.view)

    def test_does_nothing_on_error(self):
        import builtins
        class_to_test = crystals.handlers.AlchemistViewNamespaceMixin
        handler = Mock()
        handler.ui = {}
        handler.get_current_alchemist = Mock()
        handler.get_status.return_value = 500

        with patch('crystals.handlers.AlchemistView',
                   return_value=sentinel.view) as mock_view:
            mock_super = Mock(tornado.web.RequestHandler)
            mock_super.get_template_namespace.return_value = {}
            original_super = super

            def mysuper(*args, **kwargs):
                if len(args) == 2 and args[1] == handler:
                    return mock_super
                return original_super(*args, **kwargs)
            builtins.super = mysuper

            try:
                namespace = class_to_test.get_template_namespace(handler)
            finally:
                builtins.super = original_super

            self.assertFalse(mock_view.called)
            mock_super.get_template_namespace.assert_called_once_with()

        self.assertNotIn('alchemist', namespace)


@patch.object(crystals.market.TradeSession, 'trade')
class TradeTest(CrystalsTestCase):

    def test_calls_trade(self, mock_trade):
        self.fetch('/trade/')

        self.assertTrue(mock_trade.called)

    def test_calls_powders_trade(self, mock_trade):
        self.fetch('/trade/powders/')

        self.assertTrue(mock_trade.called)

    def test_process_powders(self, mock_trade):
        self.fetch('/trade/powders/?p=1&p=2&p=3&c=1&c=0&c=1&u=2')

        mock_trade.assert_called_once_with(
            powders=['1', '2', '3'])

    def test_process_upgrades(self, mock_trade):
        self.fetch('/trade/?p=1&p=2&p=3&c=1&c=0&c=1&u=2')

        mock_trade.assert_called_once_with(upgrades=['2'])

    def test_process_crystals(self, mock_trade):
        self.fetch('/trade/jeweler/?p=1&p=2&p=3&c=1&c=0&c=1&u=2')

        mock_trade.assert_called_once_with(crystals=['1', '0', '1'])

    def test_redirect_to_market(self, _):
        response = self.fetch('/trade/', follow_redirects=False)

        self.assertEqual(response.code, 302)
        self.assertIn('Location', response.headers)
        self.assertEqual(response.headers['Location'], '/market/')

    def test_redirect_to_powders(self, _):
        response = self.fetch('/trade/powders/', follow_redirects=False)

        self.assertEqual(response.code, 302)
        self.assertIn('Location', response.headers)
        self.assertEqual(response.headers['Location'], '/market/powders/')


@patch.object(tornado.web.RequestHandler, 'render')
class HandlerTemplateTest(CrystalsTestCase):

    def test_render(self, render):
        cases = {
            '/market/powders/': 'powders_shop.xhtml',
            '/market/jeweler/': 'jeweler.xhtml',
            '/lab/': 'laboratory.xhtml',
            '/': 'inventory.xhtml',
            '/market/': 'market.xhtml'
        }
        for path, template in cases.items():
            render.reset_mock()

            response = self.fetch(path)

            self.assertEqual(response.code, 200)
            self.assertEqual(render.call_count, 1)
            self.assertEqual(render.call_args[0][0], template)


class CurrentUserTest(unittest.TestCase):

    def test_get_current_alchemist(self):
        registry = create_mock_registry()
        repo = AlchemistRepo(registry)
        alchemist = repo.create()

        handler = Mock()
        handler.get_current_user.return_value = sentinel.badge
        handler.recreation.registry = registry
        handler.application.reception.get_note.return_value = \
            alchemist.alchemist_id
        handler.repo = repo

        alch = crystals.handlers.CrystalHandler.get_current_alchemist(handler)

        handler.application.reception.get_note.assert_called_once_with(
            sentinel.badge, 'alchemist')
        self.assertEqual(alch.document.document_id,
                         alchemist.document.document_id)


class LocaleTest(unittest.TestCase):

    registry = None
    user = None

    def setUp(self):
        self.reception = Mock()
        self.user = Mock()

    def test_language_user(self):
        from crystals.handlers import CrystalHandler
        from tornado.locale import get_supported_locales
        for language in [code[:2].upper()
                         for code in get_supported_locales()]:
            self.reception.get_note.reset_mock()
            self.reception.get_note.return_value = language.lower()

            user_handler = Mock()
            user_handler.get_current_user.return_value = sentinel.badge
            user_handler.application.reception = self.reception

            locale = CrystalHandler.get_user_locale(user_handler)

            self.reception.get_note.assert_called_once_with(
                sentinel.badge, 'locale')
            self.assertEqual(locale.code[:2], language.lower())
            self.assertFalse(user_handler.get_cookie.called)

    def test_language_from_browser(self):
        from crystals.handlers import CrystalHandler
        user_handler = Mock()
        user_handler.get_current_user.return_value = None

        locale = CrystalHandler.get_user_locale(user_handler)

        self.assertIsNone(locale)


if __name__ == '__main__':
    tornado.testing.main()
