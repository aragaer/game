#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=missing-docstring, too-many-public-methods

from types import SimpleNamespace
import unittest

from bs4 import BeautifulSoup
import tornado

import crystals
from utils.mock import Mock, patch, sentinel


class ModuleTest(unittest.TestCase):

    module_class = Mock()
    submodules = {}

    def setUp(self):
        self.app = Mock()
        self.app.ui_methods = {}
        self.app.settings = {}
        self.app.ui_modules = self.submodules
        self.request = Mock()
        self.request.headers = {}
        self.handler = tornado.web.RequestHandler(self.app, self.request)
        self.handler.get_template_path = Mock(
            return_value='crystals/templates')
        self.module = self.module_class(self.handler)

    def render(self, *args, **kwargs):
        return self.module.render(*args, **kwargs)

    def get_soup(self, *args, **kwargs):
        return BeautifulSoup(self.render(*args, **kwargs))


class InventoryPowderModuleTest(ModuleTest):

    module_class = crystals.uimodules.InventoryPowder

    def test_should_generate_string(self):
        powder = Mock(spec=crystals.powder.Powder)
        powder.color = 'cyan'
        powder.mass = 10000

        soup = self.get_soup(powder)

        self.assertEqual(soup.text, "10g of cyan powder")


class InventoryCrystalModuleTest(ModuleTest):

    module_class = crystals.uimodules.InventoryCrystal

    def test_should_generate_string(self):
        tcs = [
            ('red', 10, 1),
            ('green', 6, 8),
            ('yellow', 4, 15),
        ]
        for color, carats, count in tcs:
            crystal = Mock(spec=crystals.view.CrystalGroupView)
            crystal.color = color
            crystal.carat = carats
            crystal.count = count

            soup = self.get_soup(crystal)

            expected = "%s %d-carat crystal" % (color.capitalize(), carats)
            if count != 1:
                expected += " x%d" % count
            self.assertEqual(soup.text, expected)


class WalletModuleTest(ModuleTest):

    module_class = crystals.uimodules.WalletModule
    alchemist = None

    def setUp(self):
        super().setUp()
        self.alchemist = Mock(spec=crystals.view.AlchemistView)
        self.handler.get_template_namespace = Mock(
            return_value={'alchemist': self.alchemist})

    def test_should_contain_label(self):
        self.alchemist.coins = 0

        soup = self.get_soup()

        label = soup.find('label', text='Cash:')

        self.assertIsNotNone(label)
        self.assertEqual(label.get('for'), 'id_wallet')

    def test_should_show_correct_cash(self):
        for coins in [15025, 1234, 1020, 100500]:
            self.alchemist.coins = coins

            soup = self.get_soup()

            cash = soup.find('span', id='id_wallet')

            self.assertEqual(cash.text,
                             crystals.market.cash_string_from_coins(coins))


class NavBarModuleTest(ModuleTest):

    module_class = crystals.uimodules.NavBarModule
    language_switch_mock = Mock()

    submodules = {'LanguageSwitch': Mock()}

    def get_soup(self, *args, **kwargs):
        self.request.path = kwargs.pop('path') if 'path' in kwargs else '/'

        return super().get_soup(*args, **kwargs)

    def test_have_language_switch(self):
        self.submodules['LanguageSwitch'].reset_mock()

        self.render()

        self.assertTrue(self.submodules['LanguageSwitch'].called)

    def test_have_menu_items(self):
        soup = self.get_soup()
        navigation = soup.find('ul', {'class': 'nav navbar-nav'})
        links = list(self.module_class.menu_items)
        self.assertEqual(links, [i.text for i in navigation.find_all('a')])

    def test_have_settings(self):
        soup = self.get_soup()
        navigation = soup.find('ul', {'class': 'nav navbar-nav navbar-right'})
        links = ['Settings', 'Logout']
        self.assertEqual(links, [i.text for i in navigation.find_all('a')])

    def test_use_bootstrap(self):
        js_files = self.module.javascript_files()
        css_files = self.module.css_files()

        self.assertIn('/static/jquery.min.js', js_files)
        self.assertIn('/static/bootstrap/js/bootstrap.min.js', js_files)
        self.assertIn('/static/bootstrap/css/bootstrap.min.css', css_files)

    def test_mark_current_as_active(self):
        soup = self.get_soup(path='/lab/')
        navigation = soup.find('ul', {'class': 'nav navbar-nav'})
        for item in navigation.find_all('li'):
            if item.text == 'Laboratory':
                self.assertIn('active', item.get('class'))

    def test_is_collapsible(self):
        soup = self.get_soup()

        header = soup.find('div', {'class': 'navbar-header'})
        self.assertIsNotNone(header)

        toggle = header.find('button', {'class': 'navbar-toggle'})
        self.assertEqual('collapse', toggle.get('data-toggle'))
        self.assertIsNotNone(toggle)

        collapse_id = toggle.get('data-target')[1:]
        collapsible = soup.find('div', id=collapse_id)
        self.assertIsNotNone(collapsible)
        self.assertIn('collapse', collapsible.get('class'))
        self.assertIn('navbar-collapse', collapsible.get('class'))

    def test_logout_button(self):
        soup = self.get_soup()

        button = soup.find('a', text='Logout')
        self.assertEqual(button.get('href'), '/logout/')
        self.assertIn('btn-danger', button.get('class'))


class LanguageDropdownTest(ModuleTest):

    module_class = crystals.uimodules.LanguageDropdown

    def test_use_bootstrap(self):
        js_files = self.module.javascript_files()
        css_files = self.module.css_files()

        self.assertIn('/static/jquery.min.js', js_files)
        self.assertIn('/static/bootstrap/js/bootstrap.min.js', js_files)
        self.assertIn('/static/bootstrap/css/bootstrap.min.css', css_files)

    def test_use_cookie(self):
        js_files = self.module.javascript_files()

        self.assertIn('/static/jquery.cookie.js', js_files)

    def test_no_cookie_if_user_logged(self):
        self.handler.get_current_user = Mock(return_value=sentinel.user)

        js_files = self.module.javascript_files()

        self.assertNotIn('/static/jquery.cookie.js', js_files)

    def get_soup(self):
        self.handler.get_user_locale = Mock(
            return_value=SimpleNamespace(code='en_EN'))

        with patch('tornado.locale.get_supported_locales',
                   return_value=['en_US', 'ru_RU', 'fr_FR']):
            soup = super().get_soup()
        return soup

    def test_current_language(self):
        soup = self.get_soup()

        btn = soup.find('button')
        self.assertEqual(btn.text, 'EN ')

    def test_language_list_logged_in(self):
        self.handler.get_current_user = Mock(return_value=sentinel.user)

        soup = self.get_soup()

        rows = soup.find('ul', {'class': 'dropdown-menu'}).find_all('li')
        for row, text in zip(rows, ['FR', 'RU']):
            self.assertEqual(row.text, text)
            # has to have a link which sets locale for the user
            link = row.find('a')
            self.assertEqual(link.get('href'), '/locale/?lang=%s' % text)

    def test_language_list_logged_off(self):
        self.assertIsNone(self.handler.get_current_user())

        soup = self.get_soup()

        rows = soup.find('ul', {'class': 'dropdown-menu'}).find_all('li')
        for row, text in zip(rows, ['FR', 'RU']):
            self.assertEqual(row.text, text)
            # has to set a locale using cookie
            link = row.find('a')
            self.assertEqual(link.get('href'), '.')
            self.assertEqual(link.get('onclick'),
                             '$.cookie(\'lang\',\'%s\',{path:\'/\'})' % text)


class MarketCrystalModuleTest(ModuleTest):

    module_class = crystals.uimodules.MarketCrystal

    def test_display_crystal_for_sale(self):
        data = [
            ('red', 5, 2),
            ('blue', 15, 5),
            ('green', 7, 1),
            ('yellow', 4, 15),
        ]

        for color, carat, count in data:
            group = Mock(spec=crystals.view.CrystalGroupView)
            group.color, group.carat, group.count = color, carat, count
            group.cost_string = sentinel.cost_string

            soup = self.get_soup(group)

            spans = soup.find_all('span')
            self.assertEqual(spans[0].text,
                             "%s %d-carat crystal" % (
                                 color.capitalize(), carat))
            self.assertEqual(spans[1].text,
                             '' if count == 1 else 'x%d' % count)
            sell_box = spans[2].find('input')
            self.assertEqual(sell_box.get('max'), str(count))
            self.assertEqual(sell_box.get('name'), 'c')
            self.assertEqual(spans[3].text, '@ %s' % group.cost_string)


class MarketPowderModuleTest(ModuleTest):

    module_class = crystals.uimodules.MarketPowder

    def test_display_powder(self):
        from crystals.market import POWDERS, cash_string_from_coins
        for powder in POWDERS:
            soup = self.get_soup(powder)

            spans = soup.find_all('span')
            self.assertEqual(spans[0].text, powder.color.capitalize())
            buy_box = spans[1].find('input')
            self.assertEqual(buy_box.get('name'), 'p')
            self.assertEqual(spans[2].text,
                             '@ %s/gram' % cash_string_from_coins(
                                 powder.price))


class LabProcessModuleTest(ModuleTest):

    module_class = crystals.uimodules.LabProcess

    def test_display_running_process(self):
        from datetime import timedelta
        from crystals.utilities import time_str
        data = [
            ('green', timedelta(hours=3))
        ]
        for color, delta in data:
            process = Mock(spec=crystals.view.ProcessView)
            process.color = color
            process.finished = False
            process.time_left = delta

            soup = self.get_soup(1, process)

            desc = soup.find('div', {'class': 'process_desc'})
            self.assertEqual(desc.text.strip(), color.capitalize())
            time = soup.find('div', {'class': 'process_time'})
            self.assertEqual(time.text.strip(), time_str(delta, True))
            self.assertIsNotNone(soup.find('button', {'class': 'cancel'}))

    def test_display_finished_process(self):
        from datetime import timedelta
        data = [
            ('green', timedelta(hours=-3))
        ]
        for color, _ in data:
            process = Mock(spec=crystals.view.ProcessView)
            process.color = color
            process.finished = True

            soup = self.get_soup(1, process)

            text = soup.text.strip()
            self.assertTrue(text.startswith(color.capitalize()))
            self.assertIsNotNone(soup.find('button', {'class': 'collect'}))


if __name__ == '__main__':
    unittest.main()
