#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=missing-docstring, too-many-public-methods

import unittest
import crystals.utilities
from crystals.config import MOLECULES_PER_GRAM, MOLECULES_PER_CARAT


class UnitTestConverters(unittest.TestCase):

    def test_grams_from_molecules(self):
        for molecules in range(0, 10000, 50):
            expected = round(molecules / MOLECULES_PER_GRAM, 1)
            result = crystals.utilities.grams_from_molecules(molecules)
            self.assertEqual(expected, result)

    def test_molecules_from_grams(self):
        for molecules in range(0, 10000, 100):
            grams = crystals.utilities.grams_from_molecules(molecules)
            result = crystals.utilities.molecules_from_grams(grams)
            self.assertEqual(molecules, result)
            self.assertIsInstance(result, int)

    def test_carats_from_molecules(self):
        for molecules in range(0, 10000, 50):
            expected = int(round(molecules / MOLECULES_PER_CARAT))
            result = crystals.utilities.carats_from_molecules(molecules)
            self.assertEqual(expected, result)
            self.assertIsInstance(result, int)


if __name__ == '__main__':
    unittest.main()
