#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""
    Main module for Crystals game.
"""

import tornado.web

from establishment import Establishment
from establishment.reception import Reception
from establishment.reception.books import BlackBoard
from establishment.recreation import Area, Direct, Recreation

from crystals.club_handlers import LocaleHandler, LoginHandler
import crystals.handlers
import crystals.market.handlers
import crystals.uimodules


_HANDLERS = [
    (r'/login/', LoginHandler),
    (r'/logout/', LoginHandler),
    (r'/locale/', LocaleHandler),
    (r'/static/bootstrap/(.+)', tornado.web.StaticFileHandler,
     {'path': 'crystals/static/bootstrap/dist'}),
    (r'/static/(jquery.cookie.js)', tornado.web.StaticFileHandler,
     {'path': 'crystals/static/jquery-cookie'}),
    (r'/static/([^\/]*)', tornado.web.StaticFileHandler,
     {'path': 'crystals/static'}),
]

_MODULES = {
    'InventoryCrystal': crystals.uimodules.InventoryCrystal,
    'InventoryPowder': crystals.uimodules.InventoryPowder,
    'LanguageSwitch': crystals.uimodules.LanguageDropdown,
    'MarketCrystal': crystals.uimodules.MarketCrystal,
    'MarketPowder': crystals.uimodules.MarketPowder,
    'NavBarModule': crystals.uimodules.NavBarModule,
    'WalletModule': crystals.uimodules.WalletModule,
    'LabProcess': crystals.uimodules.LabProcess,
    'LabNewProcess': crystals.uimodules.LabNewProcess,
    'NewProcess': crystals.uimodules.NewProcess,
}


class CrystalsRecreation(Recreation):
    """
        That's where people play alchemists.
    """
    def __init__(self, registry):
        self.registry = registry
        super().__init__(
            'crystals',
            [Area('bowl action', r'/bowl/([0-9]*)/([a-z_]+)/',
                  crystals.handlers.BowlActionHandler),
             Area('start process', r'/start/',
                  crystals.handlers.StartProcessHandler)]
            + crystals.market.handlers.SHOP_AREAS
            + crystals.handlers.TEMPLATE_AREAS,
            [Direct('process preview', '/bowl_preview/',
                    crystals.handlers.CrystalsConnection),
             Direct('market preview', '/market_preview/',
                    crystals.handlers.MarketPreviewConnection)])


class CrystalsGameClub(Establishment):
    """
        The crystals game club.

        Everyone is welcome to play an alchemist and grow some crystals
        in a virtual world of Crystals game.
    """
    def __init__(self, **settings):
        if 'reception' not in settings:
            book = BlackBoard()
            settings['reception'] = Reception(book=book)

        if 'cookie_secret' not in settings:
            from base64 import b64encode
            from uuid import uuid4
            settings['cookie_secret'] = b64encode(uuid4().bytes+uuid4().bytes)

        if 'ui_modules' not in settings:
            settings['ui_modules'] = {}
        settings['ui_modules'].update(_MODULES)

        if 'recreations' not in settings:
            settings['recreations'] = CrystalsRecreation(None)

        # pylint: disable=star-args
        super().__init__(_HANDLERS, **settings)
