#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=too-many-public-methods, star-args

"""
    Trade handlers.
"""

from tornado.web import authenticated

from crystals.handlers import AlchemistViewNamespaceMixin, CrystalHandler
from crystals.market import TradeSession
from establishment.recreation import Area


class _CommonTradeHandler(CrystalHandler):

    args_parser = None
    query_spec = None

    def parse_args(self):
        "Convert tornado args to python dictionary."

    @authenticated
    def get(self):
        session = TradeSession(
            self.get_current_alchemist(),
            registry=self.recreation.registry)
        session.trade(**self.parse_args())
        self.redirect(self.query_spec)


class _ShopQuery(AlchemistViewNamespaceMixin, CrystalHandler):

    template = None
    action = None

    @authenticated
    def get(self):
        self.render(self.template, action=self.action)


class Shop(object):
    "Contains query and command areas."

    def __init__(self, name, suffix, template, parser_func):
        self._name = name
        self._specs = {'query': '/market/'+suffix,
                       'command': '/trade/'+suffix}
        self._query = self._make_query(template)
        self._command = self._make_command(parser_func)

    @property
    def query(self):
        "Query area."
        return self._query

    @property
    def command(self):
        "Command area."
        return self._command

    def _make_query(self, template):
        query_name = self._name+'Query'
        query_class = type(query_name, (_ShopQuery,),
                           {'action': self._specs['command'],
                            'template': template})
        return Area(query_name, self._specs['query'], query_class)

    def _make_command(self, parser_func):
        command_name = self._name+'Command'
        command = type(command_name, (_CommonTradeHandler,),
                       {'query_spec': self._specs['query'],
                        'parse_args': parser_func})
        return Area(command_name, self._specs['command'], command)


def _parse_common_trade(request):
    return {'upgrades': request.get_arguments('u')}


def _parse_powders_trade(request):
    return {'powders': request.get_arguments('p')}


def _parse_crystals_trade(request):
    return {'crystals': request.get_arguments('c')}


SHOPS = {
    "market": Shop('Market', '', 'market.xhtml', _parse_common_trade),
    "powder": Shop('PowderShop', 'powders/', 'powders_shop.xhtml',
                   _parse_powders_trade),
    "jeweler": Shop('Jeweler', 'jeweler/', 'jeweler.xhtml',
                    _parse_crystals_trade)
}
SHOP_AREAS = [SHOPS['market'].query, SHOPS['market'].command,
              SHOPS['powder'].query, SHOPS['powder'].command,
              SHOPS['jeweler'].query, SHOPS['jeweler'].command]
