#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""
    All things market-related.
"""


from collections import namedtuple
import re

from crystals.utilities import molecules_from_grams


_MarketPowder = namedtuple('_MarketPowder', 'color price')

_MARKET_POWDERS = [
    ('red', 100),
    ('blue', 100),
    ('green', 100),
    ('yellow', 70),
    ('purple', 70),
    ('cyan', 70),
    ('gray', 10),
]

_MARKET_CRYSTALS = {
    'red': 1,
    'blue': 1,
    'green': 1,
    'yellow': 0.9,
    'purple': 0.9,
    'cyan': 0.9,
    'gray': 0.1,
    'black': 0.7,
    'white': 0.7,
}

# pylint doesn't like _make
POWDERS = [_MarketPowder(c, p) for c, p in _MARKET_POWDERS]


def cash_string_from_coins(price):
    "Converts price from coins to readable string."
    coins = []
    for coin in "CS":
        coins.insert(0, (price % 100, coin))
        price //= 100
    coins.insert(0, (price, 'G'))
    return ' '.join("%d%s" % i for i in coins if i[0])


def coins_from_cash_string(price):
    "Parses string to coins."
    coins = 0
    for grp in price.split():
        count, coin = re.match(r'(\d+)([CSG])', grp).groups()
        count = int(count)
        if coin == 'S':
            count *= 100
        elif coin == 'G':
            count *= 10000
        coins += count
    return coins


# FIXME: Rewrite this to properly appraise crystals
def appraise(crystal):
    "Returns the cost of crystal based on its size and color."
    size = crystal.size
    return int(size * size / 1000 * _MARKET_CRYSTALS[crystal.color])


def enumerate_and_filter_positive(items, cast_to=int):
    "Safely parse list of arguments, returning only positive numbers."
    result = []
    for num, count in enumerate(items or []):
        try:
            count = cast_to(count)
        except ValueError:
            continue
        if count > 0:
            result.append((num, count))
    return result


def map_and_filter(counts, items, func, cast_to=int):
    "Safely parse list of count, returning objects with positive counts."
    result = []
    for item, count in zip(items or [], counts):
        try:
            count = cast_to(count)
        except ValueError:
            continue
        if func(item, count):
            result.append((item, count))
    return result


def _buy_upgrades(alchemist, upgrades):
    for _, count in upgrades:
        alchemist.bowls += count


def _buy_powders(alchemist, powders):
    for num, amount in powders:
        alchemist.add_powder(color=POWDERS[num].color,
                             mass=molecules_from_grams(amount))


def _sell_crystals(alchemist, crystals):
    for group, count in crystals:
        for _ in range(count):
            member = group.members.pop()
            if member.inner.count > 1:
                member.inner.count -= 1
            else:
                alchemist.remove_crystal(member.inner)


class TradeSession(object):
    """
        One alchemist and one trade attempt.
    """

    _upgrades = None
    _powders = None
    _crystals = None

    def __init__(self, alchemist, registry=None):
        self._alchemist = alchemist
        self._registry = registry
        self._price = 0

    def _parse_args(self, crystals=None, powders=None, upgrades=None):
        from crystals.view import AlchemistView
        sale_list = AlchemistView(self._alchemist).crystals
        self._upgrades = enumerate_and_filter_positive(upgrades)
        self._powders = enumerate_and_filter_positive(powders)
        self._crystals = map_and_filter(crystals or [], sale_list,
                                        lambda i, c: 0 < c <= i.count)

    def _calculate_price(self):
        for _, count in self._upgrades:
            self._price += count * 50000
        for num, amount in self._powders:
            self._price += POWDERS[num].price * amount
        for group, count in self._crystals:
            self._price -= appraise(group.members[0].inner) * count

    def _perform_trade(self):
        from crystals.alchemist import AlchemistRepo
        self._alchemist.coins -= self._price
        _buy_upgrades(self._alchemist, self._upgrades)
        _buy_powders(self._alchemist, self._powders)
        _sell_crystals(self._alchemist, self._crystals)
        AlchemistRepo(self._registry).save(self._alchemist)

    def trade(self, crystals=None, powders=None, upgrades=None):
        """
            Perform the trade.
        """
        self._parse_args(crystals, powders, upgrades)
        self._calculate_price()
        if self._alchemist.coins >= self._price:
            self._perform_trade()
