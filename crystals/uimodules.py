#!/usr/bin/python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""UI modules for crystals."""


from collections import OrderedDict
import gettext

import tornado.locale
import tornado.web

from crystals.utilities import grams_from_molecules


_ = gettext.gettext


class LanguageDropdown(tornado.web.UIModule):
    """Language switch module."""

    def javascript_files(self):
        js_files = ['/static/jquery.min.js',
                    '/static/bootstrap/js/bootstrap.min.js']
        if self.handler.get_current_user() is None:
            js_files.append('/static/jquery.cookie.js')
        return js_files

    def css_files(self):
        return ['/static/bootstrap/css/bootstrap.min.css']

    def render(self):
        out = ["<div id='id_languages' class='btn-group nav'>",
               "<button type='button'",
               " class='btn btn-default navbar-btn dropdown-toggle'",
               " data-toggle='dropdown'>"]
        current = self.handler.locale.code[:2].upper()
        user = self.handler.get_current_user()
        out.append("%s <span class='caret' /></button>" % current)
        out.append("<ul class='dropdown-menu' role='menu'>")

        for lang in sorted(tornado.locale.get_supported_locales()):
            lang = lang[:2].upper()
            if lang == current:
                continue
            elif user:
                click = "href='/locale/?lang=%s'" % lang
            else:   # Login page, locale is based on cookie
                click = """
                    onclick=\"$.cookie('lang','%s',{path:\'/\'})\"
                    href='.'
                    """.strip() % lang
            out.append("<li><a %s>%s</a></li>" % (click, lang))
        out.append('</ul></div>')
        return ''.join(' '.join(i.split()) for i in out)


class NewProcess(tornado.web.UIModule):
    """Empty bowls shown in inventory."""

    def render(self, count):
        return '<div class="new-process">' \
            '<a href="/preparation/">%s x%d</a></div>' \
            % (_('Empty bowl'), count)


class InventoryPowder(tornado.web.UIModule):
    """Inventory record describing a powder."""

    def render(self, powder):
        return '<li>%gg of %s powder</li>' % (
            grams_from_molecules(powder.mass),
            powder.color)


class NavBarModule(tornado.web.UIModule):
    "Navigation bar."

    menu_items = OrderedDict([
        ('News', '/news/'),
        ('Status', '/'),
        ('Laboratory', '/lab/'),
        ('Market', '/market/'),
    ])

    def javascript_files(self):
        return ['/static/jquery.min.js',
                '/static/bootstrap/js/bootstrap.min.js']

    def css_files(self):
        return ['/static/bootstrap/css/bootstrap.min.css']

    def render(self):
        for label, url in self.menu_items.items():
            if url == self.handler.request.path:
                current = label
                break
        else:
            current = None
        return self.render_string('navbar.xhtml', menu_items=self.menu_items,
                                  current=current)


class InventoryCrystal(tornado.web.UIModule):
    """Inventory record describing a crystal."""

    def render(self, crystal):
        string = "%s %d-carat crystal" % (crystal.color.capitalize(),
                                          crystal.carat)
        if crystal.count != 1:
            string += " x%d" % crystal.count
        return '<li>'+string+'</li>'


class WalletModule(tornado.web.TemplateModule):
    """Displays how much money the alchemist has."""

    def render(self):
        return super().render('wallet.xhtml')


class MarketCrystal(tornado.web.UIModule):
    """Market record for selling a crystal."""

    def render(self, crystal):
        return self.render_string('market_crystal.xhtml',
                                  cost=crystal.cost_string,
                                  color=crystal.color, carat=crystal.carat,
                                  count=crystal.count)


class MarketPowder(tornado.web.UIModule):
    """Market record for buying a powder."""

    def render(self, powder):
        return self.render_string('market_powder.xhtml', powder=powder)


class LabProcess(tornado.web.UIModule):
    """Laboratory entry for a running process."""

    def render(self, num, process):
        return self.render_string('lab_process.xhtml', num=num,
                                  process=process)


class LabNewProcess(tornado.web.UIModule):
    """Button to start a new process."""

    def render(self, number_of_empty_bowls):
        return self.render_string('lab_new_process.xhtml',
                                  count=number_of_empty_bowls)
