#!/usr/bin/python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""
    Color handling.

    The module uses modified HSL color space: hue and saturation are
    turned to complex number and its real and imaginary parts are used.
    Luminocity stays the same.
"""


from math import sqrt, pi
from colorsys import rgb_to_hls, hls_to_rgb
from cmath import rect, phase


# pylint: disable=star-args

# Clear colors
COLORS_CLEAR = {
    'red': [255, 0, 0],
    'green': [0, 255, 0],
    'blue': [0, 0, 255],
    'yellow': [255, 255, 0],
    'cyan': [0, 255, 255],
    'purple': [255, 0, 255],
    'black': [0, 0, 0],
    'white': [255, 255, 255],
    'gray': [128, 128, 128],
}

# Somewhat impure colors but still recognizable
COLORS = {
    'red': [224, 32, 32],
    'green': [32, 224, 32],
    'blue': [32, 32, 224],
    'yellow': [224, 224, 32],
    'cyan': [32, 224, 224],
    'purple': [224, 32, 224],
    'black': [32, 32, 32],
    'white': [224, 224, 224],
    'gray': [128, 128, 128],
}


class Color(object):
    def __init__(self, hsl, name):
        self._hsl = hsl
        self._name = name

    @property
    def hsl(self):
        return self._hsl

    @property
    def name(self):
        return self._name

    @staticmethod
    def from_hsl(hsl):
        name = color_name_from_hsl(hsl)
        return Color(hsl, name)

    @staticmethod
    def from_name(name):
        hsl = hsl_from_color_name(name)
        return Color(hsl, name)

    def __eq__(self, other):
        return self.hsl == other.hsl


def hsl_from_rgb(rgb):
    "Short wrapper for RGB to HSL COMPLEX coordinates conversion."
    hue, lum, sat = rgb_to_hls(*[x/255 for x in rgb])
    complex_color = rect(sat, hue*2*pi)
    return complex_color.real, complex_color.imag, lum


def rgb_from_hsl(hsl):
    "Reverse of the above."
    real, imag, lum = hsl
    complex_color = complex(real, imag)
    hue = phase(complex_color)/2/pi
    sat = abs(complex_color)
    return tuple(int(255*x) for x in hls_to_rgb(hue, lum, sat))


COLORS_HSL = dict((name, hsl_from_rgb(val)) for name, val in COLORS.items())

CLEAR_HSL = dict((name, hsl_from_rgb(val))
                 for name, val in COLORS_CLEAR.items())


def _distance(left, right):
    "Calculates distance between 2 values in euclidian space."
    return sqrt(sum((l - r) ** 2 for l, r in zip(left, right)))


def _color_distance(color):
    "Function for sorting colors."
    return lambda other: _distance(color, COLORS_HSL[other])


def color_name_from_rgb(rgb):
    "Returns name of given RGB."
    return color_name_from_hsl(hsl_from_rgb(rgb))


def color_name_from_hsl(hsl):
    "Returns name of given HSL Complex."
    return min(COLORS_HSL, key=_color_distance(hsl))


def rgb_from_color_name(name):
    "Returns the RGB tuple of named the color."
    return tuple(COLORS_CLEAR[name])


def hsl_from_color_name(name):
    "Returns the RGB tuple of named the color."
    return CLEAR_HSL[name]


def _linear_combination(items, weights):
    """Returns a weighted average of parameters."""
    total_weight = sum(weights)
    assert total_weight
    return sum(i*w for i, w in zip(items, weights))/total_weight


def mix_rgb(*args):
    """
        Calculates the resulting color of mixing two colors.

        Arguments are (rgb, weight) tuples.
    """
    return rgb_from_hsl(mix_hsl(*[(hsl_from_rgb(c), w) for c, w in args]))


def mix_hsl(*args):
    """
        Calculates the resulting color of mixing two colors.

        Arguments are (hsl, weight) tuples.
        The resulting color's hue and saturation is calculated as
        a weighted average of passed colors in HSL space, lightness
        is weighted average of lightnesses.
    """
    assert args
    if len(args) == 1:
        return args[0][0]
    hsl, weights = zip(*args)
    return tuple(_linear_combination(p, weights) for p in zip(*hsl))


Color.BLACK = Color.from_hsl((0, 0, 0))
