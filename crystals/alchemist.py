#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""
    Alchemist - a single Crystals game character.
"""

from bson import ObjectId

from crystals.color import Color
from crystals.crystal import Crystal
from crystals.document import AlchemistDocument, CrystalDocument, \
    PowderDocument, ProcessDocument
from crystals.powder import Powder
from crystals.process import Process


INITIAL_POWDERS = [
    ('red', 10000),
    ('green', 10000),
    ('blue', 10000),
]


def _supply_initial_resources(document):
    document.bowls = 3
    document.powders = [PowderDocument({'color': c, 'mass': m})
                        for c, m in INITIAL_POWDERS]
    document.coins = 1000


class AlchemistRepo(object):
    """
        A way to access alchemist model objects using alchemist_id.
    """
    def __init__(self, registry):
        self._registry = registry

    def create(self):
        """
            Create a new alchemist.
        """
        document = self._registry.fetch(self._registry.register())
        _supply_initial_resources(document)
        self._registry.save(document)
        return document.make_alchemist()

    def find(self, alchemist_id):
        """
            Return alchemist object with given id.
        """
        document = self._registry.fetch(alchemist_id)
        return document.make_alchemist()

    def save(self, alchemist):
        """
            Save the changes.
        """
        self._registry.save(AlchemistDocument.from_alchemist(alchemist))


class Alchemist(object):
    """
        An object describing an alchemist behavior.
    """

    document = None

    def __init__(self, document=None, powders=None, coins=None, bowls=None,
                 _id=None, crystals=None):
        if document is None:
            self.document = AlchemistDocument({})
        else:
            self.document = document
            for i, process in enumerate(self.processes):
                process.sync_time()
                self.document.processes[i] = \
                    ProcessDocument.from_process(process)

        self._powders = powders or []
        self._coins = coins or 0
        self._bowls = bowls or 0
        self._id = _id or ObjectId()
        self._crystals = crystals or []

    @property
    def bowls(self):
        "Empty bowls."
        return self._bowls

    @bowls.setter
    def bowls(self, value):
        "Change amount of empty bowls."
        self._bowls = value

    @property
    def alchemist_id(self):
        "Unique identifier for this alchemist."
        return self._id

    @property
    def coins(self):
        "Hard cash."
        return self._coins

    @coins.setter
    def coins(self, value):
        "Update wallet data."
        self._coins = value

    @property
    def processes(self):
        "Running processes."
        return [pdoc.make_process() for pdoc in self.document.processes]

    @property
    def powders(self):
        return self._powders

    @property
    def crystals(self):
        """
            Crystals owned by this alchemist.
        """
        return self._crystals

    def collect_from(self, process):
        """Handles collection of crystals/powder from a bowl."""
        powder, result = process.collect()
        if powder is not None:
            self.add_powder(color=powder.color, mass=powder.mass)
        for crystal in result:
            for owned in self._crystals:
                if owned.color == crystal.color and owned.size == crystal.size:
                    owned.count += crystal.count
                    break
            else:
                self._crystals.append(crystal)
        self.discard_from(process)

    def discard_from(self, process):
        """Discard bowl contents."""
        for pdoc in self.document.processes:
            if pdoc.details['_id'] == process.process_id:
                self.document.processes.remove(pdoc)
        self._bowls += 1

    def add_powder(self, powder=None, color=None, mass=0):
        """Add powder to alchemist inventory."""
        if powder is None:
            powder = Powder(mass, Color.from_name(color))
        for owned in self._powders:
            if color == owned.color:
                self._powders.remove(owned)
                powder.mix(owned)
                break
        self._powders.append(powder)

    def start_process(self, powders, water=None, seed=None):
        """Handles starting the evaporation process."""
        if self._bowls == 0:
            raise ValueError("No empty bowls")
        used_powders = [p.split(m) for p, m in zip(self._powders, powders) if m]
        self._powders = [p for p in self._powders if p.mass]
        process = Process.create(
            water=water,
            powders=used_powders,
            seed=None if seed is None else self._take_seed(seed))
        process.start_evaporation()
        self._bowls -= 1
        self.document.processes.append(ProcessDocument.from_process(process))

    def remove_crystal(self, crystal):
        for owned in self._crystals:
            if owned.color == crystal.color and owned.size == crystal.size:
                self._crystals.remove(owned)
                break
        else:
            raise ValueError("Crystal not found")

    def _take_seed(self, seed):
        seed_crystal = self._crystals[seed]
        seed_crystal.count -= 1
        if seed_crystal.count == 0:
            self._crystals.pop(seed)
        seed_label = CrystalDocument({'color': seed_crystal.color,
                                      'size': seed_crystal.size})
        return seed_label

    def __eq__(self, other):
        return self._id == other.alchemist_id
