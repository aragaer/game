#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""
    Alchemic process - this is where everything is happening.
"""

from bson import ObjectId
from datetime import datetime, timedelta
import math

from crystals.color import Color, mix_hsl
from crystals.colorless import Powder as ColorlessPowder, \
    Solution as ColorlessSolution
from crystals.config import EVAPORATION_TICK_TIME
from crystals.crystal import Crystal
from crystals.document import CrystalDocument
from crystals.powder import Powder


class Solution(object):
    def __init__(self, color, matter, water):
        self._color = color
        self._inner = ColorlessSolution(matter, water)

    @staticmethod
    def colored(water, powders):
        if water <= 0:
            raise ValueError("Water amount must be greater than 0")

        hsl = mix_hsl(*[(p.hsl, p.mass) for p in powders])
        return Solution(Color.from_hsl(hsl), sum(p.mass for p in powders),
                        water)

    @property
    def color(self):
        return self._color

    @property
    def volume(self):
        return self._inner.volume

    @volume.setter
    def volume(self, value):
        self._inner.volume = value

    @property
    def matter(self):
        return self._inner.matter

    @matter.setter
    def matter(self, value):
        self._inner.matter = value

    @property
    def absorbable(self):
        return self._inner.absorbable

    def coagulate(self):
        return Suspension(self._inner.coagulate())


class Sediment(object):
    def __init__(self, data=None, color=Color.BLACK, seed=None):
        self._inner = ColorlessPowder(data or {})
        self._seed = seed
        self._color = color

    @property
    def seed(self):
        return self._seed

    def grow(self, mass):
        return self._inner.grow(mass)

    def add_suspension(self, suspension):
        return self._inner.add_suspension(suspension.items)

    def set_seed(self, seed):
        self._seed = seed

    def add_crystal(self, crystal):
        self._inner[crystal.size] = 1

    def items(self):
        return self._inner.items()

    def set_color(self, color):
        self._color = color

    def collect_crystals(self):
        """Collect grown crystals."""
        hsl = self._color.hsl
        crystal_counter = self._inner.extract_crystals()
        former_seed = self.get_former_seed(crystal_counter)
        crystal_list = [CrystalDocument({'size': size, 'count': count,
                                         'hsl': hsl})
                        for size, count in crystal_counter.items()
                        if size and count]
        if former_seed:
            crystal_list.append(former_seed)
        return [Crystal(doc) for doc in crystal_list]

    def get_former_seed(self, crystal_counter):
        if self._seed is None:
            return None
        new_size = max(crystal_counter.keys())
        crystal_counter[new_size] -= 1
        new_hsl = mix_hsl((self._seed.hsl, self._seed.size),
                          (self._color.hsl, new_size-self._seed.size))
        return CrystalDocument({'size': new_size, 'hsl': new_hsl})

    def collect_powder(self):
        result_powder_mass = self._inner.extract_lost_mass()
        if result_powder_mass:
            return Powder(result_powder_mass, self._color)


class Suspension(object):
    def __init__(self, items):
        self._items = items

    @property
    def items(self):
        return self._items

    @property
    def mass(self):
        return sum(s*c for s, c in self._items)


class _Process(object):

    def __init__(self, solution, sediment, process_id, end_time):
        self.process_id = process_id
        self._solution = solution
        self._sediment = sediment
        self._end_time = end_time

    @property
    def end_time(self):
        "The moment the process is done."
        return self._end_time

    @property
    def solution(self):
        return self._solution

    @property
    def sediment(self):
        return self._sediment

    def collect(self):
        """Collect grown crystals and settled powder from bowl."""
        if not self.is_dry:
            raise ValueError("Bowl is not dry yet")

        return (self._sediment.collect_powder(),
                self._sediment.collect_crystals())

    def sync_time(self):
        """
            Evaporate as much water as needed.
        """
        time_till_dry = self._end_time - datetime.now()
        water_left = math.ceil(time_till_dry.total_seconds()
                               / EVAPORATION_TICK_TIME)
        if water_left < 0:
            water_left = 0
        while self._solution.volume > water_left:
            self.evaporate()

    def __eq__(self, other):
        return self.process_id == other.process_id

    @property
    def is_dry(self):
        """Bowl is dry and its contents can be collected."""
        return self._solution.volume == 0

    def evaporate(self):
        """Evaporate one step and grow crystals as needed."""
        self._solution.volume -= 1
        self._solution.matter -= self._sediment.grow(
            self._solution.absorbable())
        suspension = self._solution.coagulate()
        self._sediment.add_suspension(suspension)
        self._solution.matter -= suspension.mass

    def start_evaporation(self):
        """Start evaporation."""
        seconds = self._solution.volume*EVAPORATION_TICK_TIME
        self._end_time = datetime.now() + timedelta(seconds=seconds)


class Process(_Process):
    """
        An alchemic process in progress.
    """

    def __init__(self, solution=None, sediment=None, process_id=None,
                 end_time=None):
        process_id = process_id or ObjectId()
        solution = solution or Solution(Color.BLACK, 0, 0)
        sediment = sediment or Sediment(color=solution.color)
        end_time = end_time or datetime.now()
        super().__init__(solution, sediment, process_id, end_time)

    @staticmethod
    def create(water, powders, seed=None):
        sediment_data = {} if seed is None else {seed.size: 1}
        solution = Solution.colored(water, powders)
        return Process(sediment=Sediment(sediment_data, color=solution.color,
                                         seed=seed),
                       solution=solution)
