// vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

function fill_preview(where, desc) {
    items = where.find('li').length;
    if (items > desc.length)
        where.find('li').not(function (index) {
            return index > desc.length;
        }).remove();
    else
        for (i = items; i < desc.length; i++)
            where.append('<li>');

    where.find('li').each(function (index) {
        $(this).text(desc[index]);
    });

    // Valid solution descriptions are longer than 1 line
    $('input[type="submit"]').prop('disabled', desc.length == 1);
}

function gather_powders(where) {
    result = {};
    where.find('input[name="p"]').each(function (index) {
        result[$(this).data('color')] = +$(this).val();
    });
    return result;
}
