function all_wells() { return $('.process, .new-process'); }
function resize_stuff() {
	wells = all_wells();
	width = wells.eq(0).width();
	wells.height(width);
	$('.panel-footer').height(width/4);
}
function get_id_from_process(e) {
	process = $(e.target).parents('[data-id]').eq(0);
	return process.data('id');
}
function discard(e) {
	process_id = get_id_from_process(e);
	if (confirm('Are you sure you want to empty the bowl?'))
		window.location.href = '/bowl/'+process_id+'/empty/';
}
function collect(e) {
	process_id = get_id_from_process(e);
	window.location.href = '/bowl/'+process_id+'/collect/';
}
$(function () {
	all_wells().addClass('panel panel-default').wrap('<div class="col-xs-4 col-sm-3 col-md-2 col-lg-1"></div>');
	$('.process_desc').addClass('panel-body');
	$('.process').each(function () {
		$(this).children().not('.process_desc').wrapAll('<div class="panel-footer"></div>');
	});
	$('.process_time').addClass('pull-left');
	$('.collect').addClass('btn btn-success pull-right glyphicon glyphicon-ok').click(collect);
	$('.cancel').addClass('btn btn-danger pull-right glyphicon glyphicon-remove').click(discard);
	$('.new-process').append('<div class="panel-footer"><button class="btn btn-primary pull-right glyphicon glyphicon-plus"></button></div>');
	$('.new-process button').click(function () {window.location.href = '/preparation/'});
	resize_stuff();
});
