// vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

function gather_list(where) {
    result = [];
    where.find('input').each(function (index) {
        result[index] = +$(this).val();
    });
    return result;
}

function sell_all(where) {
    inputs = where.find('input');
    inputs.each(function (index) {
        $(this).val($(this).attr('max'));
    });
    if (inputs.length)
        inputs.eq(0).change();
}
