// vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

function fill_process(where, desc) {
    items = where.find('li').length;
    if (items > desc.length)
        where.find('li').not(function (index) {
            return index > desc.length;
        }).remove();
    else
        for (i = items; i < desc.length; i++)
            where.append('<li>');

    where.find('li').each(function (index) {
        $(this).text(desc[index]);
    });
}
