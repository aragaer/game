#!/bin/bash
mongo <<END
use crystals_backup
db.dropDatabase()
db.copyDatabase('crystals', 'crystals_backup')
END
