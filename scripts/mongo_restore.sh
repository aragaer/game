#!/bin/bash
mongo <<END
use crystals
db.dropDatabase()
db.copyDatabase('crystals_backup', 'crystals')
END
