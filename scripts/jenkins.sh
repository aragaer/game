#!/bin/bash

set -ex

START_TIME=`date +%s`
rm -rf env
virtualenv -p `which python3` env
. env/bin/activate
pip install -I -U -r requirements-dev.txt --download-cache .pip-cache -q
patch -d env/lib/python*/site-packages -p0 < ./behave.junit.diff
./make_mo.sh
pep8 . --exclude=bootstrap,env > pep8.log ||:
python `which pylint` --rcfile=pylintrc --msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" crystals tests establishment utils > pylint.log ||:
sed -n 's/Your code has been rated at \(.*\)\/10/YVALUE=\1/p' pylint.log > pylint_rating
python `which nosetests` --with-xunit --verbose
for file in `basename -s .html -a crystals/static/tests/*.html`; do
    ./scripts/run_qunit.py crystals/static/tests/$file.html > qunit.$file.xml
done
BEHAVE_START=`date +%s`
behave -v -c --junit
echo YVALUE=$((`date +%s` - $START_TIME)) > total_time
echo YVALUE=$((`date +%s` - $BEHAVE_START)) > behave_time
