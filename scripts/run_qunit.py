#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

from selenium import webdriver
import sys
import os.path

if __name__ == '__main__':
    print('Running', sys.argv[1], file=sys.stderr)
    browser = webdriver.Chrome()
    browser.get('file://%s' % os.path.abspath(sys.argv[1]))
    report = browser.execute_script('return xunit;')
    print(report)

    browser.quit()
