#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=star-args

"""
    Helper stuff for testing.
"""

import logging
from threading import Thread

from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.testing import bind_unused_port


class Server(Thread):
    """Test server."""

    http = None
    app = None

    def __init__(self):
        Thread.__init__(self)
        self.socket, self.port = bind_unused_port()

    def set_app(self, app):
        "Set the application to run."
        self.app = app

    def start(self):
        self.http = HTTPServer(self.app)
        self.http.add_sockets([self.socket])
        logging.getLogger('tornado.access').setLevel(logging.ERROR)
        Thread.start(self)

    def stop(self):
        "Stop the server."
        self.http.stop()

    def run(self):
        IOLoop.instance().start()
