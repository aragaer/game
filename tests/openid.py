#!/usr/bin/python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# pylint: disable=too-many-public-methods

"""Our own little OpenID server."""

from openid.server.server import Server as OpenIDServer
from openid.store.memstore import MemoryStore
from tornado.web import RequestHandler
from urllib.parse import parse_qsl

_OPENID_SERVERS = {}


class MockOpenIDHandler(RequestHandler):
    "Emulate OpenID server."

    endpoint = None
    fixed_user = None
    openid = None
    query = None
    user = None

    def initialize(self, **kwargs):
        port = kwargs['port']
        self.endpoint = "http://localhost:%d/openid/" % port
        if port not in _OPENID_SERVERS:
            _OPENID_SERVERS[port] = OpenIDServer(MemoryStore(), self.endpoint)
        self.openid = _OPENID_SERVERS[port]
        self.fixed_user = kwargs.get('fixed_user')

    def prepare(self):
        self.user = self.get_cookie('openid_user', default=None)
        self.query = dict((k, v[0].decode('utf-8'))
                          for k, v in self.request.arguments.items())

    def get(self, *args, **kwargs):
        if self.fixed_user is not None:
            request = self.openid.decodeRequest(
                dict(parse_qsl(self.request.query)))
            self.user = args[0]+'-'+self.fixed_user
            response = request.answer(True, identity=self.endpoint + self.user)
            self.display_response(response)
        elif self.user is None:
            self.set_cookie('openid_request', self.request.query)
            self.write(''.join([
                "<html><body><form method='post' action='../allow/'>",
                "<input type='text' id='provider' name='provider'",
                "value='%s' readonly>" % args[0],
                "<input type='text' id='login' name='login_as'>",
                "<input type='submit' value='Log in'></form></body></html>"]))
        else:
            self.server_end_point()

    def post(self, *args, **kwargs):
        if args[0] == 'allow':
            query = self.get_cookie('openid_request')
            request = self.openid.decodeRequest(dict(parse_qsl(query)))
            self.user = self.query['provider'] + '-' + self.query['login_as']
            response = request.answer(True, identity=self.endpoint + self.user)
            self.display_response(response)
        else:
            self.server_end_point()

    def server_end_point(self):
        "Process openid request"
        request = self.openid.decodeRequest(self.query)

        if request.mode in ["checkid_immediate", "checkid_setup"]:
            response = request.answer(True, identity=self.endpoint + self.user)
        else:
            response = self.openid.handleRequest(request)
        self.display_response(response)

    def display_response(self, response):
        "Send response data."
        webresponse = self.openid.encodeResponse(response)

        self.set_status(webresponse.code)
        for header, value in webresponse.headers.items():
            self.set_header(header, value)

        if self.user is None:
            self.clear_cookie('openid_user')
        else:
            self.set_cookie('openid_user', self.user)

        if webresponse.body:
            self.write(webresponse.body)
