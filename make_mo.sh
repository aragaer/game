#!/bin/bash

for pofile in `find locale -name '*.po'`; do
	msgfmt $pofile -o ${pofile/.po/.mo}
done
