#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

"""Tornado entry point"""

import os

import pymongo
import tornado.ioloop
from tornado.locale import load_gettext_translations

from establishment.reception.books import MongoBook

import crystals.document

from crystals import CrystalsGameClub


load_gettext_translations('locale/', 'crystals')


def get_mongo_connection():
    """
        It might not be trivial...
    """
    mongo_url = os.environ.get('MONGODB_URI')

    if mongo_url:
        from urllib.parse import urlparse
        conn = pymongo.MongoClient(mongo_url)
        return conn[urlparse(mongo_url).path[1:]]
    else:
        conn = pymongo.MongoClient('localhost', 27017)
        return conn.crystals


def main():
    """
        Application entry point.
    """
    connection = get_mongo_connection()

    club = CrystalsGameClub()
    club.reception.book = MongoBook(connection)
    club.recreations.registry = crystals.document.AlchemistRegistry(connection)

    club.listen(os.environ.get('PORT', 8000))
    tornado.ioloop.IOLoop.instance().start()


if __name__ == '__main__':
    from tornado.options import parse_command_line
    parse_command_line()
    main()
